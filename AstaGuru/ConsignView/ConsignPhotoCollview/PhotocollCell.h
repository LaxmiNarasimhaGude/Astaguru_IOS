//
//  PhotocollCell.h
//  AstaGuru
//
//  Created by Tejas Todkar on 06/03/21.
//  Copyright © 2021 4Fox Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PhotocollCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *collImgView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButtonCollection;

@end

NS_ASSUME_NONNULL_END
