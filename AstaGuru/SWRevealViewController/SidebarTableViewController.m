//
//  SidebarTableViewController.m
//  SidebarDemo
//
//  Created by Simon Ng on 10/11/14.
//  Copyright (c) 2014 AppCoda. All rights reserved.
//

#import "SidebarTableViewController.h"
#import "SWRevealViewController.h"
#import "EGOImageView.h"
#import "AboutUsViewController.h"
#import "ServicesViewController.h"
#import "ContactUsViewController.h"
#import "OurValuattionViewController.h"
#import "CareersViewController.h"
#import "PrivacyPoliceViewController.h"
#import "LoginViewController.h"
#import "CategoryListSidePageViewController.h"
#import "TermsConditionViewController.h"
#import "HowToBuyViewController.h"
#import "GetInTouchViewController.h"
#import "CareersPageViewController.h"
#import "CommonWebviewViewController.h"
#import "ConsignViewController.h"
#import "consignInitialViewController.h"
//#import "LoginViewController.h"
@interface SidebarTableViewController ()
{
    UINavigationController *navController;
    UISwipeGestureRecognizer *swipeGst;
    
}
@end

@implementation SidebarTableViewController {
    NSArray *menuItems,*arrmenuItemsImages;
    NSMutableArray *menuObjects, *subcategoryData;
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.revealViewController.frontViewController.view setUserInteractionEnabled:NO];
    [self.tableView reloadData];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.revealViewController.frontViewController.view setUserInteractionEnabled:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    navController = [[UINavigationController alloc] init];
    //navController.navigationBar.tintColor = [UIColor yellowColor];
    //navController.navigationBar.barTintColor = [UIColor redColor];
    
    //navController.navigationBar.tintColor = [UIColor yellowColor];
    navController.navigationBar.barTintColor = [UIColor blackColor];
    
    
    [navController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    menuObjects = [[NSMutableArray alloc]init];
    subcategoryData = [[NSMutableArray alloc]init];
    [self apiCallMenuItems];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    swipeGst = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(CloseMenu)];
    swipeGst.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.tableView addGestureRecognizer:swipeGst];
    self.tableView.keyboardDismissMode=UIScrollViewKeyboardDismissModeOnDrag;
    [self.tableView setSeparatorColor:[UIColor colorWithRed:240/255 green:240/255 blue:240/255 alpha:0.1]];
}

-(void)apiCallMenuItems {
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText = @"loading";
    
    NSMutableDictionary *Discparam=[[NSMutableDictionary alloc]init];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];  //AFHTTPResponseSerializer serializer
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    NSString  *strQuery=[NSString stringWithFormat:@"%@",[ClsSetting menuListURL]];
    NSString *url = strQuery;
    NSString *encoded = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [manager GET:encoded parameters:Discparam success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSError *error;
        NSArray *notiArray = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
        int i;
        for (i = 0; i < notiArray.count; i++) {
            NSDictionary *dict = notiArray[i];
            if ([[dict valueForKey:@"Keys"] isEqualToString:@"sub categories"]) {
                [subcategoryData addObject:notiArray[i]];
            } else {
                [menuObjects addObject:notiArray[i]];
            }
        }
        [self.tableView reloadData];
        //         [HUD hide:YES];
    }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [ClsSetting ValidationPromt:error.localizedDescription];
        //         [HUD hide:YES];
        
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section==0)
    {
        return 1;
    }
    else
    {
        return menuObjects.count;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section==0)
    {
        return 100;
    }
    else
    {
        return 40;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section==0)
    {
        static NSString* cellIdentifier = @"Profile";
        
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UILabel *lblSignIN = (UILabel*)[cell viewWithTag:2];
        
        UIButton *btnSignIn=(UIButton *)[cell viewWithTag:71];
        
        [btnSignIn addTarget:self action:@selector(btnSignInPressed) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:USER_id] intValue]>0)
        {
            lblSignIN.text = @"Sign out from My AstaGuru";
            [btnSignIn setTitle:@"Sign Out" forState:UIControlStateNormal];
        }
        else
        {
            lblSignIN.text = @"Sign in to My AstaGuru";
            [btnSignIn setTitle:@"Sign In" forState:UIControlStateNormal];
        }
        
        cell.separatorInset = UIEdgeInsetsZero;
        return cell;
    }
    else
    {
        static NSString* cellIdentifier = @"Cell";
        
        UITableViewCell* cell1 = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (!cell1)
        {
            cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        }
        cell1.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UILabel *lblUserNmae=(UILabel *)[cell1 viewWithTag:10];
        UIImageView *img=(UIImageView *)[cell1 viewWithTag:11];
        
        if (indexPath.row == menuObjects.count) {
            lblUserNmae.text = @"Consign";
            return cell1;
        }
        NSDictionary *dict = [menuObjects objectAtIndex:indexPath.row];
        
        NSString *str = [NSString stringWithFormat:@"%@%@", @"http://", [dict valueForKey:@"Icon"]];
        NSURL *url = [NSURL URLWithString:str];
        NSError* error = nil;
        NSData* data = [NSData dataWithContentsOfURL:url options:NSDataReadingUncached error:&error];
        if (error) {
            NSLog(@"%@", [error localizedDescription]);
        } else {
            NSLog(@"Data has loaded successfully.");
        }
        img.image = [UIImage imageWithData: data];
        lblUserNmae.text = [dict valueForKey:@"Title"];
        
        return cell1;
    }
    
}

-(void)btnSignInPressed{
    
    [[NSUserDefaults standardUserDefaults ]setObject:@"0" forKey:USER_id];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SignIn" bundle:nil];
    LoginViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    rootViewController.IsCommingFromSideMenu=1;
    [navController setViewControllers: @[rootViewController] animated: YES];
    [self.revealViewController setFrontViewController:navController];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell     forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==0)
    {
        
    }
    else
    {
        NSDictionary *dict = [menuObjects objectAtIndex:indexPath.row];
        
        if( [[dict valueForKey:@"Keys"] caseInsensitiveCompare:@"static"] == NSOrderedSame ) {
            
            NSString * Title = [dict valueForKey:@"Title"];
            if ([Title isEqualToString:@"Sell"]) {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                consignInitialViewController *consign = [storyboard instantiateViewControllerWithIdentifier:@"consignInitialViewController"];
                [navController setViewControllers: @[consign] animated: YES];
                [self.revealViewController setFrontViewController:navController];
                [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
                
                return;
            }
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            CommonWebviewViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"CommonWebviewViewController"];
            rootViewController.url = [NSURL URLWithString:[dict valueForKey:@"Url"]];
            rootViewController.title = [dict valueForKey:@"Title"];
            [navController setViewControllers: @[rootViewController] animated: YES];
            [self.revealViewController setFrontViewController:navController];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
            
        } else if( [[dict valueForKey:@"Keys"] caseInsensitiveCompare:@"categories"] == NSOrderedSame ) {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            CategoryListSidePageViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"CategoryListSidePageViewController"];
            rootViewController.arrCategory = subcategoryData;
            [navController setViewControllers: @[rootViewController] animated: YES];
            [self.revealViewController setFrontViewController:navController];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        }
        //
    }
    
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0){
    }
    else if (buttonIndex == 1){
    }
}

-(void)CloseMenu
{
    [self.revealViewController revealToggle:nil];
}
@end
