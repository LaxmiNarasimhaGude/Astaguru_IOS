//
//  ClsSetting.m
//  My Ghar Seva
//
//  Created by sumit mashalkar on 30/01/16.
//  Copyright © 2016 winjit. All rights reserved.
//

#import "ClsSetting.h"
#import <QuartzCore/QuartzCore.h>
#import "SWRevealViewController.h"
#import "AFNetworking.h"
#import "MBProgressHUD.h"
#import "BforeLoginViewController.h"
#import "AfterLoginViewController.h"
#import "SearchViewController.h"
#import "NotificationViewController.h"
@implementation ClsSetting
{
    
}

+ (NSString *)urlType{
    //urlType is one of the Live, Infomanav, Demo
    return @"Live";
//    return @"Demo";
}

+(NSString *)filterURL
{
//    return @"https://www.astaguru.com:83/api/FilterbyArtistid?";
    return @"https://demoapi.astaguru.com/api/FilterbyArtistid?";
}

+(NSString *)currentAuctionList
{
    return @"https://demoapi.astaguru.com/api/";
//     return @"https://www.astaguru.com:83/api/";
}

+(NSString *)tableURL
{
    if ([[ClsSetting urlType] isEqualToString:@"Live"])
    {
        //Live
      // return @"http://restapi.infomanav.com/api/v2/guru/_table/";
        return @"";
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Demo"])
    {
        //Demo
         return @"http://restapi.infomanav.com/api/v2/asta/_table/";
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Infomanav"])
    {
        //Infomanav
        return @"http://restapi.infomanav.com/api/v2/astauat/_table/";
    }
    else
    {
        return @"";
    }
}

+(NSString *)saveDeviceURL {
    return @"https://demoapi.astaguru.com/api/SaveDevice";
//    return @"https://www.astaguru.com:83/api/SaveDevice";
}

+(NSString *)getCategoryUrl {
    return @"https://demoapi.astaguru.com/api/CategoryForConsign";
//    return @"https://www.astaguru.com:83/api/CategoryForConsign";
}

+(NSString *)postConsignUrl {
    return @"https://demoapi.astaguru.com/api/SaveConsign";
//    return @"https://www.astaguru.com:83/api/SaveConsign";
}

+(NSString *)notificationListURL {
//    return @"https://www.astaguru.com:83/api/GetNotificationById";
    return @"https://demoapi.astaguru.com/api/GetNotificationById";
}

+(NSString *)menuListURL {
//    return @"https://www.astaguru.com:83/api/Menu";
       return @"https://demoapi.astaguru.com/api/Menu";
}


+(NSString *)saveBidURL {
    return @"Savebid";
}

+(NSString *)proxyBidURL {
    return @"Saveproxybid";
}

+(NSString *)sendNotificationURL {
    return @"SendNotification";
}

+(NSString *)procedureURL
{
    if ([[ClsSetting urlType] isEqualToString:@"Live"])
    {
        //Live
//        return @"https://www.astaguru.com:83/api/UpcomingSlider?Auctionid=";
        return @"https://demoapi.astaguru.com/api/UpcomingSlider?Auctionid=";
        
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Demo"])
    {
        //Demo
        return @"http://restapi.infomanav.com/api/v2/asta/_proc";
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Infomanav"])
    {
        //Infomanv
        return @"http://restapi.infomanav.com/api/v2/astauat/_proc";
    }
    else
    {
        return @"";
    }
}
+(NSString *)upcomingOnclickURL
{
    if ([[ClsSetting urlType] isEqualToString:@"Live"])
    {
        //Live
        
        return @"https://demoapi.astaguru.com/api/GetHomeBanner";
        
//        return @"https://www.astaguru.com:83/api/GetHomeBanner";
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Demo"])
    {
        //Demo
        return @"https://demoapi.astaguru.com/api/GetHomeBanner";
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Infomanav"])
    {
        //Infomanv
        return @"http://restapi.infomanav.com/api/v2/astauat/_proc";
    }
    else
    {
        return @"";
    }
}
+(NSString *)searchURL
{
    if ([[ClsSetting urlType] isEqualToString:@"Live"])
    {
        //Live
//       return @"https://www.astaguru.com:83/api/Search?key";
        return @"https://demoapi.astaguru.com/api/Search?key";
   
        
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Demo"])
    {
        //Demo
        return @"https://www.astaguru.com:83/api/UpcomingSlider?Auctionid=";
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Infomanav"])
    {
        //Infomanv
        return @"http://restapi.infomanav.com/api/v2/astauat/_proc";
    }
    else
    {
        return @"";
    }
}
+(NSString *)loginURL
{
    if ([[ClsSetting urlType] isEqualToString:@"Live"])
    {
        //Live
//       return @"https://www.astaguru.com:83/api/UserLogin?";
        return @"https://demoapi.astaguru.com/api/UserLogin?";
        
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Demo"])
    {
        //Demo
        return @"https://demoapi.astaguru.com/api/UserLogin?";
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Infomanav"])
    {
        //Infomanv
        return @"https://demoapi.astaguru.com/api/UserLogin?";
    }
    else
    {
        return @"";
    }
}
+(NSString *)defaultURL
{
    if ([[ClsSetting urlType] isEqualToString:@"Live"])
    {
        //Live
        return @"https://demoapi.astaguru.com/api/";
//        return @"https://www.astaguru.com:83/api/";
       
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Demo"])
    {
        //Demo
        return @"c255e4bd10c8468f9e7e393b748750ee108d6308e2ef3407ac5d2b163a01fa37";
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Infomanav"])
    {
        //Infomanv
        return @"b044ea92e3e2a8df97dfcf47c8d7bf950e3078571859786903bb803b05e70901";
    }
    else
    {
        return @"";
    }
}
+(NSString *)apiKey
{
    if ([[ClsSetting urlType] isEqualToString:@"Live"])
    {
        //Live
//        return @"c6935db431c0609280823dc52e092388a9a35c5f8793412ff89519e967fd27ed";
        return @"c255e4bd10c8468f9e7e393b748750ee108d6308e2ef3407ac5d2b163a01fa37";
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Demo"])
    {
        //Demo
        return @"c255e4bd10c8468f9e7e393b748750ee108d6308e2ef3407ac5d2b163a01fa37";
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Infomanav"])
    {
        //Infomanv
        return @"b044ea92e3e2a8df97dfcf47c8d7bf950e3078571859786903bb803b05e70901";
    }
    else
    {
        return @"";
    }
}

+(NSString *)autionAnalysisURL
{
    if ([[ClsSetting urlType] isEqualToString:@"Live"])
    {
        //live
        return @"http://astaguru.com/auction-analysis-mobile.aspx?astaguruauction=";
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Demo"])
    {
        //Demo
        return @"http://astaguru.com:81/auction-analysis-mobile.aspx?astaguruauction=";
    }
    else if ([[ClsSetting urlType] isEqualToString:@"Infomanav"])
    {
        //Infomanv
        return @"http://infomanav.com/auction-analysis-mobile.aspx?astaguruauction=";
    }
    else
    {
        return @"";
    }
}

+(NSString *)emailURL
{
    //    return @"https://www.astaguru.com:83/api/EmailSend";
        return @"http://newapi.astaguru.com:82/api/EmailSend";
}

+(NSString *)smsURL
{
  
//    return @"http://api.msg91.com/api/sendhttp.php?authkey=338500AD9H4VOHQl5f3135e8P1&mobiles=%@&message=%@&sender=ASTGUR&route=4&country=91";
    return @"http://api.msg91.com/api/sendhttp.php?authkey=338500AD9H4VOHQl5f3135e8P1&mobiles=%@&message=%@&sender=ASTGUR&route=4&DLT_TE_ID=%@";
}


+(NSString *)imageURL
{
    //return @"http://arttrust.southeastasia.cloudapp.azure.com/";
    return @"https://www.astaguru.com/";
}

+(NSString*)profileAPIURL
{
    return @"https://demoapi.astaguru.com/api/updateProfile";
//    return @"https://www.astaguru.com:83/api/updateProfile";
}

+(NSString*)profileImagesURL
{
    //return @"http://astanew.infomanav.com/Content/";
    return @"http://newapi.astaguru.com:83/Content/";
}

+ (BOOL) validatePanCardNumber: (NSString *) cardNumber {
    NSString *emailRegex = @"^[A-Z]{5}[0-9]{4}[A-Z]$";
    NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [cardTest evaluateWithObject:cardNumber];
}

+(CGFloat)CalculateHeightOfTextview:(UITextView *)TxtVw
{
    NSString *strDescrption=TxtVw.text;
    
    if ([strDescrption isEqualToString:@""] || [strDescrption isEqualToString:@"null"]|| [strDescrption isEqualToString:@"<null>"])
    {
        return 20.0;
    }
    else
    {
        
        CGFloat fixedWidth = TxtVw.frame.size.width;
        CGSize newSize = [TxtVw sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        CGRect newFrame = TxtVw.frame;
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
        TxtVw.frame = newFrame;
        if (TxtVw.frame.size.height<30.0)
        {
            return 50.0;
        }
        //return expectedLabelSize.height+40;
        return TxtVw.frame.size.height;
    }
    
}

+(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(NSString*)TrimWhiteSpaceAndNewLine:(NSString*)strString
{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmedString = [strString stringByTrimmingCharactersInSet:whitespace];
    return trimmedString;
}


+(void)underline:(UIView*)textField
{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.6;
    border.borderColor = [UIColor colorWithRed:196.0/255.0 green:196.0/255.0 blue:196.0/255.0 alpha:1.0].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    textField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
}



+(void)SetBorder:(UIView *)viw cornerRadius:(CGFloat)CornerRadius borderWidth:(CGFloat)borderWidth color:(UIColor*)color
{
    
//    UIColor *lightgray= [UIColor colorWithRed:200.0/255 green:200.0/255 blue:200.0/255 alpha:1.0];
    //[[viw layer] setBorderWidth:1.0f];
    [[viw layer] setBorderColor:color.CGColor];
    viw.layer.cornerRadius=CornerRadius;
    viw.layer.masksToBounds=YES;
    //viw.layer.borderColor=(__bridge CGColorRef _Nullable)(BorderColor);
    viw.layer.borderWidth= borderWidth;
    if ([viw isKindOfClass:[UITextField class]])
    {
        viw.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    }
}


+(NSString*)getAddress:(CLLocation*)newLocation
{
    CLLocation *currentLocation = newLocation;
    CLGeocoder* geocoder = [CLGeocoder new];
    __block CLPlacemark* placemark;
    
   __block NSString *strAddress=@"";
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         //NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             
             // strAdd -> take bydefault value nil
             NSString *strAdd = @"";
             
             if ([placemark.subThoroughfare length] != 0)
                 strAdd = placemark.subThoroughfare;
             
             if ([placemark.thoroughfare length] != 0)
             {
                 // strAdd -> store value of current location
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark thoroughfare]];
                 else
                 {
                     // strAdd -> store only this value,which is not null
                     strAdd = placemark.thoroughfare;
                 }
             }
             
             if ([placemark.postalCode length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark postalCode]];
                 else
                     strAdd = placemark.postalCode;
             }
             
             if ([placemark.locality length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark locality]];
                 else
                     strAdd = placemark.locality;
             }
             
             if ([placemark.administrativeArea length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark administrativeArea]];
                 else
                     strAdd = placemark.administrativeArea;
             }
             
             if ([placemark.country length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark country]];
                 else
                     strAdd = placemark.country;
             }
            
                    strAddress=strAdd;
             
             
         }
         
     }];
    NSLog(@"%@",strAddress);
   return strAddress;
}

+(void)internetConnectionPromt
{
    [[[[iToast makeText:@"Please check internet connection"] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal]show];
}

+(void)ValidationPromt:(NSString*)strValidationText
{
    [[[[iToast makeText:strValidationText] setGravity:iToastGravityCenter] setDuration:iToastDurationNormal]show];
}

-(void)callGetWeb:(NSMutableDictionary*)dict url:(NSString*)strURL view:(UIView*)callingview
{
    @try {
        
        MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:callingview animated:YES];
        HUD.labelText = @"loading";

        NSMutableDictionary *Discparam=[[NSMutableDictionary alloc]init];
        Discparam=dict;
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];  //AFHTTPResponseSerializer serializer
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
       
        NSString  *strQuery=[NSString stringWithFormat:@"%@%@",[ClsSetting tableURL],strURL];
        NSString *url = strQuery;
        NSLog(@"%@",url);
    
        NSString *encoded = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [manager GET:encoded parameters:Discparam success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             //  NSError *error=nil;
             //NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             //NSLog(@"%@",responseStr);

            [MBProgressHUD hideHUDForView:callingview animated:YES];
             NSError *error;
             NSMutableArray *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
             NSLog(@"%@",responseDict);
             [_passResponseDataDelegate passGetResponseData:responseDict];
             
             

         }
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"Error: %@", error);
                  [MBProgressHUD hideHUDForView:callingview animated:YES];
              }];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
    }
}

-(void)callDeleteWeb:(NSMutableDictionary*)dict url:(NSString*)strURL view:(UIView*)callingview
{
    @try {
        MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:callingview animated:YES];
        HUD.labelText = @"loading";
        
        
        NSMutableDictionary *Discparam=[[NSMutableDictionary alloc]init];
        Discparam=dict;
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];  //AFHTTPResponseSerializer serializer
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        
        NSString  *strQuery=[NSString stringWithFormat:@"%@%@",[ClsSetting tableURL],strURL];
        NSString *url = strQuery;
        NSLog(@"%@",url);
        NSString *encoded = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [manager GET:encoded parameters:Discparam success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             //  NSError *error=nil;
             //NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             //NSLog(@"%@",responseStr);

             NSError *error;
             NSMutableArray *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
             NSLog(@"%@",responseDict);
             [_passResponseDataDelegate passGetResponseData:responseDict];
             
             [MBProgressHUD hideHUDForView:callingview animated:YES];
            [MBProgressHUD hideHUDForView:callingview animated:YES];
         }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
                 [MBProgressHUD hideHUDForView:callingview animated:YES];
             }];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
    }
}


-(void)callPostWeb:(NSDictionary*)dict url:(NSString*)strURL view:(UIView*)callingview
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:callingview animated:YES];
    HUD.labelText = @"loading";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSLog(@"%@",strURL);
    NSLog(@"Dict %@",dict);
    
    AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = serializer;
    
    [manager POST:strURL parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [_passResponseDataDelegate passPostResponseData:responseObject];
        [MBProgressHUD hideHUDForView:callingview animated:YES];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:callingview animated:YES];
    }];
}


-(void)callPutWeb:(NSDictionary*)dict url:(NSString*)strURL view:(UIView*)callingview
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:callingview animated:YES];
    HUD.labelText = @"loading";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSLog(@"Dict %@",dict);
    
    AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = serializer;
    
    //[manager PUT:[NSString stringWithFormat:@"%@users?api_key=%@",[ClsSetting tableURL],[ClsSetting apiKey]] parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
    [manager PUT:strURL parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"JSON: %@", responseObject);
//        NSError *error=nil;
        //NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        
//         NSError *error;
//          NSMutableDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
//         NSLog(@"%@",responseDict);
        
        [_passResponseDataDelegate passPostResponseData:responseObject];
        
        [MBProgressHUD hideHUDForView:callingview animated:YES];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [MBProgressHUD hideHUDForView:callingview animated:YES];
        
    }];
    
    
}

-(void)sendSMSOTP:(NSDictionary*)dict url:(NSString*)strURL view:(UIView*)Callingview
{
    
    @try {
        
        
       // MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:Callingview animated:YES];
        //HUD.labelText = @"loading";
        
        
        NSDictionary *Discparam=[[NSDictionary alloc]init];
        Discparam=dict;
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];  //AFHTTPResponseSerializer serializer
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        
        NSString  *strQuery=[NSString stringWithFormat:@"%@",strURL];
        NSString *url = strQuery;
        NSLog(@"%@",url);
        
        
        
        NSString *encoded = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [manager GET:encoded parameters:Discparam success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             //  NSError *error=nil;
             NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             
             NSError *error;
             NSMutableArray *responseDict = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
             NSLog(@"%@",responseStr);
             NSLog(@"%@",responseDict);
             
             [_passResponseDataDelegate passPostResponseData:responseDict];
             
            // [MBProgressHUD hideHUDForView:Callingview animated:YES];
             
             
         }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
                 
                // [MBProgressHUD hideHUDForView:Callingview animated:YES];
             }];
        
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
    }
}

+(NSMutableDictionary*)RemoveNull:(NSMutableDictionary*)dict
{
    NSArray*arrkeys=[dict allKeys];
    dict=[dict mutableCopy];
    for (int i=0; i<arrkeys.count;i++ )
    {
        if (([dict valueForKey:[arrkeys objectAtIndex:i]]== nil)||([dict valueForKey:[arrkeys objectAtIndex:i]]== [NSNull null]))
        {
           [dict setValue:@" " forKey:[arrkeys objectAtIndex:i]];
        }
        
        NSString *str=[NSString stringWithFormat:@"%@",[dict valueForKey:[arrkeys objectAtIndex:i]]];
        
        NSLog(@"%@",[dict valueForKey:[arrkeys objectAtIndex:i]]);
        if ([str intValue] == 0)
        {
           str = [str stringByReplacingOccurrencesOfString: @"<br>" withString: @"\n"];
             [dict setValue:str forKey:[arrkeys objectAtIndex:i]];
        }
    }
    return dict;
}

+(NSMutableDictionary*)RemoveNullOnly:(NSMutableDictionary*)dict
{
    NSArray*arrkeys=[dict allKeys];
    dict=[dict mutableCopy];
    for (int i=0; i<arrkeys.count;i++ )
    {
        if (([dict valueForKey:[arrkeys objectAtIndex:i]]== nil)||([dict valueForKey:[arrkeys objectAtIndex:i]]== [NSNull null]))
        {
            [dict setValue:@" " forKey:[arrkeys objectAtIndex:i]];
        }
    }
    return dict;
}

+(void)myAstaGuru:(UINavigationController*)NavigationController
{
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:USER_id] intValue]>0)
    {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SignIn" bundle:nil];
        AfterLoginViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"AfterLoginViewController"];
        [NavigationController pushViewController:rootViewController animated:YES];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SignIn" bundle:nil];
        BforeLoginViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"BforeLoginViewController"];
        [NavigationController pushViewController:rootViewController animated:YES];
    }
}
+(void)myAstaGuru1:(UINavigationController*)NavigationController
{
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:USER_id] intValue]>0)
    {
        
        UIStoryboard *storyBoard=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NotificationViewController *rootViewController = [storyBoard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
        [NavigationController pushViewController:rootViewController animated:YES];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SignIn" bundle:nil];
        BforeLoginViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"BforeLoginViewController"];
        [NavigationController pushViewController:rootViewController animated:YES];
    }
}
+(void)Searchpage:(UINavigationController*)NavigationController
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SearchViewController *VCLikesControll = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
    [NavigationController pushViewController:VCLikesControll animated:YES];
}

+(NSMutableAttributedString*)getAttributedStringFormHtmlString:(NSString*)htmlString
{
    return  [[NSMutableAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
}

+(NSString*)getStringFormHtmlString:(NSString*)htmlString
{
    NSRange r;
    while ((r = [htmlString rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location     != NSNotFound)
    {
        htmlString = [htmlString stringByReplacingCharactersInRange:r withString:@""];
    }
    return htmlString;
}

+(CGFloat)heightForNSString:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font
{
    CGSize size = CGSizeZero;
    if (text)
    {
        CGRect rect = [text boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:font } context:nil];
        size = CGSizeMake(rect.size.width, rect.size.height);
    }
    return size.height;
}

+(CGFloat)heightForNSAttributedString:(NSAttributedString*)astr havingWidth:(CGFloat)width
{
    CGRect rect = [astr boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    return rect.size.height;
}


+(void)sendEmailWithInfo:(NSDictionary*)infoDic
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.securityPolicy.validatesDomainName = YES;
    manager.securityPolicy.validatesCertificateChain = YES;
    AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = serializer;
    NSLog(@"Dict %@",infoDic);
    [manager POST:[ClsSetting emailURL] parameters:infoDic success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
//    NSString *url = [ClsSetting emailURL];
//
//    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
//
//    [urlRequest setHTTPMethod:@"POST"];
//
//    NSError *error =nil;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:infoDic
//                                                           options:NSJSONWritingPrettyPrinted error:&error];
//
//    [urlRequest setHTTPBody:jsonData];
//    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//        if(httpResponse.statusCode == 200)
//        {
//            NSError *parseError = nil;
//            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
//            NSLog(@"The response is - %@",responseDictionary);
//            NSInteger success = [[responseDictionary objectForKey:@"success"] integerValue];
//            if(success == 1)
//            {
//                NSLog(@"Login SUCCESS");
//            }
//            else
//            {
//                NSLog(@"Login FAILURE");
//            }
//        }
//        else
//        {
//            NSLog(@"Error");
//        }
//    }];
//    [dataTask resume];
    
    
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler{
  if([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]){
    if([challenge.protectionSpace.host isEqualToString:@"astaguru.com"]){
      NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
      completionHandler(NSURLSessionAuthChallengeUseCredential,credential);
    }
  }
}

+(void)ISUSerLeading:(NSString*)strUserID Cell:(CurrentDefultGridCollectionViewCell*)cell
{
    if (([[[NSUserDefaults standardUserDefaults] valueForKey:USER_id] intValue]>0))
    {
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:USER_id] intValue] == [strUserID intValue])
        {
            [cell.btnLot setBackgroundImage:[UIImage imageNamed:@"img-lotno-bg2"] forState:UIControlStateNormal];
        }
        else
        {
            [cell.btnLot setBackgroundImage:[UIImage imageNamed:@"img-lotno-bg1.png"] forState:UIControlStateNormal];
        }
    }
    
}

@end
