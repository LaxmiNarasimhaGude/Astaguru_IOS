//
//  ClsSetting.h
//  My Ghar Seva
//
//  Created by sumit mashalkar on 30/01/16.
//  Copyright © 2016 winjit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "iToast.h"
#import "parese.h"
#import "CustomTextfied.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "CurrentDefultGridCollectionViewCell.h"

#define USER_id         @"userid"
#define USER_NAME      @"username"
#define USER_nicName   @"nickname"
#define USER_FirstName   @"name"
#define USER_LastName   @"lastname"


//#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@protocol PassResponse
@optional
//-(void)passReseposeData:(id)arr;
//-(void)passReseposeData1:(id)str;
-(void)passGetResponseData:(id)responseObject;
-(void)passPostResponseData:(id)responseObject;
@end

typedef enum {
    PanCard,
    AadharCard,
    None
} ImagePikerType;


@interface ClsSetting : NSObject

@property (nonatomic, assign) NSString *urlType;

+(CGFloat)CalculateHeightOfTextview:(UITextView *)TxtVw;
+ (BOOL) validatePanCardNumber: (NSString *) cardNumber;
+(void)underline:(UIView*)textField;
+(NSString*)TrimWhiteSpaceAndNewLine:(NSString*)strString;
+(BOOL) NSStringIsValidEmail:(NSString *)checkString;
+(void)internetConnectionPromt;
+(void)ValidationPromt:(NSString*)strValidationText;
+(void)SetBorder:(UIView *)viw cornerRadius:(CGFloat)CornerRadius borderWidth:(CGFloat)borderWidth color:(UIColor*)color;
+(NSString*)getAddress:(CLLocation*)newLocation;
//+(CGFloat)heightOfTextForString:(NSString *)aString andFont:(UIFont *)aFont maxSize:(CGSize)aSize;

+(CGFloat)heightForNSString:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font;
+(CGFloat)heightForNSAttributedString:(NSAttributedString*)astr havingWidth:(CGFloat)width;

@property (readwrite) id<PassResponse> passResponseDataDelegate;

//+(void)setComments:(NSMutableArray*)arrComments label:(UILabel*)Title;
-(void)callGetWeb:(NSMutableDictionary*)dict url:(NSString*)strURL view:(UIView*)callingview;
-(void)callPostWeb:(NSDictionary*)dict url:(NSString*)strURL view:(UIView*)callingview;
-(void)callPutWeb:(NSDictionary*)dict url:(NSString*)strURL view:(UIView*)callingview;
-(void)callDeleteWeb:(NSMutableDictionary*)dict url:(NSString*)strURL view:(UIView*)callingview;
-(void)sendSMSOTP:(NSDictionary*)dict url:(NSString*)strURL view:(UIView*)callingview;
+(NSMutableDictionary*)RemoveNull:(NSMutableDictionary*)dict;
+(NSMutableDictionary*)RemoveNullOnly:(NSMutableDictionary*)dict;
//-(void)calllPostWeb2:(NSDictionary*)dict url:(NSString*)strURL view:(UIView*)Callingview;
+(void)myAstaGuru:(UINavigationController*)NavigationController;
+(void)myAstaGuru1:(UINavigationController*)NavigationController;
+(NSMutableAttributedString*)getAttributedStringFormHtmlString:(NSString*)htmlString;
+(NSString*)getStringFormHtmlString:(NSString*)htmlString;
+(void)Searchpage:(UINavigationController*)NavigationController;

+(NSString *)urlType;
+(NSString *)filterURL;
+(NSString *)saveDeviceURL;
+(NSString *)notificationListURL;
+(NSString *)menuListURL;
+(NSString *)currentAuctionList;
+(NSString *)tableURL;
+(NSString *)imageURL;
+(NSString *)saveBidURL;
+(NSString *)proxyBidURL;
+(NSString *)procedureURL;
+(NSString *)upcomingOnclickURL;
+(NSString *)searchURL;
+(NSString *)loginURL;
+(NSString *)defaultURL;
+(NSString *)apiKey;
+(NSString *)autionAnalysisURL;
+(NSString *)emailURL;
+(NSString *)smsURL;
+(NSString*)profileAPIURL;
+(NSString*)profileImagesURL;
+(NSString *)sendNotificationURL;
+(NSString *)getCategoryUrl;
+(NSString *)postConsignUrl;

+(void)sendEmailWithInfo:(NSDictionary*)infoDic;
+(void)ISUSerLeading:(NSString*)strUserID Cell:(CurrentDefultGridCollectionViewCell*)cell;
@end
