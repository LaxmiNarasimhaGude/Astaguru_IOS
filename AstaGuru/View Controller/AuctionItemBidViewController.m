//
//  AuctionItemBidViewController.m
//  AstaGuru
//
//  Created by Aarya Tech on 28/09/16.
//  Copyright © 2016 Aarya Tech. All rights reserved.
//

#import "AuctionItemBidViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ClsSetting.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import <sys/utsname.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <CoreLocation/CoreLocation.h>
#import "ALNetwork.h"
#import "AppDelegate.h"
#import "NotifiactionManger.h"

#define SYSTEM_VERSION_LESS_THAN(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface AuctionItemBidViewController ()<PassResponse, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource,UIApplicationDelegate>
{
    NSString *strProxyPriceus;
    NSString *strProxyPricers;
//    NSString *strNextVAlidPriceus;
//    NSString *strNextVAlidPricers;
    NSString *strNextVAlidPriceus_send;
    NSString *strNextVAlidPricers_send;
    
    NSString *strNextIncrmentRS;
    NSString *strNextIncrmentUS;

    
//    int webservicecount;
    int proxyvalidation;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    NSString *country;
    NSString *cityName;
    NSString *thoroughfare;
    NSString *address;
    NSString *lat;
    NSString *lang;
    
    MBProgressHUD *HUDL;
    
    NSArray *fiveBid;
    int isFiveBidShow;
    
//    NSInteger amountlimt = @0;
    NSInteger isOldUser;
    
    AppDelegate *appDelegate;
}
@end

@implementation AuctionItemBidViewController
@synthesize amountlimt;

- (void)viewDidLoad
{
    // Do any additional setup after loading the view.

    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _viwContentview.layer.cornerRadius = 5;
    _viwContentview.layer.masksToBounds = YES;
    self.scrKeytboard.keyboardDismissMode=UIScrollViewKeyboardDismissModeOnDrag;
    [_btnLodId setTitle:[NSString stringWithFormat:@"Lot:%@",_objCurrentOuction.strReference] forState:UIControlStateNormal];
    [_btnLot setTitle:[NSString stringWithFormat:@"Lot:%@",_objCurrentOuction.strReference] forState:UIControlStateNormal];
    
    self.tableView_FiveBid.hidden = YES;
    self.tableView_FiveBid_Height.constant = 0;
    
    self.btn_fiveBid.hidden = YES;
    self.btn_fiveBid_Height.constant = 0;
    
    if (_isBidNow == YES)
    {
        _viwProxyBid.hidden=YES;
        _viwBidNow.hidden=NO;
    }
    else
    {
        _viwBidNow.hidden=YES;
        _viwProxyBid.hidden=NO;
        if (_IsUpcoming != 1)
        {
            self.btn_fiveBid.hidden = NO;
            self.btn_fiveBid_Height.constant = 25;
            fiveBid = [self getNextFiveBid];
            
            isFiveBidShow = 0;
            
            self.tableView_FiveBid.hidden =  NO;
            self.tableView_FiveBid.delegate = self;
            self.tableView_FiveBid.dataSource = self;
        }
    }
    
    if (_IsUpcoming == 1)
    {
        _lblbidtitle.text = @"Opening Bid"; //@"Start Price";
    }
    
    if(_isBidNow==FALSE){
        [self setPrice];//:_objCurrentOuction];
    }
    else{
        [self setPrice1];
    }
    
//    [self setPrice];//:_objCurrentOuction];
    
    cityName = @"";
    
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"amountlimt"] != nil)
    {
        id _amountlimt = [[NSUserDefaults standardUserDefaults]valueForKey:@"amountlimt"];
        if ([_amountlimt isKindOfClass:[NSString class]])
        {
            amountlimt = [_amountlimt integerValue];
        }
        else if ([_amountlimt isKindOfClass:[NSNumber class]])
        {
            amountlimt = [_amountlimt integerValue];
        }
        else
        {
            amountlimt = 0;
        }
    }
    else
    {
        amountlimt = 0;
    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"isOldUser"] != nil)
    {
        id _isOldUser = [[NSUserDefaults standardUserDefaults]valueForKey:@"isOldUser"];
        if ([_isOldUser isKindOfClass:[NSString class]])
        {
            isOldUser = [_isOldUser integerValue];
        }
        else if ([_isOldUser isKindOfClass:[NSNumber class]])
        {
            isOldUser = [_isOldUser integerValue];
        }
        else
        {
            isOldUser = 0;
        }
    }
    else
    {
        isOldUser = 0;
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:true];
    [self CurrentLocationIdentifier];
}

-(NSInteger) getNextValidBidWith:(NSInteger)currentBidPrice percentValue:(NSInteger)percent
{
    NSInteger nextValidBidPrice = 0;
    NSInteger priceIncreaseRate = 0;
    
    priceIncreaseRate = (currentBidPrice*percent)/100;
    
    nextValidBidPrice = currentBidPrice + priceIncreaseRate;
    
    return nextValidBidPrice;
}

-(NSArray *) getNextFiveBid
{
    NSString *strPriceUS = [NSString stringWithFormat:@"%@",_objCurrentOuction.strpriceus];
    NSInteger priceUS = [strPriceUS integerValue];
    
    NSString *strPriceRS = [NSString stringWithFormat:@"%@",_objCurrentOuction.strpricers];
    NSInteger priceRS = [strPriceRS integerValue];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setMaximumFractionDigits:0];
    
    NSInteger currentBidPrice = 0;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
    {
        numberFormatter.currencyCode = @"USD";
        currentBidPrice = priceUS;
    }
    else
    {
        numberFormatter.currencyCode = @"INR";
        currentBidPrice = priceRS;
    }
    
    NSInteger priceIncreaseRate = 0;
    if (priceRS >= 10000000)
    {
        priceIncreaseRate = 6;
    }
    else
    {
        priceIncreaseRate = 15;
    }
    
    currentBidPrice = [self getNextValidBidWith:currentBidPrice percentValue:priceIncreaseRate];
    
    NSMutableArray *nextFiveBid_arr = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 5; i++)
    {
        NSInteger nextValidBidPrice = [self getNextValidBidWith:currentBidPrice percentValue:priceIncreaseRate];
        currentBidPrice = nextValidBidPrice;
        NSString *strNextValidBidPrice = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:nextValidBidPrice]];
        [nextFiveBid_arr addObject:strNextValidBidPrice];
    }
    
    return nextFiveBid_arr;
}


//------------ Current Location Address-----
-(void)CurrentLocationIdentifier
{
    //---- For getting current gps location
    locationManager = [CLLocationManager new];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.delegate = self;
    [locationManager requestAlwaysAuthorization];
    [locationManager startUpdatingLocation];
    //------
}

- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            [locationManager requestAlwaysAuthorization];
            [locationManager startUpdatingLocation];
        } break;
        case kCLAuthorizationStatusDenied:
        case kCLAuthorizationStatusRestricted:
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Location Services Disabled" message:@"Please enable location services in settings" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            if (status == kCLAuthorizationStatusDenied)
            {
                UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                                 {
                                                     
//                                                     if ([CLLocationManager locationServicesEnabled])
//                                                     {
                                                         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                         
//                                                     }
//                                                     else
//                                                     {
//                                                         NSString* url = SYSTEM_VERSION_LESS_THAN(@"10.0") ? @"prefs:root=LOCATION_SERVICES" : @"App-Prefs:root=Privacy&path=LOCATION";
//                                                         [[UIApplication sharedApplication] openURL:[NSURL URLWithString: url]];
//                                                     }
                                                 }];
                [alertController addAction:settingsAction];
            }
            else
            {
                UIAlertAction *settingsAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                }];
                [alertController addAction:settingsAction];
                
            }
            
            
            [alertController addAction:cancelAction];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways: {
            HUDL = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            HUDL.labelText = @"wait";
            [locationManager startUpdatingLocation]; 
        } break;
        default:
            break;
    }
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    //[locationManager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (!(error))
         {
             NSLog(@"placemarks == %@",placemarks);
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
//
//             NSLog(@"pm.thoroughfare = %@",placemark.thoroughfare);
//
//             NSLog(@"\nCurrent Location Detected\n");
//             NSLog(@"placemark %@",placemark);
             NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
             NSString *Address = [[NSString alloc]initWithString:locatedAt];
//             NSString *Area = [[NSString alloc]initWithString:placemark.locality];
//             NSString *Country = [[NSString alloc]initWithString:placemark.country];
//             NSString *CountryArea = [NSString stringWithFormat:@"%@, %@", Area,Country];
//             NSLog(@"Address = %@ , CountryArea =  %@",Address, CountryArea);
             country = [[NSString alloc]initWithString:placemark.country];
             cityName = placemark.locality;
             thoroughfare = @"";
           
             if ([placemark.thoroughfare length] != 0)
             {
                 // strAdd -> store value of current location
                 if ([thoroughfare length] != 0)
                     thoroughfare = [NSString stringWithFormat:@"%@, %@",thoroughfare,[placemark thoroughfare]];
                 else
                 {
                     // strAdd -> store only this value,which is not null
                     thoroughfare = placemark.thoroughfare;
                 }
             }
         
             
             
             
             
             address = Address;
             lat = [NSString stringWithFormat:@"%f", currentLocation.coordinate.latitude];
             lang = [NSString stringWithFormat:@"%f", currentLocation.coordinate.longitude];
         }
         else
         {
             NSLog(@"Geocode failed with error %@", error);
             NSLog(@"\nCurrent Location Not Detected\n");
             //return;
             //CountryArea = NULL;
         }
         
         if (HUDL != nil)
         {
             [HUDL hide:YES];
         }
         
         
         /*---- For more results
          placemark.region);
          placemark.country);
          placemark.locality);
          placemark.name);
          placemark.ocean);
          placemark.postalCode);
          placemark.subLocality);
          placemark.location);
          ------*/
     }];
    

}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if (HUDL != nil)
    {
        [HUDL hide:YES];
    }
    [ClsSetting ValidationPromt:@"Location not found! Please try again."];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_txtProxyBid resignFirstResponder];
}

-(BOOL)validate
{
    if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtProxyBid.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Please Enter Proxy Bid Value"];
        return FALSE;
    }
    else if ([[ClsSetting TrimWhiteSpaceAndNewLine:_txtProxyBid.text] integerValue] == 0)
    {
        [ClsSetting ValidationPromt:@"Please Enter Proxy Bid Value"];
        return FALSE;
    }
    if (isOldUser == 0)
    {
        if (amountlimt == 0)
        {
            [ClsSetting ValidationPromt:@"Your bid limit is zero please contact to administrator"];
            return FALSE;
        }
        else if (amountlimt <= [[ClsSetting TrimWhiteSpaceAndNewLine:_txtProxyBid.text] integerValue])
        {
            [ClsSetting ValidationPromt:@"Your bid limit is exceeded please contact to administrator"];
            return FALSE;
        }
    }
    else
    {
        BOOL result = [self validation1];
        if (result == false)
        {
//            if ([_txtProxyBid.text intValue] >= 10000000)
            if ([_objCurrentOuction.strpricers intValue] >= 10000000)
//
            {
                if(_IsUpcoming==1){
[ClsSetting ValidationPromt:@"Proxy Bid amount should be equal to or greater than 5% more than opening bid"];
                }
                else{
[ClsSetting ValidationPromt:@"Proxy Bid amount should be equal to or greater than 7% more than next valid Bid"];
                }
//                [ClsSetting ValidationPromt:@"Proxy Bid value must be higher by at least 7% of current price"];
                
            }
            else
            {
                NSLog(@"%@",_isBidNow);
                if (_IsUpcoming==1) {
                     [ClsSetting ValidationPromt:@"Proxy Bid amount should be equal to or greater than 10% more than opening bid"];
                }
                else{

                    [ClsSetting ValidationPromt:@"Proxy Bid amount should be equal to or greater than 20% more than next valid Bid"];
                }
//                [ClsSetting ValidationPromt:@"Proxy Bid value must be higher by at least 15% of current price."];
                
            }
        }
        return result;
    }
    return TRUE;
}

- (IBAction)btn_fivebid_pressed:(id)sender
{
    if (isFiveBidShow==0)
    {
        isFiveBidShow = 1;
        CGFloat height = 30;
        height *= fiveBid.count;
        self.tableView_FiveBid_Height.constant = height;
    }
    else
    {
        isFiveBidShow = 0;
        self.tableView_FiveBid_Height.constant = 0;
    }
}

- (IBAction)btnCancelPressed:(id)sender
{
    if((_isBidNow==FALSE)&&(proxyvalidation==2))
    {
        proxyvalidation=1;
        _viwProxyBidConfarmation.hidden=YES;
        _viwProxyBid.hidden=NO;
        _viwBidNow.hidden=YES;
        
    }
    else
    {
        [self.delegate cancelAuctionItemBidViewController];
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
    }
}


- (IBAction)btnConfirmPressed:(id)sender
{
    

        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        
        // 3
        if ((status == kCLAuthorizationStatusDenied)  || (status == kCLAuthorizationStatusRestricted))
        {
            [self CurrentLocationIdentifier];
            return;
        }

    
    if(currentLocation == nil || [cityName isEqualToString:@""])
    {
        [self CurrentLocationIdentifier];
        [ClsSetting ValidationPromt:@"Location not found! Please try again."];
        return;
    }
    
    if (_isBidNow==1)
    {
        if (proxyvalidation==1)
        {
            proxyvalidation=0;
            _viwProxyBidConfarmation.hidden=YES;
            _viwBidNow.hidden=NO;
            _viwProxyBid.hidden=YES;
            [_btnConfirm setTitle:@"Confirm" forState:UIControlStateNormal];
        }
        else if (proxyvalidation==2)
        {
            _viwProxyBidConfarmation.hidden=YES;
            _viwBidNow.hidden=NO;
            _viwProxyBid.hidden=YES;
            [_btnConfirm setTitle:@"Confirm" forState:UIControlStateNormal];
            [self getOccttionData];
        }
        else
        {
            [self getOccttionData];
        }
    }
    else
    {
        if ([self validate])
        {
            if (proxyvalidation==0)
            {
                proxyvalidation=1;
                _lblAlert.text=@"Once submitted can not be changed online. Confirm? ";
                _viwBidNow.hidden=YES;
                _viwProxyBid.hidden=YES;
                _viwProxyBidConfarmation.hidden=NO;
            }
            else if (proxyvalidation==2)
            {
                _viwBidNow.hidden=YES;
                _viwProxyBid.hidden=NO;
                _viwProxyBidConfarmation.hidden=YES;
                [self getOccttionData];
            }
            else
            {
                [self ProxyBid1];
            }
        }
    }
}


-(void)getOccttionData
{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
//    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"defaultlots?api_key=%@&filter=productid=%@",[ClsSetting apiKey],_objCurrentOuction.strproductid] view:self.view];
//    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"http://restapi.infomanav.com/api/v2/asta/_table/acution?api_key=%@&filter=productid=%@",[ClsSetting apiKey],_objCurrentOuction.strproductid] view:self.view];
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@/AuctionProductdetails_ios?productid=%@",[ClsSetting defaultURL],_objCurrentOuction.strproductid] view:self.view];
    
    

    objSetting.passResponseDataDelegate=self;
}

//-(void)passReseposeData:(id)arr
- (void)passGetResponseData:(id)responseObject
{
//    NSError *error;
//    NSMutableArray *dict1 = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
    NSLog(@"%@",responseObject);
    NSMutableArray  *arrItemCount=[parese parseSortCurrentAuction:[responseObject valueForKey:@"resource"]];
//    NSMutableArray  *arrItemCount=[responseObject valueForKey:@"resource"];

//    NSLog(@"%@",arrItemCount);
    //MARK:- JAY
//
//    proxyvalidation=0;
//    [self bidingNow];
    
    
    if (arrItemCount.count>0)
    {
        clsCurrentOccution *objCurrentOuction1=[arrItemCount objectAtIndex:0];
//          NSLog(@"%@",[objCurrentOuction1 strpricers ]);
//        NSLog(@"%@",[objCurrentOuction1 strpriceus ]);
        
        if (proxyvalidation==2)
        {
            _objCurrentOuction = objCurrentOuction1;
            [self setPrice];
//            [self setCurrenBidvalue];
            proxyvalidation = 0;
        }
        else
        {
//            NSLog(@"%@",objCurrentOuction1.strpricers);
//            NSLog(@"%@",_objCurrentOuction.strpricers);
            
            if ([objCurrentOuction1.strpricers intValue]>[_objCurrentOuction.strpricers intValue] )
            {
                _lblAlert.text=@"The bid value for this lot has change, update your bid?";
                [_btnConfirm setTitle:@"Ok" forState:UIControlStateNormal];
                _viwProxyBidConfarmation.hidden=NO;
                _viwBidNow.hidden=YES;
                _viwProxyBid.hidden=YES;
                proxyvalidation=1;
                _objCurrentOuction=objCurrentOuction1;
                [self setPrice];
//                [self setCurrenBidvalue];
            }
//            else if (isOldUser == 0)
//            {
//                if (amountlimt == 0)
////                {
////                    [ClsSetting ValidationPromt:@"Your bid limit is zero please contact to administrator"];
////                }
//                else if (amountlimt <= [objCurrentOuction1.strpricers integerValue])
////                {
////                    [ClsSetting ValidationPromt:@"Your bid limit is exceeded please contact to administrator"];
////                }
//else if (amountlimt >= [objCurrentOuction1.strpricers integerValue])
////{
////    proxyvalidation=0;
////    [self bidingNow];
////
////}
//
//            }
            else
            {
                proxyvalidation=0;
                [self bidingNow];
            }
        }
    }
}

-(void)calculateNextIncrementValueFromCurrentValueRS:(NSString*)currentValueRS currentValueUS:(NSString*)currentValueUS
{
    int price_rs = [currentValueRS intValue];
    int price_us = [currentValueUS intValue];
    
    if ([currentValueRS intValue] >= 10000000)
    {
        int priceIncreaserete_rs = (price_rs*5)/100;
        int FinalPrice_rs = price_rs + priceIncreaserete_rs;
        strNextIncrmentRS = [NSString stringWithFormat:@"%d",FinalPrice_rs];
        
        int priceIncreaserete_us = (price_us*5)/100;
        int FinalPrice_us = price_us + priceIncreaserete_us;
        strNextIncrmentUS = [NSString stringWithFormat:@"%d",FinalPrice_us];
    }
    else
    {
        int priceIncreaserete_rs = (price_rs*10)/100;
        int FinalPrice_rs = price_rs + priceIncreaserete_rs;
        strNextIncrmentRS = [NSString stringWithFormat:@"%d",FinalPrice_rs];

        
        int priceIncreaserete_us = (price_us*10)/100;
        int FinalPrice_us = price_us + priceIncreaserete_us;
        strNextIncrmentUS = [NSString stringWithFormat:@"%d",FinalPrice_us];
    }
}

-(void)saveDevice: (NSString*)userID {
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    if (deviceToken == nil)
    {
        deviceToken = @"";
    }
    NSString *strQuery=[NSString stringWithFormat:@"%@", [ClsSetting saveDeviceURL]];
    NSString *url = strQuery;
    NSLog(@"%@",url);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = serializer;
    NSDictionary *dict = @{
                                  @"userid": userID,
                                  @"device_id": deviceToken,
                                  @"device_type": @"iOS",
                                  };
    NSLog(@"Dict %@",dict);
    [manager POST:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"%@",responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
        
}





-(void)bidingNow
{
    NSLog(@"%d",proxyvalidation);
    NSString *strUserName;
    NSString *strUserid;
    NSString *strUsernicName;
    if([[NSUserDefaults standardUserDefaults] objectForKey:USER_NAME] != nil)
    {
        strUserName=[[NSUserDefaults standardUserDefaults]valueForKey:USER_NAME];
    }
    else
    {
        strUserName=@"abhi123";
    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:USER_id] != nil)
    {
        strUserid=[[NSUserDefaults standardUserDefaults]valueForKey:USER_id];
    }
    else
    {
        strUserid=@"1972";
    }
    if([[NSUserDefaults standardUserDefaults] objectForKey:USER_nicName] != nil)
       {
           strUsernicName=[[NSUserDefaults standardUserDefaults]valueForKey:USER_nicName];
       }
       else
       {
           strUsernicName=@"AnonymousUser";
       }
    @try {
        
        MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"loading";
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];  //AFHTTPResponseSerializer serializer
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        
        
        NSArray *imgNameArr = [_objCurrentOuction.strthumbnail componentsSeparatedByString:@"/"];
        NSString *imgName = [imgNameArr objectAtIndex:1];
        
//        bidByVal int,
//        @deviceTocken VARCHAR(255),
//        @OSversion VARCHAR(255),
//        @modelName VARCHAR(100),
//        @ipAddress VARCHAR(50),
//        @userLocation
        NSString *bidByVal = @"2";
//        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
//        if (deviceToken == nil)
//        {
//            deviceToken = @"";
//        }
        NSString *Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
        float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
        NSString *OSversion = [NSString stringWithFormat:@"%.1f",ver];
        NSString *modelName = [self deviceName];
        NSString *ipAddress = [ALNetwork currentIPAddress]; //[self getIPAddress];
        
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@",/\`'"];

        //NSString *Address = [address stringByReplacingOccurrencesOfString:@"," withString:@" "];
        
        NSString *Address = [[address componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @" "];
        //[Address stringByReplacingOccurrencesOfString:@"/" withString:@" "];
        
        NSString *Area = [[thoroughfare componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @" "];
        
        //NSString *Area = [thoroughfare stringByReplacingOccurrencesOfString:@"," withString:@" "];
        
        //calculate bit limit
        
//        [NSNumber numberWithInt:[amountlimt integerValue] + [strNextVAlidPricers_send integerValue]];
//        [NSNumber numberWithInt:Int(amountlimt)]
        
//        int a = (int)amountlimt - [strNextVAlidPricers_send integerValue];
        int a = (int)amountlimt ;
//        amountlimt = (NSInteger) a;
//        amountlimt = amountlimt-[strNextVAlidPricers_send integerValue];
//        amountlimt = amountlimt-[strNextVAlidPricers_send integerValue];
        
        
        
        NSString *str;
        if (isOldUser == 0) {
            str = @"0";
        } else {
            str = @"1";
        }
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
           if (deviceToken == nil)
           {
               deviceToken = @"";
           }
//        NSLog(@"%@",_objCurrentOuction.strBidclosingtime);
//        NSLog(@"%@",_objCurrentOuction.strproductid);
//        NSLog(@"%@",_objCurrentOuction.strDollarRate);
//        NSLog(@"%@",_objCurrentOuction.strReference);
//        NSLog(@"%@",_objCurrentOuction.strpricers);
//        NSLog(@"%@",_objCurrentOuction.strpriceus);
//        NSLog(@"%@",[ClsSetting TrimWhiteSpaceAndNewLine:_objCurrentOuction.strOnline]);
//        NSLog(@"%@",_objCurrentOuction.strFirstName);
//        NSLog(@"%@",_objCurrentOuction.strLastName);
        
        NSLog(@"%@",strUsernicName);
//        NSLog(@"%@",_objCurrentOuction.strownerid);
        
        
        NSDictionary *dict = @{
                                      @"userid": strUserid,
                                      @"country": country,
                                      @"Bidclosingtime": _objCurrentOuction.strBidclosingtime,
                                      @"checknextvalidbid":_objCurrentOuction.strpricers,
                                      @"productid": _objCurrentOuction.strproductid,
                                      @"nickname": strUsernicName,
                                      @"isOldUser": str,
                                      @"username": strUserName,
                                      @"myAmount": strNextVAlidPricers_send,
                                      @"dollerRate": _objCurrentOuction.strDollarRate,
                                      @"dollerAmt": strNextVAlidPriceus_send,
                                      @"inThumbnail": imgName,
                                      @"inReference": _objCurrentOuction.strReference,
                                      @"inOldPriceRs": _objCurrentOuction.strpricers,
                                      @"inOldPriceUs": _objCurrentOuction.strpriceus,
                                      @"inAuctionid": [ClsSetting TrimWhiteSpaceAndNewLine:_objCurrentOuction.strOnline],
                                      @"inArtistFirstName": _objCurrentOuction.strFirstName,
                                      @"inArtistLastName": _objCurrentOuction.strLastName,
                                      @"bidByVal": bidByVal,
                                      @"deviceTocken": deviceToken,
                                      @"OSversion": OSversion,
                                      @"modelName": modelName,
                                      @"ipAddress": ipAddress,
                                      @"userLocation": cityName,
                                      @"fullAddress": Address,
                                      @"shortAddress": Area,
                                      @"latitude": lat,
                                      @"longitude": lang,
                                      @"dblBidLimit": [NSNumber numberWithInt:a]
                                     
                                     
                                      };
        NSLog(@"Dict %@",dict);
        NSString  *strQuery=[NSString stringWithFormat:@"%@%@",[ClsSetting defaultURL],[ClsSetting saveBidURL]];
        

        
        
        
//        NSString  *strQuery=[NSString stringWithFormat:@"%@/spBid(%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%@,%ld)?api_key=%@",[ClsSetting saveBidURL],strNextVAlidPricers_send,_objCurrentOuction.strproductid,strUserid,_objCurrentOuction.strDollarRate,strNextVAlidPriceus_send, imgName, _objCurrentOuction.strReference,_objCurrentOuction.strpricers, _objCurrentOuction.strpriceus,[ClsSetting TrimWhiteSpaceAndNewLine:_objCurrentOuction.strOnline], _objCurrentOuction.strBidclosingtime, _objCurrentOuction.strFirstName, _objCurrentOuction.strLastName, bidByVal, Identifier, OSversion, modelName, ipAddress, cityName, Address, Area, lat, lang, (long)amountlimt, [ClsSetting apiKey]];
        
        NSString *url = strQuery;
        [manager POST:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
        {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
             NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             NSError *error;
             NSMutableArray *dict1 = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
             NSDictionary *dictResult = dict1;
             NSString *currentStatus = [NSString stringWithFormat:@"%@",[dictResult valueForKey:@"currentStatus"]];
            NSLog(@"%@",[dictResult valueForKey:@"currentStatus"]);
            
             if ([currentStatus isEqualToString:@"3"])
             {
                 //save updated bidlimit
                 NSString *_amountlimt = [NSString stringWithFormat: @"%ld", (long)amountlimt];
                 [[NSUserDefaults standardUserDefaults] setValue:_amountlimt forKey:@"amountlimt"];

                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 [ClsSetting ValidationPromt:@"Your bid submitted successfully, currently you are leading for this product"];

                 NSDictionary *dict=[[NSMutableDictionary alloc]init];
                 ClsSetting *objSetting=[[ClsSetting alloc]init];
                 
                 NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
                 [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
                 [numberFormatter setMaximumFractionDigits:0];
                 
                 NSString *strUserFirstName=[[NSUserDefaults standardUserDefaults]valueForKey:USER_FirstName];
//                 NSLog(@"%@",strProxyPricers);
//                 NSLog(@"%@",strProxyPriceus);
                 NSString *targetUrl = [NSString stringWithFormat:@"https://demoapi.astaguru.com/api/PriceFormat?Pricers=%@&Priceus=%@",dictResult[@"BidAmountRs"],dictResult[@"BidAmountUs"]];
                 NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                 [request setHTTPMethod:@"GET"];
                 [request setURL:[NSURL URLWithString:targetUrl]];

                 [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
                   ^(NSData * _Nullable data,
                     NSURLResponse * _Nullable response,
                     NSError * _Nullable error) {

//                       NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                     NSMutableArray *dict1 = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                     NSMutableDictionary *priceDic = [ClsSetting RemoveNull:dict1[0]];// dict[0];
                     NSLog(@"price = %@",priceDic);
                     NSString *pricers = priceDic[@"FormatPricers"];
                     NSString *priceus = priceDic[@"FormatPriceus"];
                     
                     
//                     NSString *strMessage=[NSString stringWithFormat:@"Dear %@, please note you are leading on Lot No %@. Your Leading bid Rs.%@($%@). You can review on www.astaguru.com or mobile App.",strUserFirstName,_objCurrentOuction.strReference,pricers,priceus];
                     
                     NSString *strMessage=[NSString stringWithFormat:@"Dear %@,  please note you are leading on Lot No %@. Bid value- INR %@($%@). You can review it on www.astaguru.com or mobile App.",strUserFirstName,_objCurrentOuction.strReference,pricers,priceus];
                     
                     NSString *smsUrl = [NSString stringWithFormat:[ClsSetting smsURL], [dictResult valueForKey:@"mobileNum"], strMessage,@"1307161398105473448"];
                     NSLog(@"%@",smsUrl);
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [objSetting sendSMSOTP:dict url:smsUrl view:self.view];
                     });
                     
                     
                       NSLog(@"Data received: %@", dict);
                 }] resume];

//                 NSString *strMessage=[NSString stringWithFormat:@"Dear %@, please note you are leading on Lot No %@. Your Leading bid Rs.%@($%@). You can review on www.astaguru.com or mobile App.",strUserFirstName,_objCurrentOuction.strReference,[numberFormatter stringFromNumber:[NSNumber numberWithInteger:[dictResult[@"BidAmountRs"] integerValue]]],[numberFormatter stringFromNumber:[NSNumber numberWithInteger:[dictResult[@"BidAmountUs"] integerValue]]]];
//
//                 NSString *strmsg1 = [strMessage
//                    stringByReplacingOccurrencesOfString:@"₹" withString:@""];
//                 NSLog(@"%@",strmsg1);
//                 NSString *strmsg2 = [strmsg1
//                                      stringByReplacingOccurrencesOfString:@"Rs " withString:@"Rs."];
//                 NSLog(@"%@",strmsg2);
//                 NSString *strmsg3 = [strmsg2
//                                      stringByReplacingOccurrencesOfString:@"$ " withString:@"$"];
//                 NSString *strmsg4 = [strmsg3
//                                      stringByReplacingOccurrencesOfString:@"  " withString:@""];
//                 NSLog(@"%@",strmsg4);
//
//                 NSString *smsUrl = [NSString stringWithFormat:[ClsSetting smsURL], [dictResult valueForKey:@"mobileNum"], strmsg4];
//                 [objSetting sendSMSOTP:dict url:smsUrl view:self.view];
                 if ([ClsSetting NSStringIsValidEmail:[dictResult valueForKey:@"emailID"]])
                 {
                     [self calculateNextIncrementValueFromCurrentValueRS:strNextVAlidPricers_send currentValueUS:strNextVAlidPriceus_send];

                     NSString *subStr = [NSString stringWithFormat:@" AstaGuru - You have been leading on Lot no %@",_objCurrentOuction.strReference];
                     NSString *strUserFirstName=[[NSUserDefaults standardUserDefaults]valueForKey:USER_FirstName];
                     NSString *strUserLasttName=[[NSUserDefaults standardUserDefaults]valueForKey:USER_LastName];

                     NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
                     [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
                     [numberFormatter setMaximumFractionDigits:0];

                     
                     NSString *strmsg =  [NSString stringWithFormat:@"Dear %@ %@,<br/><br/>    We would like to bring it to your notice that you are leading  on Lot# %@, in the ongoing AstaGuru Online Auction. Your Leading bid is on <b>Rs.%@/($%@)</b>. You can review it on www.astaguru.com or mobile App.<br/><br/>Thank You,<br/>Team Astaguru.  ",strUserFirstName,strUserLasttName,_objCurrentOuction.strReference, [numberFormatter stringFromNumber:[NSNumber numberWithInteger:[dictResult[@"BidAmountRs"] integerValue]]], [numberFormatter stringFromNumber:[NSNumber numberWithInteger:[dictResult[@"BidAmountUs"] integerValue]]]];
                     NSLog(@"%@",strmsg);
                     NSString *strmsg1 = [strmsg
                        stringByReplacingOccurrencesOfString:@"₹" withString:@""];
                     NSString *strmsg2 = [strmsg1
                                          stringByReplacingOccurrencesOfString:@"Rs. " withString:@"Rs. "];
                     NSString *strmsg3 = [strmsg2
                                          stringByReplacingOccurrencesOfString:@"$ " withString:@"$"];
                     
                     
                    
                     
                     
//                     [numberFormatter stringFromNumber:[NSNumber numberWithInteger:dictResult[@"BidAmountRs"]]];
//                     NSString *strmsg =  [NSString stringWithFormat:@"Dear %@ %@,<br/><br/>    We would like to bring it to your notice that you are leading  on Lot# %@, in the ongoing AstaGuru Online Auction. Your Leading bid is on <b>Rs.%@($%@)</b>.You can review it on www.astaguru.com or mobile App. ",strUserFirstName,dictResult[@"Username"],_objCurrentOuction.strReference, dictResult[@"BidAmountRs"], dictResult[@"BidAmountUs"]];
                     
                     [self SendEmailWithSubject:subStr message:strmsg3 email:[dictResult valueForKey:@"emailID"] name:dictResult[@"Username"]];
//                     NSString *notificationMsg = [NSString stringWithFormat:@"Dear %@ %@ your bid of Rs.%@ / $(%@) on Lot No.%@ has been placed successfully. You are currently leading.",strUserFirstName,dictResult[@"Username"],dictResult[@"BidAmountRs"],dictResult[@"BidAmountUs"],_objCurrentOuction.strReference];
                     NSString *notificationMsg = [NSString stringWithFormat:@"Dear %@ %@ your bid of Rs.%@ / $(%@) on Lot No.%@ has been placed successfully. You are currently leading.",strUserFirstName,strUserLasttName,[numberFormatter stringFromNumber:[NSNumber numberWithInteger:[dictResult[@"BidAmountRs"] integerValue]]],[numberFormatter stringFromNumber:[NSNumber numberWithInteger:[dictResult[@"BidAmountUs"] integerValue]]],_objCurrentOuction.strReference];

                     NSLog(@"%@",notificationMsg);
                     NSString *strnotificationmsg1 = [notificationMsg
                        stringByReplacingOccurrencesOfString:@"₹" withString:@""];
                     NSLog(@"%@",strnotificationmsg1);
                     NSString *strnotificationmsg2 = [strnotificationmsg1
                                          stringByReplacingOccurrencesOfString:@"Rs. " withString:@"Rs. "];
                     NSString *strnotificationmsg3 = [strnotificationmsg2
                                          stringByReplacingOccurrencesOfString:@"$ " withString:@"$"];
                     
                     
                     
                     
                     
                     
                     [self sendNotificationApi:strnotificationmsg3 message:@"Bid Placed"];
//                     [self sendNotificationApi:strnotificationmsg3];
                     [self.delegate refreshBidPrice];
                     [self.view removeFromSuperview];
                     [self removeFromParentViewController];
                 }
                 
                

             }
            else if ([currentStatus isEqualToString:@"2"])
            {
                 proxyvalidation=2;
                _lblAlert.text=@"Sorry you have been outbid by higher proxy bid. Would you like to place another bid?";
                [_btnConfirm setTitle:@"Ok" forState:UIControlStateNormal];
                _viwProxyBidConfarmation.hidden=NO;
                _viwBidNow.hidden=YES;
                _viwProxyBid.hidden=YES;
               
                
            }

            else
            {
                _lblAlert.text = [NSString stringWithFormat:@"%@",[dictResult valueForKey:@"msg"]];
//                @"Sorry, Try again";
                [_btnConfirm setTitle:@"Ok" forState:UIControlStateNormal];
                _viwProxyBidConfarmation.hidden=NO;
                _viwBidNow.hidden=YES;
                _viwProxyBid.hidden=YES;
                proxyvalidation=2;
                
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"Error: %@", error);
        }];
    }
    @catch (NSException *exception)
    {
    }
    @finally
    {
    }
}
- (NSString *) getDataFrom:(NSString *)url{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:url]];

    NSError *error = nil;
    NSHTTPURLResponse *responseCode = nil;

    NSData *oResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&responseCode error:&error];

    if([responseCode statusCode] != 200){
        NSLog(@"Error getting %@, HTTP status code %i", url, [responseCode statusCode]);
        return nil;
    }

    return [[NSString alloc] initWithData:oResponseData encoding:NSUTF8StringEncoding];
}
-(void)ProxyBid1
{
    
    if (![self validation1])
    {
        if ([_txtProxyBid.text intValue] >= 10000000)
        {
            [ClsSetting ValidationPromt:@"Proxy Bid amount should be equal to or greater than 5% more than opening bid"];
        }
        else
        {
            [ClsSetting ValidationPromt:@"Proxy Bid amount should be equal to or greater than 10% more than opening bid"];
        }
    }
    else
    {
        NSString *createdBy;
        NSString *strUserid;
        NSString *strUsernicName;
        if([[NSUserDefaults standardUserDefaults] objectForKey:USER_NAME] != nil)
        {
            createdBy = [[NSUserDefaults standardUserDefaults]valueForKey:USER_NAME];
        }
        else
        {
            createdBy = @"abhi123";
        }
        
        if([[NSUserDefaults standardUserDefaults] objectForKey:USER_id] != nil)
        {
            strUserid=[[NSUserDefaults standardUserDefaults]valueForKey:USER_id];
        }
        else
        {
            strUserid=@"1972";
        }
        if([[NSUserDefaults standardUserDefaults] objectForKey:USER_nicName] != nil)
               {
                   strUsernicName=[[NSUserDefaults standardUserDefaults]valueForKey:USER_nicName];
               }
               else
               {
                   strUsernicName=@"AnonymousUser";
               }
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
        {
            float iproxy = [_txtProxyBid.text floatValue]*[_objCurrentOuction.strDollarRate floatValue];
            int result = (int)roundf(iproxy);
            strProxyPricers = [NSString stringWithFormat:@"%d",result];
            strProxyPriceus = _txtProxyBid.text;
        }
        else
        {
            float iproxy = [_txtProxyBid.text floatValue]/[_objCurrentOuction.strDollarRate floatValue];
            int result = (int)roundf(iproxy);
            strProxyPricers = _txtProxyBid.text;
            strProxyPriceus = [NSString stringWithFormat:@"%d",result];
        }
        
        
        if (_IsUpcoming == 1)
        {
            @try
            {
                MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                HUD.labelText = @"loading";
                NSMutableDictionary *Discparam=[[NSMutableDictionary alloc]init];
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                manager.requestSerializer = [AFHTTPRequestSerializer serializer];
                manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
                
                NSString *bidByVal = @"2";
                
                NSString *Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
                float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
                
                NSString *OSversion = [NSString stringWithFormat:@"%.1f",ver];
                NSString *modelName = [self deviceName];
                NSString *ipAddress = [ALNetwork currentIPAddress];//[self getIPAddress];
                NSString *Address = [address stringByReplacingOccurrencesOfString:@"," withString:@" "];
                Address = [Address stringByReplacingOccurrencesOfString:@"/" withString:@" "];
                NSString *Area = [thoroughfare stringByReplacingOccurrencesOfString:@"," withString:@" "];
                
                 NSLog(@"%@",_objCurrentOuction.strownerid);
                 NSString *strQuery=[NSString stringWithFormat:@"%@UpcomingProxyBid?inSiteUserID=%@&inProductID=%@&inProxyAmt=%@&inProxyAmtus=%@&inCreatedby=%@&inAuctionid=%@&bidByVal=%@&deviceTocken=%@&OSversion=%@&modelName=%@&ipAddress=%@&userLocation=%@&inFullAddress&inShortAddress&inLatitude&inLongitude&amountlimtuser=%ld&isOldUser=%ld&Country=India&Ownerid=%@",[ClsSetting defaultURL],strUserid, _objCurrentOuction.strproductid,strProxyPricers,strProxyPriceus,createdBy,[ClsSetting TrimWhiteSpaceAndNewLine:_objCurrentOuction.strOnline],bidByVal,Identifier,OSversion, modelName,ipAddress,cityName,(long)amountlimt,(long)isOldUser,_objCurrentOuction.strownerid];
            
            
//            http://asta.kwebmakerdigital.com/api/UpcomingProxyBid?inSiteUserID=13&inProductID=4053&inProxyAmt=1234567&inProxyAmtus=17764&inCreatedby=Abhinash&inAuctionid=53&bidByVal=1&deviceTocken=fUVlB2fVTe2xzpoQ77bx0u:APA91bG1q30-RAX69PiWpAlseWNyOKYRGNWVOcsyuR2pCjgrJWd_h6T54w55AmII9ieNKK3G21hWN1UPONyw-WLwqrCY4XNjzuEhZm-Po9-JudzGL17MejqerQZKLowKG3kUUGt5wLzH&OSversion=7.1.1&modelName=Motorola%20Moto%20G%20Play&ipAddress=192.168.0.104&userLocation=Mumbai&inFullAddress&inShortAddress&inLatitude&inLongitude&amountlimtuser=2565433&isOldUser=0&Country=India


                NSString *url = strQuery;
                NSLog(@"%@",url);
                NSString *encoded = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

                [manager GET:encoded parameters:Discparam success:^(AFHTTPRequestOperation *operation, id responseObject)
                 {
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                     NSError *error;
                     NSMutableArray *dict1 = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
                     //                     NSLog(@"%@",responseStr);
                     NSLog(@"%@",dict1);
                     if (dict1.count>0)
                     {
                         NSDictionary *dictResult= [ClsSetting RemoveNull:[dict1 objectAtIndex:0]];

                         if ([[dictResult valueForKey:@"currentStatus"] isEqualToString:@"1"])
                         {
                             [MBProgressHUD hideHUDForView:self.view animated:YES];


                             //save updated bidlimit
                             NSString *_amountlimt = [NSString stringWithFormat: @"%ld", (long)amountlimt];
                             [[NSUserDefaults standardUserDefaults] setValue:_amountlimt forKey:@"amountlimt"];

                             [ClsSetting ValidationPromt:[NSString stringWithFormat:@"%@",[dictResult valueForKey:@"msg"]]];

                             NSMutableDictionary *dictUser = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];

                             NSString *subStr = [NSString stringWithFormat:@"AstaGuru - Proxy Bid acknowledgement"];

                             NSString *strname = [NSString stringWithFormat:@"%@ %@",dictUser[@"name"], dictUser[@"lastname"]];


                             NSString *strmsg =  [NSString stringWithFormat:@"Dear %@,\rThank you for placing a Proxy Bid amount of Rs.%@($%@) for Lot No %@ part of our '%@' Auction dated %@.\r\rWe would like to acknowledge having received your Proxy Bid, our operations team will review it and revert with confirmation of the approval.\r\rIn case you are unaware of this transaction please notify us at the earliest about the misrepresentation.\r\rIn case you would like to edit the Proxy Bid value please contact us for the same at contact@astaguru.com or call us on 91-22-6901 4800. We will be glad to assist you.",strname, strProxyPricers, strProxyPriceus, _objCurrentOuction.strReference, [ClsSetting getStringFormHtmlString:_objCurrentOuction.strAuctionname], [dictResult valueForKey:@"auctionDate"]];
                             [self SendEmailWithSubject:subStr message:strmsg email:[ClsSetting TrimWhiteSpaceAndNewLine:dictUser[@"email"]] name:strname];
                         }
                         else if ([[dictResult valueForKey:@"currentStatus"] isEqualToString:@"0"])
                         {
                             [ClsSetting ValidationPromt:[NSString stringWithFormat:@"%@",[dictResult valueForKey:@"msg"]]];
                         }
                         else if ([[dictResult valueForKey:@"currentStatus"] isEqualToString:@"3"])
                         {
                             [ClsSetting ValidationPromt:@"Your Bid Limit Exceeded. Kindly Contact with Your Administrator"];
                         }
                         else
                         {
                             [ClsSetting ValidationPromt:@"This may be a server issue"];
                         }
                         [self.view removeFromSuperview];
                         [self removeFromParentViewController];
                     }
                 }
                     failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                         NSLog(@"Error: %@", error);
                         [MBProgressHUD hideHUDForView:self.view animated:YES];
                         [ClsSetting ValidationPromt:error.localizedDescription];
                     }];
            }
            @catch (NSException *exception)
            {
                
            }
            @finally
            {
            }
        }
        else
        {
            @try
            {
                MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                HUD.labelText = @"loading";
                NSMutableDictionary *Discparam=[[NSMutableDictionary alloc]init];
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                manager.requestSerializer = [AFHTTPRequestSerializer serializer];
                manager.responseSerializer = [AFHTTPResponseSerializer serializer];  //AFHTTPResponseSerializer serializer
                manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
                NSArray *imgNameArr = [_objCurrentOuction.strthumbnail componentsSeparatedByString:@"/"];
                NSString *imgName = [imgNameArr objectAtIndex:1];
                
                NSString *bidByVal = @"2";
                
                NSString *Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
                
                float ver = [[[UIDevice currentDevice] systemVersion] floatValue];
                
                NSString *OSversion = [NSString stringWithFormat:@"%.1f",ver];
                NSString *modelName = [self deviceName];
                NSString *ipAddress = [ALNetwork currentIPAddress];// [self getIPAddress];
                NSString *Address = [address stringByReplacingOccurrencesOfString:@"," withString:@" "];
                Address = [Address stringByReplacingOccurrencesOfString:@"/" withString:@" "];
                NSString *Area = [thoroughfare stringByReplacingOccurrencesOfString:@"," withString:@" "];
                
                //calculate bit limit
                amountlimt = amountlimt-[strProxyPricers integerValue];
                
                
                amountlimt = amountlimt-[strProxyPricers integerValue];
                int a = (int)amountlimt - [strNextVAlidPricers_send integerValue];
                        
                amountlimt = (NSInteger) a;
                NSString *str;
                if (isOldUser == 0) {
                    str = @"0";
                } else {
                    str = @"1";
                }
                NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
               if (deviceToken == nil)
               {
                   deviceToken = @"";
               }
                NSDictionary *dict = @{
                                              @"userid": strUserid,
                                              @"country":country ,
                                              @"Bidclosingtime": _objCurrentOuction.strBidclosingtime,
                                              @"productid": _objCurrentOuction.strproductid,
                                              @"nickname": strUsernicName,
                                              @"isOldUser": str,
                                              @"username": createdBy,
                                              @"ProxyAmt": strProxyPricers,
                                              @"dollerRate": _objCurrentOuction.strDollarRate,
                                              @"dollerAmt": _objCurrentOuction.strDollarRate,
                                              @"inThumbnail": imgName,
                                              @"inReference": _objCurrentOuction.strReference,
                                              @"inOldPriceRs": _objCurrentOuction.strpricers,
                                              @"inOldPriceUs": _objCurrentOuction.strpriceus,
                                              @"inAuctionid": [ClsSetting TrimWhiteSpaceAndNewLine:_objCurrentOuction.strOnline],
                                              @"inArtistFirstName": _objCurrentOuction.strFirstName,
                                              @"inArtistLastName": _objCurrentOuction.strLastName,
                                              @"bidByVal": bidByVal,
                                              @"deviceTocken": deviceToken,
                                              @"OSversion": OSversion,
                                              @"modelName": modelName,
                                              @"ipAddress": ipAddress,
                                              @"userLocation": cityName,
                                              @"fullAddress": Address,
                                              @"shortAddress": Area,
                                              @"latitude": lat,
                                              @"longitude": lang,
                                              @"dblBidLimit": [NSNumber numberWithInt:a],
                                              
                                              
                                              };
                NSString  *strQuery=[NSString stringWithFormat:@"%@%@",[ClsSetting defaultURL],[ClsSetting proxyBidURL]];
                NSString *url = strQuery;
                [manager POST:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
                {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                     NSError *error;
                     NSDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
                     NSDictionary *dictResult = dict1;
                    NSString *currentStatus = [NSString stringWithFormat:@"%@",[dictResult valueForKey:@"currentStatus"]];
                    NSLog(@"%@",dictResult);
                    if ([currentStatus isEqualToString:@"3"])
                    {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
                        //save updated bidlimit
                        NSString *_amountlimt = [NSString stringWithFormat: @"%ld", (long)amountlimt];
                        [[NSUserDefaults standardUserDefaults] setValue:_amountlimt forKey:@"amountlimt"];
                        
                        [ClsSetting ValidationPromt:@"Your Proxy bid has been submitted successfully,you are currently leading. "];
                        
                        [self.delegate refreshBidPrice];

                        
                        [self.view removeFromSuperview];
                        [self removeFromParentViewController];
                        
                        
                        NSDictionary *dict=[[NSMutableDictionary alloc]init];
                        ClsSetting *objSetting=[[ClsSetting alloc]init];
                        
                        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
                        [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
                        [numberFormatter setMaximumFractionDigits:0];
                        [numberFormatter setNegativeFormat:@""];
                        numberFormatter.positiveSuffix = [numberFormatter.positiveSuffix stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                        numberFormatter.negativeSuffix = [numberFormatter.negativeSuffix stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//                        NSString* retNum = [currencyFormatter stringFromNumber:[NSNumber numberWithInteger:[strProxyPricers integerValue]]];
//                        NSLog(@"%@",priceString);
                        
                        NSString *strUserFirstName=[[NSUserDefaults standardUserDefaults]valueForKey:USER_FirstName];
                        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
                        NSString *priceString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:[strProxyPricers integerValue]]];
                        NSString *strmsg1111 = [priceString
                           stringByReplacingOccurrencesOfString:@"₹" withString:@""];
                        NSLog(@"%@",strmsg1111);
//                        NSString *varyingString1 = @"hello";
                        NSString *varyingString2 = @"world";
                        
//                        NSString *str = @"hello ";
                        NSString *str = [NSString stringWithFormat: @"%@%@", varyingString2, strmsg1111];
                        
                        NSLog(@"%@",str);
                        
//                        NSLog(@"%@",formatedNumbers1);

                        NSString *targetUrl = [NSString stringWithFormat:@"https://demoapi.astaguru.com/api/PriceFormat?Pricers=%@&Priceus=%@",dictResult[@"outBidAmountRs"],dictResult[@"outBidAmountUs"]];
                        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
                        [request setHTTPMethod:@"GET"];
                        [request setURL:[NSURL URLWithString:targetUrl]];

                        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
                          ^(NSData * _Nullable data,
                            NSURLResponse * _Nullable response,
                            NSError * _Nullable error) {

       //                       NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                            NSMutableArray *dict1 = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                            NSMutableDictionary *priceDic = [ClsSetting RemoveNull:dict1[0]];// dict[0];
                            NSLog(@"price = %@",priceDic);
                            NSString *pricers = priceDic[@"FormatPricers"];
                            NSString *priceus = priceDic[@"FormatPriceus"];
                        
                        NSString *strMessage=[NSString stringWithFormat:@"Dear %@, please note you are leading on Lot No %@. Your leading bid is Rs.%@($%@). You can review it on www.astaguru.com or mobile App.",strUserFirstName,_objCurrentOuction.strReference,pricers,priceus];
                        
                            NSString *smsUrl = [NSString stringWithFormat:[ClsSetting smsURL], [dictResult valueForKey:@"mobileNum"], strMessage];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [objSetting sendSMSOTP:dict url:smsUrl view:self.view];

                            });
                            NSLog(@"Data received: %@", dict);
                      }] resume];
                            
//                        NSString *strmsg1 = [strMessage
//                           stringByReplacingOccurrencesOfString:@"₹" withString:@""];
//                        NSLog(@"%@",strmsg1);
////                        NSString *strmsg2 = [strmsg1
////                                             stringByReplacingOccurrencesOfString:@"Rs." withString:@"Rs."];
//                        NSString *strmsg3 = [strmsg1
//                                             stringByReplacingOccurrencesOfString:@"$ " withString:@"$"];
//
//                        NSString *trimmedWhiteSpace = [strmsg3 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
//
//                        NSLog(@"%@",trimmedWhiteSpace);
////                        NSString *strmsg4 = [strmsg3
//                                             stringByReplacingOccurrencesOfString:@"?" withString:@""];
//
//                        NSString *smsUrl = [NSString stringWithFormat:[ClsSetting smsURL], [dictResult valueForKey:@"mobileNum"], trimmedWhiteSpace];
//                        [objSetting sendSMSOTP:dict url:smsUrl view:self.view];
                        
                        if ([ClsSetting NSStringIsValidEmail:[dictResult valueForKey:@"emailID"]])
                        {
                           
                            [self calculateNextIncrementValueFromCurrentValueRS:dictResult[@"lastBidpriceRs"] currentValueUS:dictResult[@"lastBidpriceUs"]];
                            
//                            NSString *subStr = [NSString stringWithFormat:@" AstaGuru - You have been leading on Lot no  %@",_objCurrentOuction.strReference];
                            NSString *subStr = [NSString stringWithFormat:@"Intimation about Proxy-Bid Approval"];
                            NSString *strUserFirstName=[[NSUserDefaults standardUserDefaults]valueForKey:USER_FirstName];
                            NSString *strUserLasttName=[[NSUserDefaults standardUserDefaults]valueForKey:USER_LastName];
                            NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
                            [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
                            [numberFormatter setMaximumFractionDigits:0];
                            
                            NSString *strmsg =  [NSString stringWithFormat:@"Dear %@ %@,<br/><br/>    We are glad to inform you that your Proxy Bid amount of <b>Rs.%@/($%@)</b> for Lot No #%@ <br/>part of our Modern Indian Art Auction has been accepted.<br/><br/> For any further assistance please feel free to write to us at, contact@astaguru.com or call us on  <br/> <b>+91-22-6901 4800.</b> We will be glad to assist you.<br/><br/>Thanking You & Warm Regards,<br/><br/>Team Astaguru. ",strUserFirstName, strUserLasttName,[numberFormatter stringFromNumber:[NSNumber numberWithInteger:[strProxyPricers integerValue]]], [numberFormatter stringFromNumber:[NSNumber numberWithInteger:[strProxyPriceus integerValue]]],_objCurrentOuction.strReference];

                            
                            
//                            NSString *strmsg =  [NSString stringWithFormat:@"Dear %@ %@,<br/><br/>    We would like to bring it to your notice that you are leading on  Lot# %@, in the ongoing AstaGuru Online Auction. Your Leading bid is <b>Rs.%@($%@)</b>. You can review it on www.astaguru.com or mobile App.",strUserFirstName, dictResult[@"Username"], _objCurrentOuction.strReference, [numberFormatter stringFromNumber:[NSNumber numberWithInteger:[dictResult[@"outBidAmountRs"] integerValue]]], [numberFormatter stringFromNumber:[NSNumber numberWithInteger:[dictResult[@"outBidAmountUs"] integerValue]]]];
                            NSLog(@"%@",strmsg);
                            NSString *strmsg1 = [strmsg
                               stringByReplacingOccurrencesOfString:@"₹" withString:@""];
                            NSString *strmsg2 = [strmsg1
                                                 stringByReplacingOccurrencesOfString:@"Rs. " withString:@"Rs. "];
                            NSString *strmsg3 = [strmsg2
                                                 stringByReplacingOccurrencesOfString:@"$ " withString:@"$"];
                            NSLog(@"%@",strmsg3);
                            [self SendEmailWithSubject:subStr message:strmsg3 email:[dictResult valueForKey:@"emailID"] name:dictResult[@"Username"]];

                            
                            
                            NSString *notificationMsg = [NSString stringWithFormat:@"Dear %@ %@ your Proxy Bid for Lot No.%@ has been successfully placed.",strUserFirstName,strUserLasttName,_objCurrentOuction.strReference];
                            NSString *strnotificationmsg1 = [notificationMsg
                               stringByReplacingOccurrencesOfString:@"₹" withString:@""];
                            NSString *strnotificationmsg2 = [strnotificationmsg1
                                                 stringByReplacingOccurrencesOfString:@"Rs. " withString:@"Rs. "];
                            NSString *strnotificationmsg3 = [strnotificationmsg2
                                                 stringByReplacingOccurrencesOfString:@"$ " withString:@"$"];
                            
                
//                            [self sendNotificationApi:strnotificationmsg3];
                            [self sendNotificationApi:strnotificationmsg3 message:@"Proxy Bid"];
                        }
                    }
                    
                    else if ([currentStatus isEqualToString:@"2"])
                    {
                        _lblAlert.text=@"Sorry you have been outbid by higher proxy bid. Would you like to place another bid?";
                        [_btnConfirm setTitle:@"Ok" forState:UIControlStateNormal];
                        _viwProxyBidConfarmation.hidden=NO;
                        _viwBidNow.hidden=YES;
                        _viwProxyBid.hidden=YES;
                        proxyvalidation=2;
                        
                    }
                    else
                    {
                        _lblAlert.text= [NSString stringWithFormat:@"%@",[dictResult valueForKey:@"msg"]];
                        [_btnConfirm setTitle:@"Ok" forState:UIControlStateNormal];
                        _viwProxyBidConfarmation.hidden=NO;
                        _viwBidNow.hidden=YES;
                        _viwProxyBid.hidden=YES;
                        proxyvalidation=2;
                        
                    }
                    
                     
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    NSLog(@"Error: %@", error);
                }];
        
            }
            @catch (NSException *exception)
            {
                
            }
            @finally
            {
            }
        }
    }
}

-(void)SendEmailWithSubject:(NSString*)substr message:(NSString*)strmsg email:(NSString*)email name:(NSString*)name
{
    NSDictionary *dictTo = @{
                             @"name":name,
                             @"email":email
                             };
    
    NSArray*arrTo=[[NSArray alloc]initWithObjects:dictTo, nil];
    NSDictionary *dictMail = @{
                               @"template":@"newsletter",
                               @"to":arrTo,
                               @"subject":substr,
                               @"body_text": strmsg,
                               @"from_name":@"AstaGuru",
                               @"from_email":@"info@infomanav.com",
                               @"reply_to_name":@"AstaGuru",
                               @"reply_to_email":@"info@infomanav.com",
                               };
    [ClsSetting sendEmailWithInfo:dictMail];
}


//-(void)passReseposeData1:(id)str
- (void)passPostResponseData:(id)responseObject
{
    if (_isBidNow==1)
    {
        [ClsSetting ValidationPromt:@"Your bid has been submitted successfully,you are currently leading."];
    }
    else
    {
        [ClsSetting ValidationPromt:@"Your Proxy bid submitted successfully,currently you are leading for this product"];
    }
    
    [self.delegate refreshBidPrice];
    
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    textField.placeholder = nil;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    textField.placeholder = @" Enter your value";
}
-(void)setPrice1{
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setMaximumFractionDigits:0];
    if ([_objCurrentOuction.strpricers intValue] >= 10000000)
    {
        int price_us = [_objCurrentOuction.strpriceus intValue];
        int priceIncreaserete_us = (price_us*5)/100;
        int FinalPrice_us = price_us + priceIncreaserete_us;
        strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us];
        
        int price_rs = [_objCurrentOuction.strpricers intValue];
        int priceIncreaserete_rs = (price_rs*5)/100;
        int FinalPrice_rs = price_rs + priceIncreaserete_rs;
        strNextVAlidPricers_send = [NSString stringWithFormat:@"%d",FinalPrice_rs];
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
        {
            numberFormatter.currencyCode = @"USD";
            if (_IsUpcoming == 1)
            {
                NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_us]];
                _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
            }
            else
            {
                NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_us]];
                _lblBeadValue.text = [NSString stringWithFormat:@"%@",strNextValidBuild];
                _lblCurrentBidValue.text = [NSString stringWithFormat:@"%@",strNextValidBuild];
            }
        }
        else
        {
            numberFormatter.currencyCode = @"INR";
            if (_IsUpcoming == 1)
            {
                NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_rs]];
                _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
            }
            else
            {
                NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_rs]];
                _lblBeadValue.text = [NSString stringWithFormat:@"%@",strNextValidBuild];
                _lblCurrentBidValue.text = [NSString stringWithFormat:@"%@",strNextValidBuild];
            }
        }
    }
    else
    {

            NSLog(@"%@",[_objCurrentOuction strpricers]);
            int price_us = [_objCurrentOuction.strpriceus intValue];
            int priceIncreaserete_us = (price_us*10)/100;
            int FinalPrice_us = price_us + priceIncreaserete_us;
            strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us];
            
            int price_rs = [_objCurrentOuction.strpricers intValue];
            int priceIncreaserete_rs = (price_rs*10)/100;
            int FinalPrice_rs = price_rs + priceIncreaserete_rs;
            strNextVAlidPricers_send = [NSString stringWithFormat:@"%d",FinalPrice_rs];
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
            {
                numberFormatter.currencyCode = @"USD";
                
                if (_IsUpcoming == 1)
                {
                    NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_us]];
                    int price_us = [_objCurrentOuction.strpriceus intValue];
                    int priceIncreaserete_us = (price_us*10)/100;
                    int FinalPrice_us = price_us + priceIncreaserete_us;
                    strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us];
                    _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
                    
                    
                    
                }
                else
                {
                    NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_us]];
                    int price_us = [_objCurrentOuction.strpriceus intValue];
                    int priceIncreaserete_us = (price_us*10)/100;
                    int FinalPrice_us = price_us + priceIncreaserete_us;
                    strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us];
                    _lblBeadValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
                    _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
                }
            }
            else
            {
                numberFormatter.currencyCode = @"INR";
                if (_IsUpcoming == 1)
                {
                    NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_rs]];
                    _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
                }
                else
                {
                    NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_rs]];;
                    _lblBeadValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
                    _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
                }
            }
    }
}
-(void)setPrice
{
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setMaximumFractionDigits:0];
    if ([_objCurrentOuction.strpricers intValue] >= 10000000)
    {
        int price_us = [_objCurrentOuction.strpriceus intValue];
        int priceIncreaserete_us = (price_us*5)/100;
        int FinalPrice_us = price_us + priceIncreaserete_us;
        strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us];
        
        int price_rs = [_objCurrentOuction.strpricers intValue];
        int priceIncreaserete_rs = (price_rs*5)/100;
        int FinalPrice_rs = price_rs + priceIncreaserete_rs;
        strNextVAlidPricers_send = [NSString stringWithFormat:@"%d",FinalPrice_rs];
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
        {
            numberFormatter.currencyCode = @"USD";
            if (_IsUpcoming == 1)
            {
                NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_us]];
                _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
            }
            else
            {
                int price_us = [_objCurrentOuction.strpriceus intValue];
                int priceIncreaserete_us = (price_us*5)/100;
                int FinalPrice_us = price_us + priceIncreaserete_us;
                strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us];
                NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_us]];
                _lblBeadValue.text = [NSString stringWithFormat:@"%@",strNextValidBuild];
                _lblCurrentBidValue.text = [NSString stringWithFormat:@"%@",strNextValidBuild];
                 price_us = [_objCurrentOuction.strpriceus intValue];
                 priceIncreaserete_us = (price_us*6)/100;
                 FinalPrice_us = price_us + priceIncreaserete_us;
                 int FinalPrice_us1 = (FinalPrice_us*6)/100;
                 int FinalPrice_us2 = FinalPrice_us+FinalPrice_us1;
                 strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us2];
            }
        }
        else
        {
            numberFormatter.currencyCode = @"INR";
            if (_IsUpcoming == 1)
            {
                NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_rs]];
                _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
            }
            else
            {
                int price_rs = [_objCurrentOuction.strpricers intValue];
                       int priceIncreaserete_rs = (price_rs*5)/100;
                       int FinalPrice_rs = price_rs + priceIncreaserete_rs;
                
                NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_rs]];
                _lblBeadValue.text = [NSString stringWithFormat:@"%@",strNextValidBuild];
                _lblCurrentBidValue.text = [NSString stringWithFormat:@"%@",strNextValidBuild];
                     price_rs = [_objCurrentOuction.strpricers intValue];
                      priceIncreaserete_rs = (price_rs*6)/100;
                       FinalPrice_rs = price_rs + priceIncreaserete_rs;
                int FinalPrice_rs1 = (FinalPrice_rs*6)/100;
                int FinalPrice_rs2 = FinalPrice_rs+FinalPrice_rs1;
                strNextVAlidPricers_send = [NSString stringWithFormat:@"%d",FinalPrice_rs2];

            }
        }
    }
    else
    {
//        {
//            NSLog(@"%@",[_objCurrentOuction strpricers]);
//            int price_us = [_objCurrentOuction.strpriceus intValue];
//            int priceIncreaserete_us = (price_us*10)/100;
//            int FinalPrice_us = price_us + priceIncreaserete_us;
//            strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us];
//
//            int price_rs = [_objCurrentOuction.strpricers intValue];
//            int priceIncreaserete_rs = (price_rs*10)/100;
//            int FinalPrice_rs = price_rs + priceIncreaserete_rs;
//            strNextVAlidPricers_send = [NSString stringWithFormat:@"%d",FinalPrice_rs];
//
//            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
//            {
//                numberFormatter.currencyCode = @"USD";
//
//                if (_IsUpcoming == 1)
//                {
//                    NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_us]];
//                    _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
//                }
//                else
//                {
//                    NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_us]];
//                    _lblBeadValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//                    _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//                }
//            }
//            else
//            {
//                numberFormatter.currencyCode = @"INR";
//                if (_IsUpcoming == 1)
//                {
//                    NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_rs]];
//                    _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
//                }
//                else
//                {
//                    NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_rs]];;
//                    _lblBeadValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//                    _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//                }
//            }

        
        
     
//            NSLog(@"%@",[_objCurrentOuction strpricers]);
//            int price_us = [_objCurrentOuction.strpriceus intValue];
//            int priceIncreaserete_us = (price_us*10)/100;
//            int FinalPrice_us = price_us + priceIncreaserete_us;
//            strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us];
//
//            int price_rs = [_objCurrentOuction.strpricers intValue];
//            int priceIncreaserete_rs = (price_rs*10)/100;
//            int FinalPrice_rs = price_rs + priceIncreaserete_rs;
//            strNextVAlidPricers_send = [NSString stringWithFormat:@"%d",FinalPrice_rs];
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
            {
                numberFormatter.currencyCode = @"USD";
                
                if (_IsUpcoming == 1)
                {
                    int price_us = [_objCurrentOuction.strpriceus intValue];
                                       int priceIncreaserete_us = (price_us*10)/100;
                                       int FinalPrice_us = price_us + priceIncreaserete_us;
                    NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_us]];
                   
                    strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us];
                    _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
                    
                    
                    
                }
                else
                    
                {
                    int price_us = [_objCurrentOuction.strpriceus intValue];
                    int priceIncreaserete_us = (price_us*10)/100;
                    int FinalPrice_us = price_us + priceIncreaserete_us;
                    strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us];
                    NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_us]];
                    
                    _lblBeadValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
                    _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
                    price_us = [_objCurrentOuction.strpriceus intValue];
                    priceIncreaserete_us = (price_us*15)/100;
                    
                    FinalPrice_us = price_us + priceIncreaserete_us;
                    int FinalPrice_us1 = (FinalPrice_us*15)/100;
                    int FinalPrice_us2 = FinalPrice_us+FinalPrice_us1;
                                       strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us2];
                }
            }
            else
            {
                numberFormatter.currencyCode = @"INR";
                if (_IsUpcoming == 1)
                {
                    int price_rs = [_objCurrentOuction.strpricers intValue];
                               int priceIncreaserete_rs = (price_rs*10)/100;
                               int FinalPrice_rs = price_rs + priceIncreaserete_rs;
                               strNextVAlidPricers_send = [NSString stringWithFormat:@"%d",FinalPrice_rs];
                    
                    NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_rs]];
                    _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
                    
                     
                }
                else
                {
                    int price_rs = [_objCurrentOuction.strpricers intValue];
                    int priceIncreaserete_rs = (price_rs*10)/100;
                    int FinalPrice_rs = price_rs + priceIncreaserete_rs;
                               strNextVAlidPricers_send = [NSString stringWithFormat:@"%d",FinalPrice_rs];
                    
                    NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_rs]];;
                    _lblBeadValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
                    _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
                    
                    price_rs = [_objCurrentOuction.strpricers intValue];
                    priceIncreaserete_rs = (price_rs*15)/100;
                 
                    FinalPrice_rs = price_rs + priceIncreaserete_rs;
                    int FinalPrice_rs1 = (FinalPrice_rs*15)/100;
                    int FinalPrice_rs2 = FinalPrice_rs+FinalPrice_rs1;
                    strNextVAlidPricers_send = [NSString stringWithFormat:@"%d",FinalPrice_rs2];
                }
            }

//        }
//      if(_isBidNow==FALSE){
//          if(_IsUpcoming==1){
//              NSLog(@"%@",[_objCurrentOuction strpricers]);
//                         int price_us = [_objCurrentOuction.strpriceus intValue];
//                         int priceIncreaserete_us = (price_us*10)/100;
//                         int FinalPrice_us = price_us + priceIncreaserete_us;
//                         strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us];
//                         
//                         int price_rs = [_objCurrentOuction.strpricers intValue];
//                         int priceIncreaserete_rs = (price_rs*10)/100;
//                         int FinalPrice_rs = price_rs + priceIncreaserete_rs;
//                         strNextVAlidPricers_send = [NSString stringWithFormat:@"%d",FinalPrice_rs];
//                         
//                         if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
//                         {
//                             numberFormatter.currencyCode = @"USD";
//                             
//                             if (_IsUpcoming == 1)
//                             {
//                                 NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_us]];
//                                 _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
//                             }
//                             else
//                             {
//                                 NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_us]];
//                                 _lblBeadValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//                                 _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//                             }
//                         }
//                         else
//                         {
//                             numberFormatter.currencyCode = @"INR";
//                             if (_IsUpcoming == 1)
//                             {
//                                 NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_rs]];
//                                 _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
//                             }
//                             else
//                             {
//                                 NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_rs]];;
//                                 _lblBeadValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//                                 _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//                             }
//                         }
//          }
//          else{
//                          NSLog(@"%@",[_objCurrentOuction strpricers]);
//                          int price_us = [_objCurrentOuction.strpriceus intValue];
//                          int priceIncreaserete_us1 = price_us+(price_us*15)/100;
//                          int priceIncreaserete_us = priceIncreaserete_us1+(priceIncreaserete_us1*15)/100;
//                          int FinalPrice_us = priceIncreaserete_us;
//                          strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us];
//                          
//                          int price_rs = [_objCurrentOuction.strpricers intValue];
//                          int priceIncreaserete_rs1 = price_rs+(price_rs*15)/100.0;
//                          
//                          int priceIncreaserete_rs = priceIncreaserete_rs1+(priceIncreaserete_rs1*15)/100.0;
//              //            NSLog(@"%@",priceIncreaserete_rs1);
//              //             int priceIncreaserete_rs = (priceIncreaserete_rs1*15)/100;
//              //             NSLog(@"%@",priceIncreaserete_rs);
//                          int FinalPrice_rs =  priceIncreaserete_rs;
//                          strNextVAlidPricers_send = [NSString stringWithFormat:@"%d",FinalPrice_rs];
//                          
//                          if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
//                          {
//                              numberFormatter.currencyCode = @"USD";
//                              
//                              if (_IsUpcoming == 1)
//                              {
//                                  NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_us]];
//                                  _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
//                              }
//                              else
//                              {
//                                  NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_us]];
//                                  _lblBeadValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//                                  _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//                              }
//                          }
//                          else
//                          {
//                              numberFormatter.currencyCode = @"INR";
//                              if (_IsUpcoming == 1)
//                              {
//                                  NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_rs]];
//                                  _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
//                              }
//                              else
//                              {
//                                  NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_rs]];;
//                                  _lblBeadValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//                                  _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//                              }
//                          }
//
//          }
//        }
        
//        NSLog(@"%@",[_objCurrentOuction strpricers]);
//        int price_us = [_objCurrentOuction.strpriceus intValue];
//        int priceIncreaserete_us = (price_us*10)/100;
//        int FinalPrice_us = price_us + priceIncreaserete_us;
//        strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us];
//
//        int price_rs = [_objCurrentOuction.strpricers intValue];
//        int priceIncreaserete_rs = (price_rs*10)/100;
//        int FinalPrice_rs = price_rs + priceIncreaserete_rs;
//        strNextVAlidPricers_send = [NSString stringWithFormat:@"%d",FinalPrice_rs];
//
//        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
//        {
//            numberFormatter.currencyCode = @"USD";
//
//            if (_IsUpcoming == 1)
//            {
//                NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_us]];
//                _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
//            }
//            else
//            {
//                NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_us]];
//                _lblBeadValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//                _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//            }
//        }
//        else
//        {
//            numberFormatter.currencyCode = @"INR";
//            if (_IsUpcoming == 1)
//            {
//                NSString *bidValue = [numberFormatter stringFromNumber:[NSNumber numberWithInt:price_rs]];
//                _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",bidValue];
//            }
//            else
//            {
//                NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_rs]];;
//                _lblBeadValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//                _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@", strNextValidBuild];
//            }
//        }
    }
}

//-(void)setCurrenBidvalue
//{
//    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
//    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
//    [numberFormatter setMaximumFractionDigits:0];
//    if ([_objCurrentOuction.strpricers intValue] >= 10000000)
//    {
//        int price_us = [_objCurrentOuction.strpriceus intValue];
//        int priceIncreaserete_us = (price_us*5)/100;
//        int FinalPrice_us = price_us + priceIncreaserete_us;
//        strNextVAlidPriceus_send=[NSString stringWithFormat:@"%d",FinalPrice_us];
//        
//        int price_rs = [_objCurrentOuction.strpricers intValue];
//        int priceIncreaserete_rs = (price_rs*5)/100;
//        int FinalPrice_rs = price_rs + priceIncreaserete_rs;
//        strNextVAlidPricers_send=[NSString stringWithFormat:@"%d",FinalPrice_rs];
//
//        if (_iscurrencyInDollar==1)
//        {
//            numberFormatter.currencyCode = @"USD";
//
//            NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_us]];
//            
//            _lblBeadValue.text=[NSString stringWithFormat:@"%@",strNextValidBuild];
//            _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",strNextValidBuild];
//        }
//        else
//        {
//            numberFormatter.currencyCode = @"INR";
//
//            NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_rs]];
//            
//            _lblBeadValue.text=[NSString stringWithFormat:@"%@",strNextValidBuild];
//            _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",strNextValidBuild];
//        }
//    }
//    else
//    {
//        int price_us = [_objCurrentOuction.strpriceus intValue];
//        int priceIncreaserete_us = (price_us*10)/100;
//        int FinalPrice_us = price_us+priceIncreaserete_us;
//        strNextVAlidPriceus_send = [NSString stringWithFormat:@"%d",FinalPrice_us];
//        
//        int price_rs = [_objCurrentOuction.strpricers intValue];
//        int priceIncreaserete_rs = (price_rs*10)/100;
//        int FinalPrice_rs = price_rs + priceIncreaserete_rs;
//        strNextVAlidPricers_send=[NSString stringWithFormat:@"%d",FinalPrice_rs];
//
//        if (_iscurrencyInDollar==1)
//        {
//            numberFormatter.currencyCode = @"USD";
//            
//            NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_us]];
//
//            _lblBeadValue.text=[NSString stringWithFormat:@"%@",strNextValidBuild];
//            _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",strNextValidBuild];
//        }
//        else
//        {
//            numberFormatter.currencyCode = @"INR";
//
//            NSString *strNextValidBuild = [numberFormatter stringFromNumber:[NSNumber numberWithInt:FinalPrice_rs]];
//
//            _lblBeadValue.text=[NSString stringWithFormat:@"%@",strNextValidBuild];
//            _lblCurrentBidValue.text=[NSString stringWithFormat:@"%@",strNextValidBuild];
//        }
//    }
//}

-(BOOL)validation1
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
    {
        if ([strNextVAlidPriceus_send intValue] <= [_txtProxyBid.text intValue])
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        if ([strNextVAlidPricers_send intValue] <= [_txtProxyBid.text intValue])
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    return true;
}

-(void)SendSMSOTP:(NSDictionary*)dict url:(NSString*)strURL view:(UIView*)Callingview
{
    @try {
  
        NSDictionary *Discparam=[[NSDictionary alloc]init];
        Discparam=dict;
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];  //AFHTTPResponseSerializer serializer
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        
        NSString  *strQuery=[NSString stringWithFormat:@"%@",strURL];
        NSString *url = strQuery;
        NSLog(@"%@",url);
        
        NSString *encoded = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [manager GET:encoded parameters:Discparam success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             //  NSError *error=nil;
             NSString *responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
             NSError *error;
             NSMutableArray *dict1 = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
             NSLog(@"%@",responseStr);
             NSLog(@"%@",dict1);
             
         }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
             }];
    }
    @catch (NSException *exception)
    {
    }
    @finally
    {
    }
}

- (NSString*) deviceName
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    static NSDictionary* deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{@"i386"      : @"Simulator",
                              @"x86_64"    : @"Simulator",
                              @"iPod1,1"   : @"iPod Touch",        // (Original)
                              @"iPod2,1"   : @"iPod Touch",        // (Second Generation)
                              @"iPod3,1"   : @"iPod Touch",        // (Third Generation)
                              @"iPod4,1"   : @"iPod Touch",        // (Fourth Generation)
                              @"iPod7,1"   : @"iPod Touch",        // (6th Generation)
                              @"iPhone1,1" : @"iPhone",            // (Original)
                              @"iPhone1,2" : @"iPhone",            // (3G)
                              @"iPhone2,1" : @"iPhone",            // (3GS)
                              @"iPad1,1"   : @"iPad",              // (Original)
                              @"iPad2,1"   : @"iPad 2",            //
                              @"iPad3,1"   : @"iPad",              // (3rd Generation)
                              @"iPhone3,1" : @"iPhone 4",          // (GSM)
                              @"iPhone3,3" : @"iPhone 4",          // (CDMA/Verizon/Sprint)
                              @"iPhone4,1" : @"iPhone 4S",         //
                              @"iPhone5,1" : @"iPhone 5",          // (model A1428, AT&T/Canada)
                              @"iPhone5,2" : @"iPhone 5",          // (model A1429, everything else)
                              @"iPad3,4"   : @"iPad",              // (4th Generation)
                              @"iPad2,5"   : @"iPad Mini",         // (Original)
                              @"iPhone5,3" : @"iPhone 5c",         // (model A1456, A1532 | GSM)
                              @"iPhone5,4" : @"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" : @"iPhone 5s",         // (model A1433, A1533 | GSM)
                              @"iPhone6,2" : @"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPhone7,1" : @"iPhone 6 Plus",     //
                              @"iPhone7,2" : @"iPhone 6",          //
                              @"iPhone8,1" : @"iPhone 6S",         //
                              @"iPhone8,2" : @"iPhone 6S Plus",    //
                              @"iPhone8,4" : @"iPhone SE",         //
                              @"iPhone9,1" : @"iPhone 7",          //
                              @"iPhone9,3" : @"iPhone 7",          //
                              @"iPhone9,2" : @"iPhone 7 Plus",     //
                              @"iPhone9,4" : @"iPhone 7 Plus",     //
                              @"iPhone10,1": @"iPhone 8",          // CDMA
                              @"iPhone10,4": @"iPhone 8",          // GSM
                              @"iPhone10,2": @"iPhone 8 Plus",     // CDMA
                              @"iPhone10,5": @"iPhone 8 Plus",     // GSM
                              @"iPhone10,3": @"iPhone X",          // CDMA
                              @"iPhone10,6": @"iPhone X",          // GSM
                              
                              @"iPad4,1"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   : @"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   : @"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                              @"iPad4,7"   : @"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                              @"iPad6,7"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                              @"iPad6,8"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                              @"iPad6,3"   : @"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                              @"iPad6,4"   : @"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                              };
    }
    
    NSString* deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        }
        else {
            deviceName = @"Unknown";
        }
    }
    
    return deviceName;
}

- (NSString *)getIPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}

//five bid

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return fiveBid.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    UILabel *lbl = [cell viewWithTag:11];
    lbl.text = [fiveBid objectAtIndex:indexPath.row];
    
    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *number =  [fiveBid objectAtIndex:indexPath.row];
    
    NSCharacterSet *nonNumbersSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet];
    NSString *result = [number stringByTrimmingCharactersInSet:nonNumbersSet];
    result = [result stringByReplacingOccurrencesOfString:@"," withString:@""];
    _txtProxyBid.text = result;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}


-(void)sendNotificationApi:(NSString *)notificationMsg message:(NSString*)substr{
    [[NotifiactionManger sharedInstance]sendNotificationMsg:substr messageInfo:notificationMsg];
}



@end
