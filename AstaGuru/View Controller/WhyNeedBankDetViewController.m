//
//  WhyNeedBankDetViewController.m
//  AstaGuru
//
//  Created by macbook on 09/04/20.
//  Copyright © 2020 4Fox Solutions. All rights reserved.
//

#import "WhyNeedBankDetViewController.h"

@interface WhyNeedBankDetViewController ()

@end

@implementation WhyNeedBankDetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_isFromBiling == TRUE) {
        _labelText.text = @"AstaGuru requires a certain sum of money to be deposited at the time of registration, for first time users. In case the user wins a lot(s) in the auction, the sum is adjusted within the final price. However, in the event of an unsuccessful bid or non-participation we will refund the amount in the bank account mentioned above. The amount will be credited within 5 working days, post the auction.";
    } else {
        _labelText.text = @"This value indicates the max. limit a user can bid in an auction. To increase this limit, kindly get in touch with AstaGuru at contact@astaguru.com or call us at 022-22048138";
    }
}

- (IBAction)btn_Dismiss_pressed:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
