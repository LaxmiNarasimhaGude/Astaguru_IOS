//
//  CommonWebviewViewController.h
//  AstaGuru
//
//  Created by macbook on 09/04/20.
//  Copyright © 2020 4Fox Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommonWebviewViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property(nonatomic,retain)NSURL *url;
@property(nonatomic,retain)NSURL *navTitle;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicator;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *sideleftbarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end

NS_ASSUME_NONNULL_END
