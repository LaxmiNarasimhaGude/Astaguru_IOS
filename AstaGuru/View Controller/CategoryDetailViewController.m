//
//  CategoryDetailViewController.m
//  AstaGuru
//
//  Created by Apple.Inc on 11/12/18.
//  Copyright © 2018 4Fox Solutions. All rights reserved.
//

#import "CategoryDetailViewController.h"
#import "clsAboutUs.h"
#import "EGOImageView.h"
#import "SectionHeaderReusableView.h"
#import "AboutUsCollectionViewCell.h"
#import "SWRevealViewController.h"
#import "ViewController.h"
#import "AppDelegate.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "ClsSetting.h"


@interface CategoryDetailViewController ()<MFMailComposeViewControllerDelegate,AboutUs>
{
    NSMutableArray *arrEmploye;

//    NSMutableArray *arrManagement;
//    NSMutableArray *arrSpecialities;
    int height;
    NSString *strintro;
    NSString *gstOnSale;
    NSString *category;
    NSString *titleSection;
    NSCharacterSet *charSet;

}
@end

@implementation CategoryDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Categories Details";
    
    charSet = [NSCharacterSet newlineCharacterSet];

    titleSection = @"For further details, please contact:";
    
    arrEmploye = [[NSMutableArray alloc] init];
  
    self.clv_aboutus.hidden = true;
    [self spGetAboutUs];
    
    [self setNavigationBarBackButton];
}

-(void)setNavigationBarBackButton
{
    self.navigationItem.hidesBackButton = YES;
    UIButton *_backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backButton setFrame:CGRectMake(0, 0, 30, 22)];
    [_backButton setImage:[UIImage imageNamed:@"icon-back.png"] forState:UIControlStateNormal];
    [_backButton setTintColor:[UIColor whiteColor]];
    [_backButton addTarget:self action:@selector(backPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *_backBarButton = [[UIBarButtonItem alloc] initWithCustomView:_backButton];
    [self.navigationItem setLeftBarButtonItem:_backBarButton];
}

-(void)backPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)closePressed
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SWRevealViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    UIViewController *viewController =rootViewController;
    AppDelegate * objApp = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    objApp.window.rootViewController = viewController;
    
}

-(void)spGetAboutUs
{
    //-------
    @try {
        
        MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"loading";
        NSMutableDictionary *Discparam=[[NSMutableDictionary alloc]init];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];  //AFHTTPResponseSerializer serializer
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        
        NSString  *strQuery=[NSString stringWithFormat:@"%@/spDepartment(%@)?api_key=%@",[ClsSetting procedureURL], self.catID, [ClsSetting apiKey]];
        
        NSString *url = strQuery;
        NSLog(@"%@",url);
        
        NSString *encoded = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [manager GET:encoded parameters:Discparam success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             
             NSError *error;
             NSMutableArray *arrElement = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
             NSLog(@"%@",arrElement);
             if(arrElement.count > 0)
             {
                 self.clv_aboutus.hidden = false;
                 NSArray *arrAboutus = arrElement[0];
                 
                 strintro = [arrAboutus[0] valueForKey:@"categoryDesc"];
                 gstOnSale = [arrAboutus[0] valueForKey:@"gstOnSale"];
                 category = [arrAboutus[0] valueForKey:@"category"];
                 
                 NSArray *arr_Employe = arrElement[1];
                 
                 [arrEmploye removeAllObjects];
                 //[arrSpecialities removeAllObjects];
                 for (int i = 0; i < arr_Employe.count; i++)
                 {
                     NSDictionary *emp =  [ClsSetting RemoveNullOnly:arr_Employe[i]];
                     clsAboutUs *objclsAboutus=[[clsAboutUs alloc]init];
                     objclsAboutus.strimage = emp[@"empimageurl"];
                     objclsAboutus.strName = emp[@"Empname"];
                     objclsAboutus.strEmpdesc = emp[@"empdesc"];
                     objclsAboutus.strType = emp[@"empmanagement"];
                     objclsAboutus.strEmail = emp[@"empemail"];
                     [arrEmploye addObject:objclsAboutus];
                 }
                 
                 [self.clv_aboutus reloadData];
             }
             
             [MBProgressHUD hideHUDForView:self.view animated:YES];
         }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 [ClsSetting ValidationPromt:error.localizedDescription];
             }];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 1;
    }
    return arrEmploye.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell3;
    AboutUsCollectionViewCell *cell1;
    if (indexPath.section==0)
    {
        cell1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellone" forIndexPath:indexPath];
        cell1.lblTitle.text = strintro;
        cell1.lblTitle.textAlignment = NSTextAlignmentJustified;
        cell1.lbl_vat.text = gstOnSale;
        cell3=cell1;
    }
    else
    {
        AboutUsCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        clsAboutUs *objAboutUs = [[clsAboutUs alloc] init];
        
        if (indexPath.section==1)
        {
            objAboutUs = [arrEmploye objectAtIndex:indexPath.row];
        }
        
        cell.img.imageURL=[NSURL URLWithString:objAboutUs.strimage];
        
        cell.lblTitle.text = [[objAboutUs.strName componentsSeparatedByCharactersInSet:charSet] componentsJoinedByString: @""];
        cell.lblDesignation.text = [[objAboutUs.strEmpdesc componentsSeparatedByCharactersInSet:charSet] componentsJoinedByString: @""];
        cell.lblEmail.text = [[objAboutUs.strEmail componentsSeparatedByCharactersInSet:charSet] componentsJoinedByString: @""];
        cell.lblMobileNo.text = [[objAboutUs.strPhone componentsSeparatedByCharactersInSet:charSet] componentsJoinedByString: @""];
        
        cell.btnEmail.hidden = YES;

        cell.objAboutUs = objAboutUs;
        cell.AboutUsdelegate=self;
        cell3=cell;
    }
    return cell3;
}

- (CGSize)collectionView:(UICollectionView *)collectionView1 layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        float width=(collectionView1.frame.size.width/2);
        return CGSizeMake(width-6, 180);
    }
    else
    {
        CGSize maximumLabelSize = CGSizeMake(collectionView1.frame.size.width-16, FLT_MAX);
        
        CGRect labelRect1 = [strintro
                            boundingRectWithSize:maximumLabelSize
                            options:NSStringDrawingUsesLineFragmentOrigin
                            attributes:@{
                                         NSFontAttributeName : [UIFont fontWithName:@"WorkSans-Regular" size:16]
                                         }
                            context:nil];
        
        CGRect labelRect2 = [gstOnSale
                            boundingRectWithSize:maximumLabelSize
                            options:NSStringDrawingUsesLineFragmentOrigin
                            attributes:@{
                                         NSFontAttributeName : [UIFont fontWithName:@"WorkSans-Regular" size:16]
                                         }
                            context:nil];
        
        float height_strIntro = labelRect1.size.height;
        float height_gstOnSale = labelRect2.size.height;

        float height1 = height_strIntro + height_gstOnSale + 40;
        
        return CGSizeMake(collectionView1.frame.size.width, height1);
    }
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader)
    {
        SectionHeaderReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        
        if (indexPath.section == 0)
        {
            headerView.title.hidden=NO;
            headerView.title.text = [[category componentsSeparatedByCharactersInSet:charSet] componentsJoinedByString: @""];
            headerView.title.textColor = [UIColor blackColor];
            UIFont *font= [UIFont fontWithName:@"WorkSans-Medium" size:18];
            headerView.title.font = font;
        }
        else
        {
            if(arrEmploye.count == 0)
            {
                headerView.title.text = @"";
            }
            else
            {
                headerView.title.hidden=NO;
                headerView.title.text = titleSection;
                headerView.title.textColor = [UIColor grayColor];
                UIFont *font= [UIFont fontWithName:@"WorkSans-Regular" size:16];
                headerView.title.font = font;
            }
        }
        headerView.title.numberOfLines = 0;
        reusableview = headerView;
    }
    else if (kind == UICollectionElementKindSectionFooter)
    {
        UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
        reusableview = footerview;
    }
    return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    CGSize maximumLabelSize = CGSizeMake(collectionView.frame.size.width-16, FLT_MAX);

    if(section == 0)
    {

        CGRect labelRect1 = [category
                             boundingRectWithSize:maximumLabelSize
                             options:NSStringDrawingUsesLineFragmentOrigin
                             attributes:@{
                                          NSFontAttributeName : [UIFont fontWithName:@"WorkSans-Medium" size:18]
                                          }
                             context:nil];
        return CGSizeMake(CGRectGetWidth(collectionView.bounds), labelRect1.size.height + 10);
    }
    else
    {
        CGRect labelRect1 = [titleSection
                             boundingRectWithSize:maximumLabelSize
                             options:NSStringDrawingUsesLineFragmentOrigin
                             attributes:@{
                                          NSFontAttributeName : [UIFont fontWithName:@"WorkSans-Regular" size:16]
                                          }
                             context:nil];
        return CGSizeMake(CGRectGetWidth(collectionView.bounds), labelRect1.size.height + 16);
    }
}

- (IBAction)btnEmailClicked:(UIButton*)sender
{
    clsAboutUs *objAboutUs=[[clsAboutUs alloc]init];
    objAboutUs = [arrEmploye objectAtIndex:sender.tag];
    
    MFMailComposeViewController *composer=[[MFMailComposeViewController alloc]init];
    [composer setMailComposeDelegate:self];
    if ([MFMailComposeViewController canSendMail]) {
        [composer setToRecipients:[NSArray arrayWithObjects:objAboutUs.strEmail, nil]];
        [composer setSubject:[NSString stringWithFormat:@"Inquiry from Astaguru app"]];
        NSString *message=[NSString stringWithFormat:@"Hello %@,\n",objAboutUs.strName];
        [composer setMessageBody:message isHTML:NO];
        [composer setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:composer animated:YES completion:nil];
    }
    else {
    }
    
    
    
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (error) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"error" message:[NSString stringWithFormat:@"error %@",[error description]] delegate:nil cancelButtonTitle:@"dismiss" otherButtonTitles:nil, nil];
        [alert show];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)btnEmail:(clsAboutUs *)objAboutUS
{
    MFMailComposeViewController *composer=[[MFMailComposeViewController alloc]init];
    [composer setMailComposeDelegate:self];
    if ([MFMailComposeViewController canSendMail]) {
        [composer setToRecipients:[NSArray arrayWithObjects:objAboutUS.strEmail, nil]];
        [composer setSubject:[NSString stringWithFormat:@"Inquiry from Astaguru app"]];//@"Astaguru: %@",objAboutUS.strName]];
        
        //    [composer.setSubject.placeholder = [NSLocalizedString(@"This is a placeholder",)];
        NSString *message=[NSString stringWithFormat:@"Hello %@,\n",objAboutUS.strName];
        [composer setMessageBody:message isHTML:NO];
        [composer setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:composer animated:YES completion:nil];
    }
    else {
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
