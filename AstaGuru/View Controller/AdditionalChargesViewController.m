//
//  AdditionalChargesViewController.m
//  AstaGuru
//
//  Created by Aarya Tech on 13/10/16.
//  Copyright © 2016 Aarya Tech. All rights reserved.
//

#import "AdditionalChargesViewController.h"
#import "ClsSetting.h"
#import "AdditionalChargesCollectionViewCell.h"
#import "MyProfileViewController.h"
#import "BforeLoginViewController.h"
#import "UserInfoCollectionViewCell.h"

typedef struct Calculation {
    
    BOOL isMargin;
    BOOL isInternational;
    
    NSInteger astaguruPrice;
    
    NSString *nextVaidBid;
    NSString *currentVaidBid;
    NSString *margin;
    NSString *gst;
    NSString *gstOnBuyersPremium;
    NSString *customsDuty;
    
    NSString *total;
    NSString *tcs;
   
    
    
}Calculation;

@interface AdditionalChargesViewController ()<PassResponse>
{
   
    NSMutableArray *arrBottomMenu;
    NSMutableDictionary *dictUserProfile;
    int tag;
}
@end

@implementation AdditionalChargesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setNavigationBarBackButton];

    tag = 0;
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated
{
    /* self.navigationItem.backBarButtonItem =
     [[UIBarButtonItem alloc] initWithTitle:@"Back"
     style:UIBarButtonItemStylePlain
     target:nil
     action:nil];*/

    [self getAuction];
    
//    else
//    {
//        [self GetProfile];
//    }
    //self.navigationController.navigationBar.backItem.title = @"Back";
}
-(void)setNavigationBarBackButton
{
    self.navigationItem.hidesBackButton = YES;
    UIButton *_backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backButton setFrame:CGRectMake(0, 0, 30, 22)];
    [_backButton setImage:[UIImage imageNamed:@"icon-back.png"] forState:UIControlStateNormal];
  //  [_backButton imageView].contentMode = UIViewContentModeScaleAspectFit;
  //  [_backButton setImageEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
  //  [_backButton setTitle:@"Back" forState:UIControlStateNormal];
   // [[_backButton titleLabel] setFont:[UIFont fontWithName:@"WorkSans-Medium" size:18]];
  //  [_backButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -34, 0, 0)];
    [_backButton setTintColor:[UIColor whiteColor]];
    [_backButton addTarget:self action:@selector(backPressed) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *_backBarButton = [[UIBarButtonItem alloc] initWithCustomView:_backButton];
    [self.navigationItem setLeftBarButtonItem:_backBarButton];
}

-(void)backPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setUpNavigationItem];
}
-(void)searchPressed
{
     [ClsSetting Searchpage:self.navigationController];
}
-(void)myastaguru
{
    
    [ClsSetting myAstaGuru:self.navigationController];
    
}
-(void)myastaguru1
{
    
    [ClsSetting myAstaGuru1:self.navigationController];
    
}
-(void)getAuction
{
    tag = 1;
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"acution/%@/?api_key=%@&filter=online=%@&related=*",_objCurrentOuction.strproductid,[ClsSetting apiKey],_objCurrentOuction.strOnline] view:self.view];
    objSetting.passResponseDataDelegate=self;
}

-(void)getProfile
{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@FilterUser?userid=%@&username=&email=",[ClsSetting defaultURL],[[NSUserDefaults standardUserDefaults] valueForKey:USER_id]] view:self.view];
    objSetting.passResponseDataDelegate=self;
}

//-(void)passReseposeData:(id)arr
- (void)passGetResponseData:(id)responseObject
{
//    NSError *error;
//    NSMutableDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
    if(tag == 1)
    {
        NSString *PrVat = [responseObject valueForKey:@"PrVat"];
        _objCurrentOuction.strprVat = PrVat;
        
        NSString *isMargin = [responseObject valueForKey:@"isMargin"];
        _objCurrentOuction.isMargin = isMargin;
        
        NSString *isInternational = [responseObject valueForKey:@"isInternational"];
        _objCurrentOuction.isInternational = isInternational;
        
        _objCurrentOuction.astaguruPrice = [responseObject valueForKey:@"astaguruPrice"];
        _objCurrentOuction.usedGoodPercentage = [responseObject valueForKey:@"usedGoodPercentage"];
        _objCurrentOuction.isInternationalGST = [responseObject valueForKey:@"isInternationalGST"];
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:USER_id] intValue] > 0)
        {
            tag = 2;
            [self getProfile];
        }
        [_clvViewAdditionalCgharges reloadData];
    }
    else
    {
        NSMutableArray *arr1=[responseObject valueForKey:@"resource"];
        if (arr1.count>0)
        {
            NSMutableDictionary *dict=[arr1 objectAtIndex:0];
            dict=[ClsSetting RemoveNullOnly:dict];
            //        NSString *strname=[dict valueForKey:@"name"];
            dictUserProfile=dict;
            
            [_clvViewAdditionalCgharges reloadData];
        }
        else
        {
            [ClsSetting ValidationPromt:@"Some thing went wrong"];
        }
    }
}


-(void)setUpNavigationItem
{
    // self.sidebarButton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"signs"] style:UIBarButtonItemStyleDone target:self.revealViewController action:@selector(revealToggle:)];
    // self.sidebarButton.tintColor=[UIColor whiteColor];
    
    self.navigationItem.title=@"Additional Charges";
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    // [[self navigationItem] setLeftBarButtonItem:self.sidebarButton];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    
    UIButton *btnBack = [[UIButton alloc]initWithFrame:CGRectMake(-20, 0, -20, 20)];
    [btnBack setImage:[UIImage imageNamed:@"icon-search"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(searchPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btnBack];
    
    UIButton *btnBack1 = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, -20)];
    [btnBack1 setImage:[UIImage imageNamed:@"icon-myastaguru"] forState:UIControlStateNormal];
    [btnBack1 addTarget:self action:@selector(myastaguru) forControlEvents:UIControlEventTouchUpInside];
    UIButton *btnBack2 = [[UIButton alloc]initWithFrame:CGRectMake(-100, 0, 20, -20)];
    [btnBack2 setImage:[UIImage imageNamed:@"icons8-notification-32"] forState:UIControlStateNormal];
    [btnBack2 addTarget:self action:@selector(myastaguru1) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem1 = [[UIBarButtonItem alloc]initWithCustomView:btnBack1];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc]initWithCustomView:btnBack2];

    UIBarButtonItem *spaceFix = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];
    spaceFix.width = -12;
    UIBarButtonItem *spaceFix1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];
    spaceFix.width = -8;
    UIBarButtonItem *spaceFix2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];
    spaceFix.width = -4;
    [self.navigationItem setRightBarButtonItems:@[spaceFix,barButtonItem,spaceFix1, barButtonItem2,spaceFix2,barButtonItem1]];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"WorkSans-Medium" size:17]}];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (collectionView == _clvViewAdditionalCgharges)
    {
        if (([[[NSUserDefaults standardUserDefaults] valueForKey:USER_id] intValue]>0))
        {
//            return 2;
             return 1;
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return 1;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if (collectionView==_clvViewAdditionalCgharges)
    {
        if (section==0)
        {
            return 1;
        }
        else
        {
            return  1;
        }
    }
    else
    {
        return arrBottomMenu.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell;
   
    AdditionalChargesCollectionViewCell *CurrentSelectedGridCell;
    UserInfoCollectionViewCell *UserInfoCell;
    UICollectionViewCell *cell1;
    
    if (collectionView==_clvViewAdditionalCgharges)
    {
        if (([[[NSUserDefaults standardUserDefaults] valueForKey:USER_id] intValue]>0))
        {
            if (indexPath.section==0)
            {
//                UserInfoCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"BillingInfo" forIndexPath:indexPath];
//                [self setupProfileCell:UserInfoCell];
//                cell = UserInfoCell;
                CurrentSelectedGridCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CurrentSelected1234" forIndexPath:indexPath];
                               [self setupAdditionalChargesCell:CurrentSelectedGridCell];
                               cell = CurrentSelectedGridCell;
            }
            else if (indexPath.section==1)
            {
                CurrentSelectedGridCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CurrentSelected1234" forIndexPath:indexPath];
                [self setupAdditionalChargesCell:CurrentSelectedGridCell];
                cell = CurrentSelectedGridCell;
            }
        }
        else
        {
            if (indexPath.section==0)
            {
                CurrentSelectedGridCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CurrentSelected1234" forIndexPath:indexPath];
                [self setupAdditionalChargesCell:CurrentSelectedGridCell];
                cell = CurrentSelectedGridCell;
            }
        }
    }
    else
    {
        static NSString *identifier = @"Cell11";
        cell1 = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        UILabel *lblTitle = (UILabel *)[cell1 viewWithTag:20];
        lblTitle.text=[arrBottomMenu objectAtIndex:indexPath.row];
        
        UILabel *lblSelectedline = (UILabel *)[cell1 viewWithTag:22];
        lblSelectedline.hidden=YES;
        
        UIButton *btnLive = (UIButton *)[cell1 viewWithTag:23];
        btnLive.layer.cornerRadius = 4;
        btnLive.hidden = YES;
        
        UILabel *lblline = (UILabel *)[cell1 viewWithTag:21];

        if (indexPath.row == 1)
        {
            btnLive.hidden = NO;
        }
        
        if (indexPath.row==1)
        {
            lblTitle.textColor=[UIColor colorWithRed:167.0/255.0 green:142.0/255.0 blue:105.0/255.0 alpha:1];
            
            lblline.backgroundColor=[UIColor colorWithRed:167.0/255.0 green:142.0/255.0 blue:105.0/255.0 alpha:1];
            lblSelectedline.hidden=NO;
        }
        else
        {
            lblTitle.textColor=[UIColor blackColor];//[UIColor colorWithRed:124.0/255.0 green:124.0/255.0 blue:124.0/255.0 alpha:1];
            lblline.backgroundColor=[UIColor colorWithRed:224.0/255.0 green:224.0/255.0 blue:224.0/255.0 alpha:1];
            lblSelectedline.hidden=YES;
        }
        cell=cell1;
    }
    return cell;
}

-(void)setupProfileCell:(UserInfoCollectionViewCell*)UserInfoCell
{
    [self checkBlackOrNot:[dictUserProfile valueForKey:@"name"] label:UserInfoCell.lblBillingName];
    [self checkBlackOrNot:[dictUserProfile valueForKey:@"address1"] label:UserInfoCell.lblBillingAddress];
    [self checkBlackOrNot:[dictUserProfile valueForKey:@"city"] label:UserInfoCell.lblBillingCity];
    [self checkBlackOrNot:[dictUserProfile valueForKey:@"zip"] label:UserInfoCell.lblBillingZip];
    [self checkBlackOrNot:[dictUserProfile valueForKey:@"state"] label:UserInfoCell.lblBillingState];
    [self checkBlackOrNot:[dictUserProfile valueForKey:@"country"] label:UserInfoCell.lblBillingCountry];
    [self checkBlackOrNot:[dictUserProfile valueForKey:@"Mobile"] label:UserInfoCell.lblBillingPhone];
    [self checkBlackOrNot:[dictUserProfile valueForKey:@"email"] label:UserInfoCell.lblBillingEmail];
}

-(NSInteger) getNextValidBidWith:(NSInteger)currentBidPrice percentValue:(NSInteger)percent
{
    NSInteger nextValidBidPrice = 0;
    NSInteger priceIncreaseRate = 0;
    
    
    priceIncreaseRate = (currentBidPrice*percent)/100;
    
    nextValidBidPrice = currentBidPrice + priceIncreaseRate;
    
    return nextValidBidPrice;
}

-(Calculation)calculateAdditionCahrges
{
    
    NSInteger gstValueArtwork = 0;
    NSInteger gstValueBuyersPremium = 0;

    NSInteger margin = 0;
    NSInteger gstOnArtWork = 0;
    NSInteger gstOnBuyersPremium = 0;
    NSInteger customsDuty = 0;
    NSInteger grandTotal = 0;
    NSInteger tcs = 0;
    
    NSString *strPriceUS = [NSString stringWithFormat:@"%@",_objCurrentOuction.strpriceus];
    NSInteger priceUS = [strPriceUS integerValue];
    NSLog(@"%ld",(long)priceUS);
    
    
    
    
    NSString *strPriceRS = [NSString stringWithFormat:@"%@",_objCurrentOuction.strpricers];
    NSInteger priceRS = [strPriceRS integerValue];
    NSLog(@"%ld",(long)priceRS);
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init] ;
    [numberFormatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [numberFormatter setMaximumFractionDigits:0];
    
    NSInteger currentBidPrice = 0;
    
//    if ([[[NSUserDefaults standardUserDefaults] valueForKey:USER_id] intValue] == [_objCurrentOuction.strmyuserid intValue])
//            {
//                strPriceUS = [NSString stringWithFormat:@"%@",_objCurrentOuction.strpriceus];
//                priceUS = [strPriceUS integerValue];
//                strPriceRS = [NSString stringWithFormat:@"%@",_objCurrentOuction.strpricers];
//                priceRS = [self getNextValidBidWith:currentBidPrice percentValue:priceIncreaseRate];
//            }
//    else{
//        strPriceUS = [NSString stringWithFormat:@"%@",_objCurrentOuction.strpriceus];
//        priceUS = [strPriceUS integerValue];
//        strPriceRS = [NSString stringWithFormat:@"%@",_objCurrentOuction.strpricers];
//        priceRS = [strPriceRS integerValue];
//    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
    {
        numberFormatter.currencyCode = @"USD";
        currentBidPrice = priceUS;
        
        gstValueArtwork = 0;
        gstValueBuyersPremium = 0;
    }
    else
    {

            numberFormatter.currencyCode = @"INR";
            currentBidPrice = priceRS;
            
            gstValueArtwork = [_objCurrentOuction.strprVat integerValue];
            gstValueBuyersPremium = 18;

        
    }
    
    NSInteger priceIncreaseRate = 0;
    if (priceRS > 10000000)
    {
        priceIncreaseRate = 5;
    }
    else
    {
        priceIncreaseRate = 10;
    }
    
    NSInteger nextValidBid = [self getNextValidBidWith:currentBidPrice percentValue:priceIncreaseRate];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:USER_id] intValue] == [_objCurrentOuction.strmyuserid intValue])
    {
        nextValidBid = [self getNextValidBidWith:currentBidPrice percentValue:0];
    }
    else{
        nextValidBid = [self getNextValidBidWith:currentBidPrice percentValue:priceIncreaseRate];
    }
    margin = (nextValidBid*15)/100;
    
    NSInteger isMargin = [_objCurrentOuction.isMargin integerValue];

    NSInteger astaguruPrice = [_objCurrentOuction.astaguruPrice integerValue];

    NSInteger isInternational = [_objCurrentOuction.isInternational integerValue];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
    {
        if (isMargin == 1)
        {
            gstOnArtWork = (nextValidBid*gstValueArtwork)/100;
            gstOnBuyersPremium = (margin*gstValueBuyersPremium)/100;
        }
        else
        {
            NSInteger total = nextValidBid + customsDuty +  margin;
            gstOnArtWork = (total*gstValueArtwork)/100;
        }
    }
    else
    {
        if (isInternational == 1)
        {
            //INR value with astaguruPrice international calculatuon
            if (astaguruPrice > 0)
            {
                customsDuty = (astaguruPrice*11)/100;

                astaguruPrice = astaguruPrice + customsDuty;
                
                NSInteger marginValue = nextValidBid + margin;
                marginValue = marginValue - astaguruPrice;
                gstOnArtWork = (marginValue*gstValueArtwork)/100;
            }
            else
            {
                customsDuty = (nextValidBid*11)/100;

                if (isMargin == 1)
                {
                    gstOnArtWork = (nextValidBid*gstValueArtwork)/100;
                    gstOnBuyersPremium = (margin*gstValueBuyersPremium)/100;
                }
                else
                {
                    NSInteger total = nextValidBid + customsDuty +  margin;
                    gstOnArtWork = (total*gstValueArtwork)/100;
                }
            }
        }
        else
        {
            if (astaguruPrice > 0)
            {
                NSInteger usedGoodPercentage = 0;
                if (![_objCurrentOuction.usedGoodPercentage isEqual: [NSNull null]])
                {
                    usedGoodPercentage = [_objCurrentOuction.usedGoodPercentage integerValue];
                }
                
                usedGoodPercentage = (nextValidBid*usedGoodPercentage)/100;
                
                NSInteger purchaseValue = nextValidBid - usedGoodPercentage;
                
                NSInteger usedGoodValue = (nextValidBid + margin) - purchaseValue;
                gstOnArtWork = (usedGoodValue*gstValueArtwork)/100;
            }
            else
            {
                if (isMargin == 1)
                {
                    gstOnArtWork = (nextValidBid*gstValueArtwork)/100;
                    gstOnBuyersPremium = (margin*gstValueBuyersPremium)/100;
                }
                else
                {
                    NSInteger total = nextValidBid + customsDuty +  margin;
                    gstOnArtWork = (total*gstValueArtwork)/100;
                }
            }
        }
    }
    

    if ((isInternational == 1) && (astaguruPrice > 0))
    {
        grandTotal = nextValidBid + margin + gstOnArtWork + gstOnBuyersPremium;
    }
    else{
        grandTotal = nextValidBid + customsDuty + margin + gstOnArtWork + gstOnBuyersPremium;
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
       {
    tcs = 0;
       }
    else{
         tcs = (grandTotal/100)*0.075;
    }
    NSLog(@"%ld", (long)tcs);
    grandTotal = grandTotal+tcs;
    
    Calculation c;
    c.isMargin = isMargin;
    c.isInternational = isInternational;
    c.astaguruPrice = astaguruPrice;
    
    c.nextVaidBid = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:nextValidBid]];
    
    c.currentVaidBid = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:currentBidPrice]];
    
    c.margin =  [numberFormatter stringFromNumber:[NSNumber numberWithInteger:margin]];
    c.gst = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:gstOnArtWork]];
    c.gstOnBuyersPremium = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:gstOnBuyersPremium]];
    c.customsDuty = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:customsDuty]];
    c.total = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:grandTotal]];
    c.tcs = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:tcs]];
    
    
    
    return c;
}

-(void)setupAdditionalChargesCell:(AdditionalChargesCollectionViewCell*)currentSelectedGridCell
{
    NSInteger gstValueArtwork = 0;
    NSInteger gstValueBuyersPremium = 0;
    NSString *customsDutyText = @"";

    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
    {
        gstValueArtwork = 0;
        gstValueBuyersPremium = 0;
        
        customsDutyText = @"Custom Duty";
    }
    else
    {
        gstValueArtwork = [_objCurrentOuction.strprVat integerValue];
        gstValueBuyersPremium = 18;
        
        customsDutyText = @"11% Custom Duty (Buyers importing in India)";
    }
    
    Calculation c = [self calculateAdditionCahrges];
    
    if (c.isInternational)
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isUSD"])
        {
            currentSelectedGridCell.lbl_customDutyTxt.text = @"Custom Duty";
            currentSelectedGridCell.lbl_customDuty.text = @"Country Dependent";
            currentSelectedGridCell.lbl_internationalText.text = @"The 'Total' price does not include the custom duty as it is country dependent. Please note it will be an addition to the price mentioned above.";
        }
        else
        {
            if (c.astaguruPrice > 0)
            {
                currentSelectedGridCell.lbl_customDutyTxt.text = @"";
                currentSelectedGridCell.lbl_customDuty.text = @"";
            }
            else{
                currentSelectedGridCell.lbl_customDutyTxt.text = @"11% Custom Duty (Buyers importing in India)";
                currentSelectedGridCell.lbl_customDuty.text = c.customsDuty;
            }
            
            currentSelectedGridCell.lbl_internationalText.text = @"";

        }
    }
    else
    {
        currentSelectedGridCell.lbl_customDutyTxt.text = @"";
        currentSelectedGridCell.lbl_customDuty.text = @"";
        
        if (c.astaguruPrice > 0)
        {
            NSInteger usedGoodPercentage = 0;
            if (![_objCurrentOuction.usedGoodPercentage isEqual: [NSNull null]])
            {
                usedGoodPercentage = [_objCurrentOuction.usedGoodPercentage integerValue];
            }

            NSInteger temp = 15 + usedGoodPercentage;
            
            NSString *strprVat = [ClsSetting TrimWhiteSpaceAndNewLine:_objCurrentOuction.strprVat];
            
            NSString *noteStr = [NSString stringWithFormat: @"Note: The GST on this lot is charged at %@ %@ on %ld %@ total Astaguru margin (buyer + seller) in accordance with 32(5) of CGST Rule 2017.", strprVat, @"%", (long)temp, @"%"];
            currentSelectedGridCell.lbl_internationalText.text = noteStr;
        }
        else
        {
            currentSelectedGridCell.lbl_internationalText.text = @"";
        }
    }
    NSLog(@"%@",c.nextVaidBid);
    NSLog(@"%@",c.currentVaidBid);
    NSLog(@"%d",[_objCurrentOuction.strpricelow intValue]);
    NSLog(@"%d",[_objCurrentOuction.strpricers intValue]);
    
    NSString *strPricelow = [NSString stringWithFormat:@"%@",_objCurrentOuction.strpricelow];
    NSInteger pricelow = [strPricelow integerValue];
    NSLog(@"%ld",(long)pricelow);
    
    NSString *strPricers = [NSString stringWithFormat:@"%@",_objCurrentOuction.strpricers];
    NSInteger pricers = [strPricers integerValue];
    NSLog(@"%ld",(long)pricers);
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:USER_id] intValue] == [_objCurrentOuction.strmyuserid intValue])
    {
        currentSelectedGridCell.lbl_hammerPrice.text = c.currentVaidBid;
//        if (pricelow > pricers){
//                currentSelectedGridCell.lbl_hammerPrice.text = c.nextVaidBid;
//        }
//        else{
//            currentSelectedGridCell.lbl_hammerPrice.text = c.currentVaidBid;
//        }
    }
    else{
            currentSelectedGridCell.lbl_hammerPrice.text = c.nextVaidBid;
    }
    
//    currentSelectedGridCell.lbl_hammerPrice.text = c.nextVaidBid;
        
    
    currentSelectedGridCell.lbl_marginTxt.text = @"15% Margin";
    currentSelectedGridCell.lbl_margin.text = c.margin;
    
    if (c.astaguruPrice > 0)
    {
        currentSelectedGridCell.lbl_gstOnArtworkTxt.text = [NSString stringWithFormat:@"GST on Lot (%ld %s)*",(long)gstValueArtwork,"%"];
    }
    else
    {
        currentSelectedGridCell.lbl_gstOnArtworkTxt.text = [NSString stringWithFormat:@"GST on Lot (%ld %s)",(long)gstValueArtwork,"%"];
    }
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"isUSD"])
    {
        currentSelectedGridCell.lbl_tcsHeaderText.text = @"TCS(0%)";
    }
    else{
        currentSelectedGridCell.lbl_tcsHeaderText.text = @"TCS(0.075%)";
    }
    currentSelectedGridCell.lbl_tcsText.text = c.tcs;
   
    currentSelectedGridCell.lbl_gstOnArtwork.text = c.gst;
    
    if (c.isMargin)
    {
        currentSelectedGridCell.lbl_gstOnBuyersPremiumTxt.text = [NSString stringWithFormat:@"%ld %s GST on Buyers Premium", (long)gstValueBuyersPremium, "%"];
        currentSelectedGridCell.lbl_gstOnBuyersPremium.text = c.gstOnBuyersPremium;
    }
    else
    {
        currentSelectedGridCell.lbl_gstOnBuyersPremiumTxt.text = @"";
        currentSelectedGridCell.lbl_gstOnBuyersPremium.text = @"";
    }
    
    currentSelectedGridCell.lbl_GrandTotal.text = c.total;
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"isUSD"])
    {
        currentSelectedGridCell.lblEstimation.text=_objCurrentOuction.strestamiate;
    }
    else
    {
        currentSelectedGridCell.lblEstimation.text=_objCurrentOuction.strcollectors;
    }
    
    //Check is "Collectibles Auction"
    if ([_objCurrentOuction.auctionType intValue] != 1)
    {
        UIView *subvuew = (UIView*) [currentSelectedGridCell viewWithTag:10];
        UILabel *Lbl_1 = (UILabel *)[subvuew viewWithTag:1];
        Lbl_1.text = @"Title: ";
        UILabel *Lbl_2 = (UILabel *)[subvuew viewWithTag:2];
        Lbl_2.text = @"Category: ";
        UILabel *Lbl_3 = (UILabel *)[subvuew viewWithTag:3];
        Lbl_3.text = @" ";
        UILabel *Lbl_4 = (UILabel *)[subvuew viewWithTag:4];
        Lbl_4.text = @" ";
        UILabel *Lbl_5 = (UILabel *)[subvuew viewWithTag:5];
        Lbl_5.text = @" ";
        
        currentSelectedGridCell.lblArtistName.text=_objCurrentOuction.strtitle;
        // NSString *ht = [ClsSetting getAttributedStringFormHtmlString:_objCurrentOuction.strPrdescription];
        currentSelectedGridCell.lblMedium.text= _objCurrentOuction.strcategory;//ht;
        currentSelectedGridCell.lblYear.text = @" ";
        currentSelectedGridCell.lblSize.text = @" ";////[NSString stringWithFormat:@"%@ in",_objCurrentOuction.strproductsize];
        
        currentSelectedGridCell.lblEstimation.text = @" ";
    }
    else
    {
        currentSelectedGridCell.lblArtistName.text=[NSString stringWithFormat:@"%@ %@",_objCurrentOuction.strFirstName,_objCurrentOuction.strLastName];
        
        currentSelectedGridCell.lblMedium.text=[NSString stringWithFormat:@"%@",_objCurrentOuction.strmedium];
        
        currentSelectedGridCell.lblSize.text=[NSString stringWithFormat:@"%@ in",_objCurrentOuction.strproductsize];
        
        currentSelectedGridCell.lblYear.text=[NSString stringWithFormat:@"%@",_objCurrentOuction.strproductdate];
    }
    
    currentSelectedGridCell.lblProductName.text= _objCurrentOuction.strtitle;
    currentSelectedGridCell.imgProduct.imageURL=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[ClsSetting imageURL], _objCurrentOuction.strthumbnail]];
    [currentSelectedGridCell.btnLot setTitle:[NSString stringWithFormat:@"Lot:%@",[ClsSetting TrimWhiteSpaceAndNewLine:_objCurrentOuction.strReference]] forState:UIControlStateNormal];
}

- (CGSize)collectionView:(UICollectionView *)collectionView1 layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView1==_clvViewAdditionalCgharges)
    {
        if (([[[NSUserDefaults standardUserDefaults] valueForKey:USER_id] intValue]>0))
        {
            if (indexPath.section==0)
            {
//                return   CGSizeMake(collectionView1.frame.size.width,270);
                return   CGSizeMake(collectionView1.frame.size.width,500);
            }
            else
            {
                return   CGSizeMake(collectionView1.frame.size.width,440);
            }
        }
        else
        {
            return   CGSizeMake(collectionView1.frame.size.width,500);
        }
    }
    else
    {
        float width=(self.view.frame.size.width/4);
        //NSLog(@"%f",width);
        
        return CGSizeMake(width, collectionView1.frame.size.height);
    }
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)updateBillingAddress:(id)sender
{
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:USER_id] intValue]>0)
    {
        UIStoryboard *storyboard=[UIStoryboard storyboardWithName:@"SignIn" bundle:nil];
        MyProfileViewController *objMyProfileViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyProfileViewController"];
        
        
        [self.navigationController pushViewController:objMyProfileViewController animated:YES];
    }
    else
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SignIn" bundle:nil];
        BforeLoginViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"BforeLoginViewController"];
        [self.navigationController pushViewController:rootViewController animated:YES];
    }
}

-(void)checkBlackOrNot:(NSString*)str label:(UILabel*)lbl;
{
    if (str.length==0)
    {
        lbl.text=@"--";
    }
    else
    {
        lbl.text=str;
    }
}
@end
