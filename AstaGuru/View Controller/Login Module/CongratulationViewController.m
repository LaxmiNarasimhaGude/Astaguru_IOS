//
//  CongratulationViewController.m
//  AstaGuru
//
//  Created by sumit mashalkar on 10/09/16.
//  Copyright © 2016 Aarya Tech. All rights reserved.
//

#import "CongratulationViewController.h"
#import "CurrentOccutionViewController.h"
#import "ClsSetting.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "NotifiactionManger.h"
@interface CongratulationViewController ()

@end

@implementation CongratulationViewController
@synthesize dict;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self SendEmail];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self setUpNavigationItem];
    _btnViewconnentAuctions .layer.cornerRadius=2;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpNavigationItem
{
    UIButton *btnBack = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    [btnBack setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    // [btnBack addTarget:self action:@selector(backPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    
    self.navigationItem.title=@"Complete Sign Up";
    self.sideleftbarButton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-close"] style:UIBarButtonItemStyleDone target:self action:@selector(closePressed)];
    self.sideleftbarButton.tintColor=[UIColor whiteColor];
    [[self navigationItem] setRightBarButtonItem:self.sideleftbarButton];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"WorkSans-Medium" size:17]}];
    
}

- (IBAction)btnProccedPressed:(id)sender
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CurrentOccutionViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"CurrentOccutionViewController"];
    [self.navigationController pushViewController:rootViewController animated:YES];
}
-(void)closePressed
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SWRevealViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    UIViewController *viewController =rootViewController;
    AppDelegate * objApp = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    objApp.window.rootViewController = viewController;
}


-(void)SendEmail
{
//    NSMutableDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];

    NSDictionary *dictTo = @{
                             @"name":[NSString stringWithFormat:@"%@",_strname],
                             @"email":_strEmail,
                             };
    
    NSArray*arrTo=[[NSArray alloc]initWithObjects:dictTo, nil];
    NSString *strUserFirstName=[[NSUserDefaults standardUserDefaults]valueForKey:USER_FirstName];

    // NSDictionary *dictMail=[[NSDictionary alloc]init];
    NSDictionary *dictMail = @{
                               @"template":@"newsletter",
                               @"to":arrTo,
                               @"subject":@"Your Registration with Astaguru.com is confirmed & complete.",
//
                               @"body_text": [NSString stringWithFormat:@"Dear %@ %@,<br/><br/> <b>Congratulations!</b></b> Your AstaGuru account is now active.<br/>Your AstaGuru Login-ID and Password are provided below. Please use the same Login-Id and Password to access your account.<br/><br/><b>Your Login Id:</b> %@<br/><b>Password:</b> %@<br/><br/><b>Please Note:</b> <br/>In order to participate in our auctions first time bidders will need <b>‘Bidding Access‘</b> that will be provided only upon completion of the following verification process.<br/><br/><b>A:</b> Login to Astaguru.com and upload your KYC documents in <b>‘My Profile’</b> section.<br/><br/><b>B:</b> A telephonic verification with our representative. You will be contacted on the registered mobile number, please note this is an onetime procedure.<br/><br/><b>C:</b> First time bidders are requested to transfer a sum of Rs. 5,00,000 or equivalent US$ value as an onetime <b>‘Security Deposit’.</b> Please note this amount will be adjusted in case you have won any lot(s). If you have not acquired any lot(s), the amount will be credited in your respective bank account within 7 working days<br/><br/><b>Mode of Transfer:</b> DD/RTGS/NEFT<br/><b>AstaGuru Bank Details:</b><br/><b>Name of the Beneficiary:</b> Astaguru.com<br/><b>Bank Name:</b> ICICI Bank,<br/><b>Branch:</b> 240 Navsari Building, D. N. Road, Fort, Mumbai - 400001. India.<br/><b>Account No:</b> 623505385049<br/><b>Swift Code:</b> ICICINBBCTS<br/><b>RTGS / NEFT/ IFSC Code:</b> ICIC0006235<br/><br/>*Please do share the details of your bank account in which you would like AstaGuru to credit the balance / full ‘Security Deposit’ amount in the <b>‘My Profile’</b> section.<br/><br/>We sincerely appreciate your due diligent response. As part of AstaGuru’s objective of security and procedural efficacy we request you to complete the verification process as advised. Failing to complete the same would result in your account being deactivated during the course of our auctions. Rest assured that customer confidentially is of utmost importance to us. Your information will be never be shared with any third party.<br/><br/>Thank You & Warm Regards,<br/><br/>Team AstaGuru." ,_strname,_strlastname,dict[@"username"],dict[@"password"]],
                               
                               };
    NSString *notificationMsg = [NSString stringWithFormat:@"Dear %@ thank you for registering with AstaGuru Auction House",_strname];
    
    [[NotifiactionManger sharedInstance]sendNotificationMsg:@"First Time login Welcome Message" messageInfo:notificationMsg];
    [ClsSetting sendEmailWithInfo:dictMail];
}

@end
