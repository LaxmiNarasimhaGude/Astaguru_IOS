//
//  Country.m
//  AstaGuru
//
//  Created by Apple.Inc on 26/08/19.
//  Copyright © 2019 4Fox Solutions. All rights reserved.
//

#import "Country.h"

@implementation Country

+ (NSArray *)coutrysFromJSON:(NSArray *)responseObject
{
    NSMutableArray *countrys = [[NSMutableArray alloc] init];
    
    for (NSDictionary *countryDic in responseObject) {
        Country *country = [[Country alloc] init];
        
        for (NSString *key in countryDic) {
            if ([country respondsToSelector:NSSelectorFromString(key)]) {
                [country setValue:[countryDic valueForKey:key] forKey:key];
            }
        }
        [countrys addObject:country];
    }
    
    return countrys;
}

@end

@implementation State

+ (NSArray *)statesFromJSON:(NSArray *)responseObject
{
    NSMutableArray *states = [[NSMutableArray alloc] init];
    
    for (NSDictionary *stateDic in responseObject) {
        State *state = [[State alloc] init];
        
        for (NSString *key in stateDic) {
            if ([state respondsToSelector:NSSelectorFromString(key)]) {
                [state setValue:[stateDic valueForKey:key] forKey:key];
            }
        }
        [states addObject:state];
    }
    
    return states;
}

@end


@implementation City

+ (NSArray *)citysFromJSON:(NSArray *)responseObject
{
    NSMutableArray *citys = [[NSMutableArray alloc] init];
    
    for (NSDictionary *cityDic in responseObject) {
        City *city = [[City alloc] init];
        
        for (NSString *key in cityDic) {
            if ([city respondsToSelector:NSSelectorFromString(key)]) {
                [city setValue:[cityDic valueForKey:key] forKey:key];
            }
        }
        [citys addObject:city];
    }
    
    return citys;
}

@end

@implementation Interest

+ (NSArray *)interestsFromJSON:(NSArray *)responseObject
{
    NSMutableArray *interests = [[NSMutableArray alloc] init];
    
    for (NSDictionary *interestDic in responseObject) {
        Interest *interest = [[Interest alloc] init];
        
        for (NSString *key in interestDic) {
            if ([interest respondsToSelector:NSSelectorFromString(key)]) {
                [interest setValue:[interestDic valueForKey:key] forKey:key];
            }
        }
        [interests addObject:interest];
    }
    
    return interests;
}
@end

@implementation About

+ (NSArray *)aboutsFromJSON:(NSArray *)responseObject
{
    NSMutableArray *abouts = [[NSMutableArray alloc] init];
    
    for (NSDictionary *aboutDic in responseObject) {
        About *about = [[About alloc] init];
        
        for (NSString *key in aboutDic) {
            if ([about respondsToSelector:NSSelectorFromString(key)]) {
                [about setValue:[aboutDic valueForKey:key] forKey:key];
            }
        }
        [abouts addObject:about];
    }
    
    return abouts;
}

@end
