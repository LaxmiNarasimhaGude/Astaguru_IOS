//
//  RegistrationViewController.m
//  AstaGuru
//
//  Created by sumit mashalkar on 09/09/16.
//  Copyright © 2016 Aarya Tech. All rights reserved.
//

#import "RegistrationViewController.h"
#import "VerificationViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "ClsSetting.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "Country.h"
#import "SearchResultsTableViewController.h"
#import "DropDownListView.h"
#import "JTSImageViewController.h"
#import "JTSImageInfo.h"
#import "CommonWebviewViewController.h"

@interface RegistrationViewController ()<PassResponse,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, SearchResultsDelegate, UISearchResultsUpdating, kDropDownListViewDelegate>
{
    NSString *strEmailCode;
    NSString *strSMSCode;
    
    int isCheckTermsAndConditions;
    
    NSDictionary *paradict;
    
    int isPanCardSelect;
    int isAadharCardSelect;
    
    //NSString *genderId;
    NSString *aboutId;
    NSString *strInterestedIds;
    
    Country *selectedPostalCountry;
    State *selectedPostalState;
    City *selectedPostalCity;
//    Countryid *selectedPostalCountryId;
    
    UIImagePickerController *ipc;
    
    ImagePikerType imagePikerType;
    
    MBProgressHUD *HUD;
        
    NSString *apiCallStr;
    
    SearchResultType searchResultType;
    
    DropDownListView * dropObj;
    
    UINavigationController *navController;

}

@property (nonatomic, retain) NSArray *intrests;
@property (nonatomic, retain) NSArray *abouts;
@property (nonatomic, retain) NSArray *coutrys;
@property (nonatomic, retain) NSArray *states;
@property (nonatomic, retain) NSArray *citys;

@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResults; // Filtered search results

@end

@implementation RegistrationViewController

- (void)viewDidLoad
{
    // Do any additional setup after loading the view.
    [super viewDidLoad];
    
     navController = [[UINavigationController alloc] init];
    
    self.navigationItem.title=@"Sign Up";

    [self setUpNavigationItem];
    
    [self setBroder];
    
    isCheckTermsAndConditions = 0;
    _imgCheckTermsAndCondition.image=[UIImage imageNamed:@""];
    _imgCheckTermsAndCondition.layer.cornerRadius = 10;
    _imgCheckTermsAndCondition.clipsToBounds = YES;

    
    [self getInterestMaster];
    
    isPanCardSelect = 0;
    isAadharCardSelect = 0;
    
//    UITapGestureRecognizer *panCardPhoto_TapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(view_panCardPhoto_tap:)];
//    [_view_panCardPhoto addGestureRecognizer:panCardPhoto_TapRecognizer];
//    _view_panCardPhoto.tag = 11;
//
//    UITapGestureRecognizer *aadharCardPhoto_TapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(view_aadharPhoto_tap:)];
//    [_view_aadharPhoto addGestureRecognizer:aadharCardPhoto_TapRecognizer];
//    _view_aadharPhoto.tag = 22;
    
    imagePikerType = None;
    
    strInterestedIds = @"";

    
    _txt_aboutUs.delegate = self;
    _txt_interestedIn.delegate = self;
    
    _txtcountry.delegate = self;
    _txtState.delegate = self;
    _txtCity.delegate = self;
    _txt_birthDay.delegate = self;
    _txt_birthMonth.delegate = self;
    _txt_birthYear.delegate = self;
    _txtZip.delegate = self;
    
    NSArray *arr1 = (NSArray*)[[NSUserDefaults standardUserDefaults] valueForKey:@"coutrys"];
    NSArray *countrysResult = [Country coutrysFromJSON:arr1];
    self.coutrys = countrysResult;
    
    
    //Configure SearchController
    // Create a mutable array to contain products for the search results table.
    self.searchResults = [[NSMutableArray alloc] init];
    
    // The table view controller is in a nav controller, and so the containing nav controller is the 'search results controller'
    UINavigationController *searchResultsController = [[self storyboard] instantiateViewControllerWithIdentifier:@"TableSearchResultsNavController"];
    SearchResultsTableViewController *vc = (SearchResultsTableViewController *)searchResultsController.topViewController;
    vc.searchResultsDelegate = self;
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultsController];
    self.searchController.searchResultsController.automaticallyAdjustsScrollViewInsets = NO;
    self.searchController.dimsBackgroundDuringPresentation = NO; // default is YES
    
    self.searchController.searchResultsUpdater = self;
    
    //self.searchController.searchBar.delegate = self;
    
    self.searchController.searchBar.barTintColor = [UIColor blackColor];
    self.searchController.searchBar.backgroundColor = [UIColor blackColor];
    self.searchController.searchBar.translucent = NO;
    self.searchController.searchBar.tintColor = [UIColor colorWithRed:130.0f/255.0f green:103.0f/255.0f blue:67.0f/255.0f alpha:1];
    
    self.definesPresentationContext = YES;
}

- (void)showDatePicker:(UITextField*)textFiled
{
    textFiled.tintColor = [UIColor whiteColor];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:-18];
    NSDate *maxDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    [comps setYear:-150];
    NSDate *minDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    
    [datePicker setMinimumDate:minDate];
    [datePicker setMaximumDate:maxDate];
    [datePicker setDate:maxDate];
    

    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    [textFiled setInputView:datePicker];
}

-(void)updateTextField:(UIDatePicker*)sender
{
    NSLog(@"Date == %@", sender.date);
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:sender.date];
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    _txt_birthDay.text = [NSString stringWithFormat: @"%ld", (long)day];
    _txt_birthMonth.text = [NSString stringWithFormat: @"%ld", (long)month];
    _txt_birthYear.text = [NSString stringWithFormat: @"%ld", (long)year];
}

-(void) setBroder
{
    UIColor *bColor = [UIColor colorWithRed:219.0f/255.0f green:219.0f/255.0f blue:219.0f/255.0f alpha:1];
    [ClsSetting SetBorder:_btnProceed cornerRadius:5 borderWidth:0 color:bColor];

    [ClsSetting SetBorder:_fName_View cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_lName_View cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_address_View cornerRadius:2 borderWidth:1 color:bColor];
    
    [ClsSetting SetBorder:_address2_View cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_city_View cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_country_View cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_state_View cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_zip_View cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_mobile_View cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_telephone_View cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_fax_View cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_email_View cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_userName_View cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_password_View cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_confirmPassword_View cornerRadius:2 borderWidth:1 color:bColor];
    
    [ClsSetting SetBorder:_view_birthDay cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_view_birthMonth cornerRadius:2 borderWidth:1 color:bColor];
    
    [ClsSetting SetBorder:_view_birthYear cornerRadius:2 borderWidth:1 color:bColor];
    
    [ClsSetting SetBorder:_view_aboutUs cornerRadius:2 borderWidth:1 color:bColor];

    [ClsSetting SetBorder:_view_insterstedIn cornerRadius:2 borderWidth:1 color:bColor];

}

-(void)closePressed
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpNavigationItem
{
    UIButton *btnBack = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    [btnBack setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    // [btnBack addTarget:self action:@selector(backPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    
    
    self.sideleftbarButton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-close"] style:UIBarButtonItemStyleDone target:self action:@selector(closePressed)];
    self.sideleftbarButton.tintColor=[UIColor whiteColor];
    [[self navigationItem] setRightBarButtonItem:self.sideleftbarButton];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"WorkSans-Medium" size:17]}];
}


- (IBAction)btnAgreePressed:(id)sender
{
    if (isCheckTermsAndConditions==0)
    {
        isCheckTermsAndConditions = 1;
        _imgCheckTermsAndCondition.image=[UIImage imageNamed:@"img-checkbox-checked"];
    }
    else
    {
        isCheckTermsAndConditions = 0;
        _imgCheckTermsAndCondition.image=[UIImage imageNamed:@""];
    }
}

- (IBAction)btnTermsAndConditions:(id)sender {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
               CommonWebviewViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"CommonWebviewViewController"];
               rootViewController.url = [NSURL URLWithString:@"https://www.astaguru.com/MobileApp/TermsAndConditions"];
               rootViewController.title = @"Terms & Conditions";
    [self.navigationController pushViewController:rootViewController animated:YES];

//               [navController setViewControllers: @[rootViewController] animated: YES];
//               [self.revealViewController setFrontViewController:navController];
//               [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    
//    Title = "Terms & Conditions";
//           Url = "https://www.astaguru.com/MobileApp/TermsAndConditions";
    
}


- (IBAction)btnProccedPressed:(id)sender
{
    if ([self validate])
    {
        if (isCheckTermsAndConditions)
        {
            [self checkEmailUserNameExist];
        }
        else
        {
            [ClsSetting ValidationPromt:@"Accept Terms and Conditions"];
        }
    }
}






- (IBAction)btn_male_pressed:(UIButton *)sender
{
    [self.btn_male setSelected:TRUE];
    [self.btn_female setSelected:FALSE];
    [self.btn_other setSelected:FALSE];
}

- (IBAction)btn_female_pressed:(UIButton *)sender
{
    [self.btn_male setSelected:FALSE];
    [self.btn_female setSelected:TRUE];
    [self.btn_other setSelected:FALSE];
}

- (IBAction)btn_other_pressed:(UIButton *)sender
{
    [self.btn_male setSelected:FALSE];
    [self.btn_female setSelected:FALSE];
    [self.btn_other setSelected:TRUE];
}



- (IBAction)no:(UIButton *)sender {
   [self.btn_yes setSelected:FALSE];
   [self.btn_no setSelected:TRUE];
}

- (IBAction)yes:(id)sender {
   [self.btn_yes setSelected:TRUE];
   [self.btn_no setSelected:FALSE];
}

-(BOOL)validate
{
    if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtFirstName.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter First Name"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtLastName.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Last Name"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtAddress.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Address"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtCity.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter City"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtcountry.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Country"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtState.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter State"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtZip.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Zip Code"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtCountryCode.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Country Code"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtMobileNumber.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Mobile Number"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtMobileNumber.text].length<10)
    {
        [ClsSetting ValidationPromt:@"Enter Valid Mobile Number"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtTelephoneNumber.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Valid Telephone Number"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtEmail.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Email Id"];
        return NO;
    }
    else if (![ClsSetting NSStringIsValidEmail:_txtEmail.text])
    {
        [ClsSetting ValidationPromt:@"Enter Valid Email Id"];
        return NO;
    }
    if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_birthDay.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Birth Day"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_birthMonth.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Birth Month"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_birthYear.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Birth Year"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_aboutUs.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Select about you here"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_interestedIn.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Select Interested In"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtUserName.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Username"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtPassword.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Password"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtPassword.text].length<6)
    {
        [ClsSetting ValidationPromt:@"Password Not Less Than 6 Digits"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txtConfarmPassword.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Re-enter Password"];
        return NO;
    }
    else if (![_txtPassword.text isEqualToString:_txtConfarmPassword.text])
    {
        [ClsSetting ValidationPromt:@"Password Does Not Match"];
        return NO;
    }
    return YES;
}

#pragma mark - SearchResultsDelegate
-(void)didSelectSearchResult:(NSString *)item
{
    self.searchController.searchBar.text = @"";
    if (searchResultType == PostalCountry)
    {

        selectedPostalCountry = [[self.coutrys filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.countryname == %@",item]] objectAtIndex:0];

        _txtcountry.text = selectedPostalCountry.countryname;

        _txtCountryCode.text = selectedPostalCountry.countrycode;
        
//        NSNumber *myNumber = selectedPostalCountry.countrycode;
//        NSString *myNumberInString = [myNumber stringValue];
//        
//        NSLog(@"%@",myNumberInString);
        
        _txtCountryCode.enabled = NO;
        [_txtCountryCode resignFirstResponder];
        _txtState.text = @"";
        selectedPostalState = nil;
        
        _txtCity.text = @"";
        selectedPostalCity = nil;
    }
    if (searchResultType == PostalState)
    {
        selectedPostalState =  [[self.states filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.statename == %@",item]] objectAtIndex:0];
        _txtState.text = selectedPostalState.statename;
        
        _txtCity.text = @"";
        selectedPostalCity = nil;
    }
    if (searchResultType == PostalCity)
    {
        selectedPostalCity = [[self.citys filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.cityname == %@",item]] objectAtIndex:0];
        _txtCity.text = selectedPostalCity.cityname;
    }
    [self dismissViewControllerAnimated:true completion:nil];
}

#pragma mark - UISearchResultsUpdating

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    self.searchController.searchResultsController.view.hidden = NO;
    
    NSString *searchString = [self.searchController.searchBar text];
    
    [self updateFilteredContentForProductName:searchString];
    
    if (self.searchController.searchResultsController)
    {
        //NSLog(@"searchResults == %lu", (unsigned long)self.searchResults.count);
        UINavigationController *navController = (UINavigationController *)self.searchController.searchResultsController;
        SearchResultsTableViewController *vc = (SearchResultsTableViewController *)navController.topViewController;
        vc.searchResults = self.searchResults;
        [vc.tableView reloadData];
    }
    
}


#pragma mark - Content Filtering

- (void)updateFilteredContentForProductName:(NSString *)searchString
{
    NSArray *searchingFrom;
    if ((searchResultType == PostalCountry) || (searchResultType == BillingCountry))
    {
        searchingFrom =  [self.coutrys valueForKey:@"countryname"];  //self.coutrys;
    }
    else if ((searchResultType == PostalState) || (searchResultType == BillingState))
    {
        searchingFrom = [self.states valueForKey:@"statename"]; //self.states;
    }
    else if ((searchResultType == PostalCity) || (searchResultType == BillingCity))
    {
        searchingFrom = [self.citys valueForKey:@"cityname"]; //self.city;
    }
    
    // Update the filtered array based on the search text and scope.
    if ((searchString == nil) || [searchString length] == 0)
    {
        self.searchResults = [searchingFrom mutableCopy];
        return;
    }
    
    [self.searchResults removeAllObjects]; // First clear the filtered array.
    
    /*  Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
     */
    for (NSString *countryname in searchingFrom)
    {
        NSUInteger searchOptions = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;
        NSRange productNameRange = NSMakeRange(0, countryname.length);
        NSRange foundRange = [countryname rangeOfString:searchString options:searchOptions range:productNameRange];
        if (foundRange.length > 0) {
            [self.searchResults addObject:countryname];
        }
    }
}

#pragma mark - UITextFieldDelegate Delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (textField==_txtMobileNumber)
    {
        return newLength <= 10;
    }
    else if (textField == _txtTelephoneNumber)
    {
        return newLength <= 15;
    }
    else if (textField == _txtFaxNumber)
    {
        return newLength <= 15;
    }
    else if (textField == _txt_birthDay || textField == _txt_birthMonth)
    {
        return newLength <= 2;
    }
    else if (textField == _txt_birthYear)
    {
        return newLength <= 4;
    }
    else if (textField == _txtZip)
    {
        return newLength <= 10;
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [dropObj fadeOut];
    if ((textField == _txt_birthDay) || (textField == _txt_birthMonth) || (textField == _txt_birthYear))
    {
        [self showDatePicker:textField];
    }
    else if (textField == _txtcountry)
    {
        [self.view endEditing:YES];

        searchResultType = PostalCountry;
        
        if (self.coutrys.count == 0 || selectedPostalCountry == nil)
        {
            [self getCountry];
        }
        else
        {
            [self presentViewController:self.searchController animated:true completion:nil];
            self.searchController.searchResultsController.view.hidden = NO;
        }
        return NO;
    }
    else if (textField == _txtState)
    {
        [self.view endEditing:YES];

        searchResultType = PostalState;
        
        if (self.states.count == 0 || selectedPostalState == nil)
        {
            if (selectedPostalCountry == nil)
            {
                [ClsSetting ValidationPromt:@"Select Postal Address Country"];
            }
            else
            {
                [self getStateWithCountryId:selectedPostalCountry.countryid];
            }
        }
        else
        {
            [self presentViewController:self.searchController animated:true completion:nil];
            self.searchController.searchResultsController.view.hidden = NO;
            
        }
        return NO;
    }
    else if (textField == _txtCity)
    {
        [self.view endEditing:YES];

        searchResultType = PostalCity;
        
        if (self.citys.count == 0 || selectedPostalCity == nil)
        {
            if (selectedPostalState == nil)
            {
                [ClsSetting ValidationPromt:@"Enter State"];
            }
            else
            {
                [self getCityWithStateId:selectedPostalState.stateid];
            }
        }
        else
        {
            [self presentViewController:self.searchController animated:true completion:nil];
            self.searchController.searchResultsController.view.hidden = NO;
        }
        return NO;
    }
    else if (textField == _txt_interestedIn)
    {
        [self.view endEditing:YES];

        if ([self.intrests count] > 0)
        {
            NSArray *arrOptions = [self.intrests valueForKey:@"interest"];
            [dropObj fadeOut];
            [self showPopUpWithTitle:@"Select Interest" withOption:arrOptions xy:CGPointMake(((SCREEN_WIDTH/2) - (287/2)), 130) size:CGSizeMake(287, SCREEN_HEIGHT/1.8) isMultiple:YES];
        }
        else
        {
            [self getInterestMaster];
        }
        return NO;
    }
    else if (textField == _txt_aboutUs)
    {
        [self.view endEditing:YES];

        if ([self.abouts count] > 0)
        {
            NSArray *arrOptions = [self.abouts valueForKey:@"about"];
            [dropObj fadeOut];
            [self showPopUpWithTitle:@"About You Here" withOption:arrOptions xy:CGPointMake(((SCREEN_WIDTH/2) - (287/2)), 130) size:CGSizeMake(287, SCREEN_HEIGHT/1.8) isMultiple:NO];
        }
        else
        {
            [self getAboutUs];
        }
        return NO;
    }
    return YES;
}

#pragma mark - on btn click show drop down menu
-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple
{
    dropObj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple parseKey:nil];
    dropObj.delegate = self;
    [dropObj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [dropObj SetBackGroundDropDown1_R:150.0 G:122.0 B:85.0 alpha:0.70];
}

#pragma mark - kDropDownListViewDelegate

- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex
{
    /*----------------Get Selected Value[Single selection]-----------------*/
    
    About *about = self.abouts[anIndex];
    aboutId = about.aboutId;
    _txt_aboutUs.text = about.about;
}

- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData
{
    //----------------Get Selected Value[Multiple selection]-----------------
    if (ArryData.count>0)
    {
        strInterestedIds = @"";
        for (NSString *intrest in ArryData)
        {
            NSArray *filterInterest_Arr = [self.intrests filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.interest == %@",intrest]];
            Interest *interest = ([filterInterest_Arr count] > 0) ? [filterInterest_Arr objectAtIndex:0] : nil;
            if (interest != nil)
            {
                if (strInterestedIds.length == 0)
                {
                    strInterestedIds = [NSString stringWithFormat:@"%ld", (long)interest.interestedId];
                }
                else
                {
                    strInterestedIds = [NSString stringWithFormat:@"%@,%ld", strInterestedIds, (long)interest.interestedId];
                }
            }
        }
        
        _txt_interestedIn.text = [ArryData componentsJoinedByString:@","];
        //CGSize size=[self GetHeightDyanamic:_lblSelectedCountryNames];
        //_lblSelectedCountryNames.frame=CGRectMake(16, 240, 287, size.height);
    }
    else
    {
        _txt_interestedIn.text = @"";
    }
}

- (void)DropDownListViewDidCancel
{
    
}


#pragma mark -  API Call

-(void)getInterestMaster
{
    apiCallStr = @"getInterestMaster";
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@interestMaster",[ClsSetting defaultURL]]  view:self.view];
    objSetting.passResponseDataDelegate=self;
}

-(void)getAboutUs
{
    apiCallStr = @"getAboutUs";
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@aboutUs",[ClsSetting defaultURL]]  view:self.view];
    objSetting.passResponseDataDelegate=self;
}

-(void)getCountry
{
    
    apiCallStr = @"getCountry";
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@Country",[ClsSetting defaultURL]]  view:self.view];
    objSetting.passResponseDataDelegate=self;
}

-(void)getStateWithCountryId:(NSString*)countryid
{
    
    apiCallStr = @"getState";
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@State?countryid=%@",[ClsSetting defaultURL], selectedPostalCountry.countryid]  view:self.view];
    objSetting.passResponseDataDelegate = self;
}

-(void)getCityWithStateId:(NSString*)stateid
{
    //http://restapi.infomanav.com/api/v2/asta/_table/City?api_key=c255e4bd10c8468f9e7e393b748750ee108d6308e2ef3407ac5d2b163a01fa37&filter=stateid%20=%2022
    
    
    apiCallStr = @"getCity";
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@City?stateid=%@",[ClsSetting defaultURL],stateid]  view:self.view];
    objSetting.passResponseDataDelegate=self;
}

-(void)checkEmailUserNameExist
{
    apiCallStr = @"checkEmailUserNameExist";
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
//    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"users/?api_key=%@&fields=userid,username,email,name&filter=(username=%@)or(email=%@)",[ClsSetting apiKey],[ClsSetting TrimWhiteSpaceAndNewLine:_txtUserName.text],[ClsSetting TrimWhiteSpaceAndNewLine:_txtEmail.text]] view:self.view];
    
    
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@UserEmailMobileExist?username=%@&email=%@&Mobile=%@",[ClsSetting defaultURL],[ClsSetting TrimWhiteSpaceAndNewLine:_txtUserName.text],[ClsSetting TrimWhiteSpaceAndNewLine:_txtEmail.text],[ClsSetting TrimWhiteSpaceAndNewLine:_txtMobileNumber.text]] view:self.view];

//    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@FilterUser?userid=&username=%@&email=%@",[ClsSetting defaultURL],[ClsSetting TrimWhiteSpaceAndNewLine:_txtUserName.text],[ClsSetting TrimWhiteSpaceAndNewLine:_txtEmail.text]] view:self.view];
    objSetting.passResponseDataDelegate=self;
}

- (void)passGetResponseData:(id)responseObject
{
//    NSError *error;
//    NSMutableDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
    //NSMutableArray *arr1 = [dict1 valueForKey:@"resource"];
    
    NSMutableArray *arr1 = [responseObject valueForKey:@"resource"];

    if (arr1.count>0)
    {
        if ([apiCallStr isEqualToString:@"getInterestMaster"])
        {
            NSArray *intrestsResult = [Interest interestsFromJSON:arr1];
            self.intrests = intrestsResult;
            [self getAboutUs];
        }
        else if ([apiCallStr isEqualToString:@"getAboutUs"])
        {
            NSArray *aboutsResult = [About aboutsFromJSON:arr1];
            self.abouts = aboutsResult;
        }
        else if ([apiCallStr isEqualToString:@"checkEmailUserNameExist"])
        {
            [ClsSetting ValidationPromt:@"Email ID or Username Already Exists. Please Use Alternate Email ID or Username"];
        }
        else if ([apiCallStr isEqualToString:@"getCountry"])
        {
            NSArray *countrysResult = [Country coutrysFromJSON:arr1];
//            NSLog(@"%@",countrysResult);
            self.coutrys = countrysResult;
            
            [[NSUserDefaults standardUserDefaults] setObject:arr1 forKey:@"coutrys"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self presentViewController:self.searchController animated:true completion:nil];
            self.searchController.searchResultsController.view.hidden = NO;
        }
        else if ([apiCallStr isEqualToString:@"getState"])
        {
            self.states = [State statesFromJSON:arr1];;
            
            [self presentViewController:self.searchController animated:true completion:nil];
            self.searchController.searchResultsController.view.hidden = NO;
            
        }
        else if ([apiCallStr isEqualToString:@"getCity"])
        {
            self.citys = [City citysFromJSON:arr1];;
            
            [self presentViewController:self.searchController animated:true completion:nil];
            self.searchController.searchResultsController.view.hidden = NO;
        }
    }
    else
    {
        if ([apiCallStr isEqualToString:@"getInterestMaster"])
        {
            [ClsSetting ValidationPromt:@"Error While Featching Interst"];
            [self getAboutUs];
        }
        else if ([apiCallStr isEqualToString:@"getAboutUs"])
        {
            [ClsSetting ValidationPromt:@"Error While Featching About"];
        }
        if ([apiCallStr isEqualToString:@"checkEmailUserNameExist"])
        {
            //Email Username Not Exist
            [self registerUser];
        }
        else if ([apiCallStr isEqualToString:@"getCountry"])
        {
            [ClsSetting ValidationPromt:@"Error While Featching Country"];
        }
        else if ([apiCallStr isEqualToString:@"getState"])
        {
            [ClsSetting ValidationPromt:@"Error While Featching State"];
        }
        else if ([apiCallStr isEqualToString:@"getCity"])
        {
            [ClsSetting ValidationPromt:@"Error While Fetching City"];
        }
    }
}

-(void)registerUser
{
    NSString *strFirstName = _txtFirstName.text;
    NSString *strLastName = _txtLastName.text;
    NSString *strCity = _txtCity.text;
    NSString *strContry = _txtcountry.text;
    NSString *strState = _txtState.text;
    NSString *strZip = _txtZip.text;
    NSString *strMobile = [NSString stringWithFormat:@"%@%@",_txtCountryCode.text,_txtMobileNumber.text];
    NSString *strTelephone = _txtTelephoneNumber.text;
    NSString *strFax = _txtFaxNumber.text;
    NSString *strEmail = _txtEmail.text;
    NSString *strUserName = _txtUserName.text;
    NSString *strPassword = _txtPassword.text;
    NSString *strNickname = [NSString stringWithFormat:@"Anonymous%u",arc4random() % 900000 + 100000];
    NSString *strAddress = _txtAddress.text;
    
    strSMSCode = [NSString stringWithFormat:@"%d",arc4random() % 9000 + 1000];
    strEmailCode = [NSString stringWithFormat:@"%d",arc4random() % 9000 + 1000];
    
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *strRegistrationDate = [DateFormatter stringFromDate:[NSDate date]];
    
    
    NSString *strDeviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    if (strDeviceToken == nil)
    {
        strDeviceToken = @"";
    }
    
    //New
    NSString *strAddress2 = _txtAddress.text;
    NSString *strGenderId = (_btn_male.selected == YES) ? @"1" : (_btn_female.selected == YES) ? @"2" : (_btn_other.selected == YES) ? @"3" : @"0";
    NSString *strBiddingId = (_btn_yes.selected == YES) ? @"1" : (_btn_no.selected == YES) ? @"2" : @"0";
    NSString *strBDay = _txt_birthDay.text;
    NSString *strBMonth = _txt_birthMonth.text;
    NSString *strBYear = _txt_birthYear.text;
    NSString *strAboutId = [NSString stringWithFormat:@"%@", aboutId];

    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"name"] = strFirstName;
    params[@"lastname"] = strLastName;
    params[@"city"] = strCity;
    params[@"country"] = strContry;
    params[@"state"] = strState;
    params[@"zip"] = strZip;
    params[@"Mobile"] = strMobile;
    params[@"telephone"] = strTelephone;
    params[@"fax"] = strFax;
    params[@"email"] = strEmail;
    params[@"username"] = strUserName;
    params[@"password"] = strPassword;
    params[@"nickname"] = strNickname;
    params[@"address1"] = strAddress;
    params[@"t_username"] = strUserName;
    params[@"t_password"] = strPassword;
    params[@"t_firstname"] = strFirstName;
    params[@"t_lastname"] = strLastName;

    //Primary Postal Addrss
    params[@"t_City"] = strCity;
    params[@"t_State"] = strState;
    params[@"t_Country"] = strContry;
    params[@"t_telephone"] = strTelephone;
    params[@"t_fax"] = strFax;
    params[@"t_email"] = strEmail;
    params[@"t_mobile"] = strMobile;
    params[@"t_address1"] = strAddress;
    params[@"t_nickname"] = strNickname;

    //Billing Address
    params[@"t_billingaddress"] = strAddress;
    params[@"t_billingname"] = strFirstName;
    params[@"t_billingcity"] = strCity;
    params[@"t_billingstate"] = strState;
    params[@"t_billingcountry"] = strContry;
    params[@"t_billingzip"] = strZip;
    params[@"t_billingtelephone"] = strTelephone;
    params[@"t_billingemail"] = strEmail;

    params[@"BillingName"] = [NSString stringWithFormat:@"%@ %@",strFirstName,strLastName];
    params[@"BillingCity"] = strCity;
    params[@"BillingState"] = strState;
    params[@"BillingCountry"] = strContry;
    params[@"BillingZip"] = strZip;
    params[@"BillingAddress"] = strAddress;
    params[@"BillingTelephone"] = strTelephone;
    params[@"BillingEmail"] = strEmail;

    params[@"SmsCode"] = @"0";
    params[@"admin"] = @"0";
    params[@"MobileVerified"] = @"0";
    params[@"EmailVerified"] = @"0";
    params[@"chatdept"] = @"Test";
    params[@"confirmbid"] = @"0";
    params[@"Visits"] = @"0";
    params[@"buy"] = @"0";
    params[@"applyforbid"] = @"1";
    params[@"applyforchange"] = @"0";

    params[@"RegistrationDate"] = strRegistrationDate;
    params[@"deviceTocken"] = strDeviceToken;
    params[@"androidDeviceTocken"] = strDeviceToken; // FCM ID

    // new fields
    params[@"address2"] = strAddress2;
    params[@"genderid"] = strGenderId;
    params[@"bday"] = strBDay;
    params[@"bmonth"] = strBMonth;
    params[@"byear"] = strBYear;
    params[@"aboutId"] = strAboutId;
    params[@"applyforBid"] = strBiddingId;
    params[@"interestedIds"] = strInterestedIds;
    params[@"countryid"] = selectedPostalCountry.countryid;
    params[@"stateid"] = selectedPostalState.stateid;
    params[@"cityid"] = selectedPostalCity.cityid;

    params[@"bCountryid"] = selectedPostalCountry.countryid;
    params[@"bStateid"] = selectedPostalState.stateid;
    params[@"bCityid"] = selectedPostalCity.cityid;
    
    paradict = params;
    
    NSMutableArray *arr = [NSMutableArray arrayWithObjects:params,nil];
    NSDictionary *pardsams = @{@"resource":arr};
    
  NSString *URLString = [NSString stringWithFormat:@"%@UserRegistration",[ClsSetting defaultURL]];

//    NSString *URLString = [NSString stringWithFormat:@"%@users?api_key=%@",[ClsSetting tableURL],[ClsSetting apiKey]];

    ClsSetting *objClssetting=[[ClsSetting alloc] init];
    objClssetting.passResponseDataDelegate=self;
    [objClssetting callPostWeb:pardsams url:URLString view:self.view];
}

-(void)passPostResponseData:(id)responseObject
{
//    NSArray *value = responseObject[@"resource"];
//    NSLog(@"%@",value);
//    NSMutableDictionary *dictUser=[value objectAtIndex:0];
//    NSDictionary *dict=[ClsSetting RemoveNullOnly:dictUser];
//
//    [[NSUserDefaults standardUserDefaults] setValue:[ClsSetting TrimWhiteSpaceAndNewLine:_txtUserName.text] forKey:USER_NAME];
//    [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"userid"] forKey:@"ruserid"];
//
//    NSString *str_userid = [dict valueForKey:@"userid"];
//    [self saveDevice:str_userid];
       NSArray *value = responseObject[@"resource"];
       NSLog(@"%@",value);
       NSMutableDictionary *dictUser=responseObject;
       NSDictionary *dict=[ClsSetting RemoveNullOnly:dictUser];
       NSLog(@"%@",dictUser);
       NSLog(@"%@",dict);
       [[NSUserDefaults standardUserDefaults] setValue:[ClsSetting TrimWhiteSpaceAndNewLine:_txtUserName.text] forKey:USER_NAME];
       [[NSUserDefaults standardUserDefaults]setValue:[dict valueForKey:@"userid"] forKey:@"ruserid"];
       NSLog(@"%@",[dict valueForKey:@"userid"]);
       NSString *str_userid = [dict valueForKey:@"userid"];
       [self saveDevice:str_userid];
    
    
    [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"confirmbid"];
    [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"EmailVerified"];
    [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"MobileVerified"];
    
    
    VerificationViewController *rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VerificationViewController"];
    rootViewController.strEmail=[ClsSetting TrimWhiteSpaceAndNewLine:_txtEmail.text];
    rootViewController.strMobile=[ClsSetting TrimWhiteSpaceAndNewLine:_txtMobileNumber.text];
    rootViewController.strname=_txtFirstName.text;
    rootViewController.strSMSCode=strSMSCode;
    rootViewController.strEmialCode=strEmailCode;
    rootViewController.strlastname=_txtLastName.text;
    rootViewController.isRegistration = YES;
    rootViewController.dict = [paradict mutableCopy];
    [self.navigationController pushViewController:rootViewController animated:YES];
}

-(void)saveDevice: (NSString*)userID {
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    if (deviceToken == nil)
    {
        deviceToken = @"";
    }
    NSString *strQuery=[NSString stringWithFormat:@"%@", [ClsSetting saveDeviceURL]];
    NSString *url = strQuery;
    NSLog(@"%@",url);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    manager.requestSerializer = serializer;
    NSDictionary *dict = @{
                                  @"userid": userID,
                                  @"device_id": deviceToken,
                                  @"device_type": @"iOS",
                                  };
    NSLog(@"Dict %@",dict);
    [manager POST:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"%@",responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
        
}

@end
