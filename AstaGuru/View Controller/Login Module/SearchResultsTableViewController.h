//
//  SearchResultsTableViewController.h
//  Sample-UISearchController
//
//  Created by James Dempsey on 9/20/14.
//  Copyright (c) 2014 Tapas Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Country.h"

NS_ASSUME_NONNULL_BEGIN


typedef enum {
    PostalCountry,
    BillingCountry,
    PostalState,
    BillingState,
    PostalCity,
    BillingCity,
} SearchResultType;


@protocol SearchResultsDelegate<NSObject>
@required
-(void)didSelectSearchResult:(NSString*)item;

@end


@interface SearchResultsTableViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *searchResults; // Filtered search results

@property(nullable, nonatomic, weak)   id<SearchResultsDelegate> searchResultsDelegate;             // default is nil. weak reference

@end

NS_ASSUME_NONNULL_END
