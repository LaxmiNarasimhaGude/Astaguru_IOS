//
//  MyProfileViewController.m
//  AstaGuru
//
//  Created by sumit mashalkar on 15/09/16.
//  Copyright © 2016 Aarya Tech. All rights reserved.
//

#import "MyProfileViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "ClsSetting.h"
#import "Country.h"
#import "SearchResultsTableViewController.h"
#import "DropDownListView.h"
#import "JTSImageViewController.h"
#import "JTSImageInfo.h"
#import "WhyNeedBankDetViewController.h"
#import "JVFloatLabeledTextField.h"



@interface MyProfileViewController ()<PassResponse, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, SearchResultsDelegate, UISearchResultsUpdating, kDropDownListViewDelegate>
{
    int isDifferentBilingAddress;
    
    int isPanCardSelect;
    int isAadharCardSelect;
    
    NSString *genderId;
    NSString *aboutId;
    NSString *interestedIds;
    
    Country *selectedPostalCountry;
    State *selectedPostalState;
    City *selectedPostalCity;

    Country *selectedBillingCountry;
    State *selectedBillingState;
    City *selectedBillingCity;

    UIImagePickerController *ipc;
    
    ImagePikerType imagePikerType;
    
    MBProgressHUD *HUD;
    
    int billingAddress_height;
    
    NSString *apiCallStr;
    
    SearchResultType searchResultType;
    
    DropDownListView * dropObj;

}

@property (nonatomic, retain) NSArray *intrests;
@property (nonatomic, retain) NSArray *abouts;
@property (nonatomic, retain) NSArray *coutrys;
@property (nonatomic, retain) NSArray *states;
@property (nonatomic, retain) NSArray *citys;

@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResults; // Filtered search results

@end

@implementation MyProfileViewController

- (void)viewDidLoad
{
    // Do any additional setup after loading the view.
    [super viewDidLoad];
    
   
    
    
    [self setUpNavigationItem];
//    [self setBroder];
    
    [self getInterestMaster];
    
    isDifferentBilingAddress = 0;
    isPanCardSelect = 0;
    isAadharCardSelect = 0;
    
    UITapGestureRecognizer *panCardPhoto_TapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(view_panCardPhoto_tap:)];
    [_view_panCardPhoto addGestureRecognizer:panCardPhoto_TapRecognizer];
    _view_panCardPhoto.tag = 11;

    UITapGestureRecognizer *aadharCardPhoto_TapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(view_aadharPhoto_tap:)];
    [_view_aadharPhoto addGestureRecognizer:aadharCardPhoto_TapRecognizer];
    _view_aadharPhoto.tag = 22;

    imagePikerType = None;
    
    interestedIds = @"";
    
    isDifferentBilingAddress=0;
    _imgView_checkBillingAddress.image=[UIImage imageNamed:@"checkbox_empty"];
    billingAddress_height = _view_billingAddress_height.constant;
    _view_billingAddress_height.constant = 0;
    _view_billingAddress.hidden = YES;
    
    //_txt_aboutUs.enabled = YES;
    _txt_aboutUs.delegate = self;
    _txt_interestedIn.delegate = self;
    _txt_postalCountry.delegate = self;
    _txt_billingCountry.delegate = self;
    _txt_postalState.delegate = self;
    _txt_billingState.delegate = self;
    _txt_postalCity.delegate = self;
    _txt_billingCity.delegate = self;
    _txt_birthDay.delegate = self;
    _txt_birthMonth.delegate = self;
    _txt_birthYear.delegate = self;
    _txt_postalZip.delegate = self;
    _txt_billingZip.delegate = self;
    _txt_aadharCard.delegate = self;
    _txt_panCard.delegate = self;
    _txt_accountNo.delegate = self;
    _txt_ifscCode.delegate = self;

    NSArray *arr1 = (NSArray*)[[NSUserDefaults standardUserDefaults] valueForKey:@"coutrys"];
    NSArray *countrysResult = [Country coutrysFromJSON:arr1];
    self.coutrys = countrysResult;

    //Configure SearchController
    // Create a mutable array to contain products for the search results table.
    self.searchResults = [[NSMutableArray alloc] init];
    
    // The table view controller is in a nav controller, and so the containing nav controller is the 'search results controller'
    UINavigationController *searchResultsController = [[self storyboard] instantiateViewControllerWithIdentifier:@"TableSearchResultsNavController"];
    SearchResultsTableViewController *vc = (SearchResultsTableViewController *)searchResultsController.topViewController;
    vc.searchResultsDelegate = self;
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultsController];
    self.searchController.searchResultsController.automaticallyAdjustsScrollViewInsets = NO;
    self.searchController.dimsBackgroundDuringPresentation = NO; // default is YES

    self.searchController.searchResultsUpdater = self;
    
    //self.searchController.searchBar.delegate = self;
    
    self.searchController.searchBar.barTintColor = [UIColor blackColor];
    self.searchController.searchBar.backgroundColor = [UIColor blackColor];
    self.searchController.searchBar.translucent = NO;
    self.searchController.searchBar.tintColor = [UIColor colorWithRed:130.0f/255.0f green:103.0f/255.0f blue:67.0f/255.0f alpha:1];
    
    self.definesPresentationContext = YES;

}

-(void) openPopupForNeedBankDetail{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle:nil];
    WhyNeedBankDetViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"WhyNeedBankDetViewController"];
    vc.isFromBiling = YES;
    [self presentViewController:vc
                       animated:YES
                     completion:nil];
}

- (IBAction)btn_need_bankDetail_pressed:(UIButton *)sender
{
    [self openPopupForNeedBankDetail];
}
- (IBAction)btn_bidLimit_pressed:(id)sender {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                        bundle:nil];
    WhyNeedBankDetViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"WhyNeedBankDetViewController"];
    vc.isFromBiling = NO;
    [self presentViewController:vc
              animated:YES
            completion:nil];
}

-(void)setUpNavigationItem
{
    UIButton *btnBack = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    [btnBack setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    // [btnBack addTarget:self action:@selector(backPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    self.title=@"My Profile";
    self.sidebarButton=[[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(closePressed)];
    [[self navigationItem] setLeftBarButtonItem:self.sidebarButton];
    self.sidebarButton.tintColor=[UIColor colorWithRed:167/255.0 green:142/255.0 blue:105/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.sideleftbarButton=[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(btn_done_pressed)];
    self.sideleftbarButton.tintColor=[UIColor colorWithRed:167/255.0 green:142/255.0 blue:105/255.0 alpha:1.0];
    [[self navigationItem] setRightBarButtonItem:self.sideleftbarButton];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"WorkSans-Medium" size:17]}];
}
-(void) setBroder
{
    UIColor *bColor = [UIColor colorWithRed:219.0f/255.0f green:219.0f/255.0f blue:219.0f/255.0f alpha:1];
    
    //Presonal details
    [ClsSetting SetBorder:_view_firstName cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_lastName cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_mobileNumber cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_email cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_bidLimit cornerRadius:2 borderWidth:1 color:bColor];
    
    //Profile
    [ClsSetting SetBorder:_view_birthDay cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_birthMonth cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_birthYear cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_birthYear cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_aboutUs cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_insterstedIn cornerRadius:2 borderWidth:1 color:bColor];

    //Primary postal address
    [ClsSetting SetBorder:_view_postalAddressLine1 cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_postalAddressLine2 cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_postalCountry cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_postalState cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_postalCity cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_postalZip cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_postalGstin cornerRadius:2 borderWidth:1 color:bColor];
    
    //Billing address
    [ClsSetting SetBorder:_view_billingName cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_billingAddressLine1 cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_billingAddressLine2 cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_billingCountry cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_billingState cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_billingCity cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_billingZip cornerRadius:2 borderWidth:1 color:bColor];
    
    //Account details
    [ClsSetting SetBorder:_view_userName cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_password cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_nickName cornerRadius:2 borderWidth:1 color:bColor];
    
    //Banking details
    [ClsSetting SetBorder:_view_panCard cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_panCardPhoto cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_aadharCard cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_aadharPhoto cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_accountNo cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_accountNo cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_aacountHolderName cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_ifscCode cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_branchName cornerRadius:2 borderWidth:1 color:bColor];
    [ClsSetting SetBorder:_view_swiftCode cornerRadius:2 borderWidth:1 color:bColor];

}

- (void)btn_done_pressed
{
    if ([self validate])
    {
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"Uploding";
        HUD.detailsLabelText = @"Please wait!";
        HUD.mode = MBProgressHUDModeDeterminateHorizontalBar;
        HUD.progress = 0.0f;
        HUD.square = YES;
        [self updateProfile];
    }
}

-(void)closePressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)validate
{
    if((_btn_male.selected == NO) && (_btn_female.selected == NO) && (_btn_other.selected == NO))
    {
        [ClsSetting ValidationPromt:@"Select Gender"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_birthDay.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Birth Day"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_birthMonth.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Birth Month"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_birthYear.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Birth Year"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_postalAddressLine1.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter primary Postal Address Line1"];
        return NO;
    }
    //    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_postalAddressLine2.text].length==0)
    //    {
    //        [ClsSetting ValidationPromt:@"Enter Primary Postal Address Line2"];
    //        return NO;
    //    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_postalCountry.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Select Primary Postal Country"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_postalState.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Select Primary Postal State"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_postalCity.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Select Primary Postal City"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_postalZip.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Select Primary Postal ZIP/PIN"];
        return NO;
    }
    if (isDifferentBilingAddress)
    {
        if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_postalAddressLine1.text].length==0)
        {
            [ClsSetting ValidationPromt:@"Enter Billing Address Line1"];
            return NO;
        }
        //    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_postalAddressLine2.text].length==0)
        //    {
        //        [ClsSetting ValidationPromt:@"Enter Billin Address Line2"];
        //        return NO;
        //    }
        else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_postalCountry.text].length==0)
        {
            [ClsSetting ValidationPromt:@"Select Billing Country"];
            return NO;
        }
        else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_postalState.text].length==0)
        {
            [ClsSetting ValidationPromt:@"Select Billing State"];
            return NO;
        }
        else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_postalCity.text].length==0)
        {
            [ClsSetting ValidationPromt:@"Select Billing City"];
            return NO;
        }
        else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_postalZip.text].length==0)
        {
            [ClsSetting ValidationPromt:@"Select Billing ZIP/PIN"];
            return NO;
        }
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_password.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Password"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_nickName.text].length==0)
    {
        [ClsSetting ValidationPromt:@"Enter Nickname"];
        return NO;
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_panCard.text].length != 0)
    {
        if (![ClsSetting validatePanCardNumber:_txt_panCard.text])
        {
            [ClsSetting ValidationPromt:@"Enter Valid PAN Number"];
            return NO;
        }
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_panCard.text].length != 0)
    {
        UIImage *uploadImg = [UIImage imageNamed:@"upload.png"];
        if ([UIImagePNGRepresentation(_imgView_panCard.image) isEqualToData:UIImagePNGRepresentation(uploadImg)])
        {
            [ClsSetting ValidationPromt:@"Select PAN Card Photo"];
            return NO;
        }
    }
    else if ([ClsSetting TrimWhiteSpaceAndNewLine:_txt_aadharCard.text].length != 0)
    {
        UIImage *uploadImg = [UIImage imageNamed:@"upload.png"];
        if ([UIImagePNGRepresentation(_imgView_panCard.image) isEqualToData:UIImagePNGRepresentation(uploadImg)])
        {
            [ClsSetting ValidationPromt:@"Select Aadhar Card Photo"];
            return NO;
        }
    }
    
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btn_male_pressed:(UIButton *)sender
{
    [self.btn_male setSelected:TRUE];
    [self.btn_female setSelected:FALSE];
    [self.btn_other setSelected:FALSE];
}

- (IBAction)btn_female_pressed:(UIButton *)sender
{
    [self.btn_male setSelected:FALSE];
    [self.btn_female setSelected:TRUE];
    [self.btn_other setSelected:FALSE];
}

- (IBAction)btn_other_pressed:(UIButton *)sender
{
    [self.btn_male setSelected:FALSE];
    [self.btn_female setSelected:FALSE];
    [self.btn_other setSelected:TRUE];
}

- (IBAction)btn_differentBillingAddres_pressed:(UIButton *)sender
{
    if (isDifferentBilingAddress == 0)
    {
        isDifferentBilingAddress=1;
        _imgView_checkBillingAddress.image=[UIImage imageNamed:@"checkbox_filled"];
        _view_billingAddress_height.constant = billingAddress_height;
        _view_billingAddress.hidden = NO;
    }
    else
    {
        isDifferentBilingAddress=0;
        _imgView_checkBillingAddress.image=[UIImage imageNamed:@"checkbox_empty"];
        billingAddress_height = _view_billingAddress_height.constant;
        _view_billingAddress_height.constant = 0;
        _view_billingAddress.hidden = YES;
    }
}


#pragma mark - SearchResultsDelegate
-(void)didSelectSearchResult:(NSString *)item
{
    self.searchController.searchBar.text = @"";
    if (searchResultType == PostalCountry)
    {
        selectedPostalCountry = [[self.coutrys filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.countryname == %@",item]] objectAtIndex:0];
        _txt_postalCountry.text = selectedPostalCountry.countryname;
        
        _txt_postalState.text = @"";
        selectedPostalState = nil;
        
        _txt_postalCity.text = @"";
        selectedPostalCity = nil;
        
        if ([selectedPostalCountry.countryname.lowercaseString isEqualToString:@"india"])
        {
            _txt_aadharCard.placeholder = @"Government Identity Number";
        }
    }
    else if (searchResultType == BillingCountry)
    {
        selectedBillingCountry = [[self.coutrys filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.countryname == %@",item]] objectAtIndex:0];
        _txt_billingCountry.text = selectedBillingCountry.countryname;
        
        _txt_billingState.text = @"";
        selectedBillingState = nil;
        
        _txt_billingCity.text = @"";
        selectedBillingCity = nil;

        
    }
    if (searchResultType == PostalState)
    {
        selectedPostalState =  [[self.states filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.statename == %@",item]] objectAtIndex:0];
        _txt_postalState.text = selectedPostalState.statename;
        
        _txt_postalCity.text = @"";
        selectedPostalCity = nil;

        
    }
    else if (searchResultType == BillingState)
    {
        selectedBillingState = [[self.states filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.statename == %@",item]] objectAtIndex:0];
        _txt_billingState.text = selectedBillingState.statename;
        
        _txt_billingCity.text = @"";
        selectedBillingCity = nil;
    }
    if (searchResultType == PostalCity)
    {
        selectedPostalCity = [[self.citys filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.cityname == %@",item]] objectAtIndex:0];
        _txt_postalCity.text = selectedPostalCity.cityname;
    }
    else if (searchResultType == BillingCity)
    {
        selectedBillingCity = [[self.citys filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.cityname == %@",item]] objectAtIndex:0];
        _txt_billingCity.text = selectedBillingCity.cityname;
    }
    [self dismissViewControllerAnimated:true completion:nil];
}


#pragma mark - UISearchResultsUpdating

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    self.searchController.searchResultsController.view.hidden = NO;
    
    NSString *searchString = [self.searchController.searchBar text];
    
    [self updateFilteredContentForProductName:searchString];
    
    if (self.searchController.searchResultsController)
    {
        NSLog(@"searchResults == %lu", (unsigned long)self.searchResults.count);
        UINavigationController *navController = (UINavigationController *)self.searchController.searchResultsController;
        SearchResultsTableViewController *vc = (SearchResultsTableViewController *)navController.topViewController;
        vc.searchResults = self.searchResults;
        [vc.tableView reloadData];
    }
    
}


#pragma mark - Content Filtering

- (void)updateFilteredContentForProductName:(NSString *)searchString
{
    NSArray *searchingFrom;
    if ((searchResultType == PostalCountry) || (searchResultType == BillingCountry))
    {
        searchingFrom =  [self.coutrys valueForKey:@"countryname"];  //self.coutrys;
    }
    else if ((searchResultType == PostalState) || (searchResultType == BillingState))
    {
        searchingFrom = [self.states valueForKey:@"statename"]; //self.states;
    }
    else if ((searchResultType == PostalCity) || (searchResultType == BillingCity))
    {
        searchingFrom = [self.citys valueForKey:@"cityname"]; //self.city;
    }
    
    // Update the filtered array based on the search text and scope.
    if ((searchString == nil) || [searchString length] == 0)
    {
        self.searchResults = [searchingFrom mutableCopy];
        return;
    }
    
    [self.searchResults removeAllObjects]; // First clear the filtered array.
    
    /*  Search the main list for products whose type matches the scope (if selected) and whose name matches searchText; add items that match to the filtered array.
     */
    for (NSString *countryname in searchingFrom)
    {
        NSUInteger searchOptions = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;
        NSRange productNameRange = NSMakeRange(0, countryname.length);
        NSRange foundRange = [countryname rangeOfString:searchString options:searchOptions range:productNameRange];
        if (foundRange.length > 0) {
            [self.searchResults addObject:countryname];
        }
    }
}

- (void)showDatePicker:(UITextField*)textFiled
{
    textFiled.tintColor = [UIColor whiteColor];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:-18];
    NSDate *maxDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    [comps setYear:-150];
    NSDate *minDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    
    [datePicker setMinimumDate:minDate];
    [datePicker setMaximumDate:maxDate];
    [datePicker setDate:maxDate];
    
    
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    [textFiled setInputView:datePicker];
}

-(void)updateTextField:(UIDatePicker*)sender
{
    NSLog(@"Date == %@", sender.date);
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:sender.date];
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    _txt_birthDay.text = [NSString stringWithFormat: @"%ld", (long)day];
    _txt_birthMonth.text = [NSString stringWithFormat: @"%ld", (long)month];
    _txt_birthYear.text = [NSString stringWithFormat: @"%ld", (long)year];
}


#pragma mark - UITextFieldDelegate Delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;

    if (textField == _txt_birthDay || textField == _txt_birthMonth)
    {
        return newLength <= 2;
    }
    else if (textField == _txt_birthYear)
    {
        return newLength <= 4;
    }
    else if (textField == _txt_postalZip ||textField == _txt_billingZip)
    {
        return newLength <= 10;
    }
    else if (textField == _txt_accountNo)
    {
        return newLength <= 25;
    }
    else if (textField == _txt_ifscCode)
    {
        return newLength <= 11;
    }
    else if (textField == _txt_panCard)
    {
        if (newLength <= 10)
        {
            NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
            
            if (lowercaseCharRange.location != NSNotFound)
            {
                textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                         withString:[string uppercaseString]];
                return NO;
            }
        }
        else
        {
            return NO;
        }
    }
    else if (textField == _txt_aadharCard)
    {
        return newLength <= 12;
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [dropObj fadeOut];
    if ((textField == _txt_birthDay) || (textField == _txt_birthMonth) || (textField == _txt_birthYear))
    {
        [self showDatePicker:textField];
    }
    else if (textField == _txt_postalCountry)
    {
        [self.view endEditing:YES];

        searchResultType = PostalCountry;

        if (self.coutrys.count == 0)
        {
            [self getCountry];
        }
        else
        {
            [self presentViewController:self.searchController animated:true completion:nil];
            self.searchController.searchResultsController.view.hidden = NO;
        }
        return NO;
    }
    else if (textField == _txt_billingCountry)
    {
        [self.view endEditing:YES];

        searchResultType = BillingCountry;

        if (self.coutrys.count == 0)
        {
            [self getCountry];
        }
        else
        {
            [self presentViewController:self.searchController animated:true completion:nil];
            self.searchController.searchResultsController.view.hidden = NO;
        }
        return NO;
    }
    else if (textField == _txt_postalState)
    {
        [self.view endEditing:YES];

        searchResultType = PostalState;
        NSLog(@"%@",_states);
        if (self.states.count == 0)
        {
            if (selectedPostalCountry == nil)
            {
                [ClsSetting ValidationPromt:@"Select Postal Address Country"];
            }
            else
            {
                
                [self getStateWithCountryId:selectedPostalCountry.countryid];
            }
        }
        else
        {
//            [self presentViewController:self.searchController animated:true completion:nil];
//            self.searchController.searchResultsController.view.hidden = NO;
             [self getStateWithCountryId:selectedPostalCountry.countryid];
        }
        return NO;
    }
    else if (textField == _txt_billingState)
    {
        [self.view endEditing:YES];

        searchResultType = BillingState;

        if (self.states.count == 0)
        {
            if (selectedBillingCountry == nil)
            {
                [ClsSetting ValidationPromt:@"Select Billing Address Country"];
            }
            else
            {
                [self getStateWithCountryId:selectedBillingCountry.countryid];
            }
        }
        else
        {
//            [self presentViewController:self.searchController animated:true completion:nil];
//            self.searchController.searchResultsController.view.hidden = NO;
            [self getStateWithCountryId:selectedBillingCountry.countryid];
            
        }
        return NO;
    }
    else if (textField == _txt_postalCity)
    {
        [self.view endEditing:YES];

        searchResultType = PostalCity;

        if (self.citys.count == 0)
        {
            if (selectedPostalState == nil)
            {
                [ClsSetting ValidationPromt:@"Select Postal Address State"];
            }
            else
            {
                [self getCityWithStateId:selectedPostalState.stateid];
            }
        }
        else
        {
            [self presentViewController:self.searchController animated:true completion:nil];
            self.searchController.searchResultsController.view.hidden = NO;
            
        }
        return NO;
    }
    else if (textField == _txt_billingCity)
    {
        [self.view endEditing:YES];

        searchResultType = BillingCity;

        if (self.states.count == 0)
        {
            if (selectedBillingState == nil)
            {
                [ClsSetting ValidationPromt:@"Select Billing Address State"];
            }
            else
            {
                [self getCityWithStateId:selectedBillingState.stateid];
            }
        }
        else
        {
//            [self presentViewController:self.searchController animated:true completion:nil];
//            self.searchController.searchResultsController.view.hidden = NO;
             [self getCityWithStateId:selectedBillingState.stateid];
            
        }
        return NO;
    }
    else if (textField == _txt_interestedIn)
    {
        [self.view endEditing:YES];

        if ([self.intrests count] > 0)
        {
            NSArray *arrOptions = [self.intrests valueForKey:@"interest"];
            [dropObj fadeOut];
            [self showPopUpWithTitle:@"Select Interest" withOption:arrOptions xy:CGPointMake(((SCREEN_WIDTH/2) - (287/2)), 130) size:CGSizeMake(287, SCREEN_HEIGHT/1.8) isMultiple:YES];
        }
        return NO;
    }
    else if (textField == _txt_aboutUs)
    {
        [self.view endEditing:YES];

        NSArray *arrOptions = [self.abouts valueForKey:@"about"];
        [dropObj fadeOut];
        [self showPopUpWithTitle:@"About You Here" withOption:arrOptions xy:CGPointMake(((SCREEN_WIDTH/2) - (287/2)), 130) size:CGSizeMake(287, SCREEN_HEIGHT/1.8) isMultiple:NO];
        return NO;
    }

    return YES;

}

#pragma mark - on btn click show drop down menu
-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple
{
    dropObj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple parseKey:nil];
    dropObj.delegate = self;
    [dropObj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [dropObj SetBackGroundDropDown1_R:150.0 G:122.0 B:85.0 alpha:0.70];
}

#pragma mark - kDropDownListViewDelegate

- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex
{
    /*----------------Get Selected Value[Single selection]-----------------*/
    
    About *about = self.abouts[anIndex];
    aboutId = about.aboutId;
    _txt_aboutUs.text = about.about;
}

- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData
{
     //----------------Get Selected Value[Multiple selection]-----------------
     if (ArryData.count>0)
     {
         interestedIds = @"";
         for (NSString *intrest in ArryData)
         {
             NSArray *filterInterest_Arr = [self.intrests filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.interest == %@",intrest]];
             Interest *interest = ([filterInterest_Arr count] > 0) ? [filterInterest_Arr objectAtIndex:0] : nil;
             if (interest != nil)
             {
                 if (interestedIds.length == 0)
                 {
                     interestedIds = [NSString stringWithFormat:@"%ld", (long)interest.interestedId];
                 }
                 else
                 {
                     interestedIds = [NSString stringWithFormat:@"%@,%ld", interestedIds, (long)interest.interestedId];
                 }
             }
         }
         
         _txt_interestedIn.text = [ArryData componentsJoinedByString:@","];
         //CGSize size=[self GetHeightDyanamic:_lblSelectedCountryNames];
         //_lblSelectedCountryNames.frame=CGRectMake(16, 240, 287, size.height);
     }
     else
     {
         _txt_interestedIn.text = @"";
     }
}

- (void)DropDownListViewDidCancel
{
    
}

#pragma mark - UITapGestureRecognizer

-(void)view_panCardPhoto_tap:(UITapGestureRecognizer*)sender
{
    if(sender.view.tag == 11)
    {
        imagePikerType = PanCard;
        [self showImagePickerForImageView:_imgView_panCard title:@"Pan Card Photo"];
    }
}

-(void)view_aadharPhoto_tap:(UITapGestureRecognizer*)sender
{
    if(sender.view.tag == 22)
    {
        imagePikerType = AadharCard;
        [self showImagePickerForImageView:_imgView_aadhar title:@"Aadhar Card Photo"];
    }
}


#pragma mark - ImagePickerController

-(void)showImagePickerForImageView:(UIImageView*)imageView title:(NSString*)title
{
    [self.view endEditing:YES];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:title message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIImage *uploadImg = [UIImage imageNamed:@"upload.png"];
    if (![UIImagePNGRepresentation(imageView.image) isEqualToData:UIImagePNGRepresentation(uploadImg)])
    {
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"View" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            // Distructive button tapped.
            [self dismissViewControllerAnimated:YES completion:^{
            }];
            
            JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
            if (imagePikerType == PanCard)
            {
                imageInfo.image = self.imgView_panCard.image;
            }
            else if (imagePikerType == AadharCard)
            {
                imageInfo.image = self.imgView_aadhar.image;
            }
            
            // Setup view controller
            JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                                   initWithImageInfo:imageInfo
                                                   mode:JTSImageViewControllerMode_Image
                                                   backgroundStyle:JTSImageViewControllerBackgroundOption_Scaled];
            
            // Present the view controller.
            [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];

        }]];
    }

    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
        
        ipc = [[UIImagePickerController alloc] init];
        ipc.delegate = self;
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
            AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if(authStatus == AVAuthorizationStatusAuthorized)
            {
                [self presentViewController:ipc animated:YES completion:NULL];
            }
            else if(authStatus == AVAuthorizationStatusNotDetermined)
            {
                NSLog(@"%@", @"Camera access not determined. Ask for permission.");
                
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
                 {
                     if(granted)
                     {
                         NSLog(@"Granted access to %@", AVMediaTypeVideo);
                         [self presentViewController:ipc animated:YES completion:NULL];
                     }
                     else
                     {
                         NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                         [self camDenied];
                     }
                 }];
            }
            else if (authStatus == AVAuthorizationStatusRestricted)
            {
                // My own Helper class is used here to pop a dialog in one simple line.
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                alert = nil;
            }
            else
            {
                [self camDenied];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No Camera Available." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            alert = nil;
        }
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
    {
        [self dismissViewControllerAnimated:YES completion:^{
        }];
        ipc= [[UIImagePickerController alloc] init];
        ipc.delegate = self;
        ipc.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [self presentViewController:ipc animated:YES completion:NULL];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");
    
    NSString *alertText;
    NSString *alertButton;
    
    NSURL *settings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    if ([[UIApplication sharedApplication] canOpenURL:settings])
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again.";
        alertButton = @"Go";

    }
    else
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Turn the Camera on.\n\n5. Open this app and try again.";
        alertButton = @"OK";

    }
    
    UIAlertController *actionAlert = [UIAlertController alertControllerWithTitle:@"Error" message:alertText preferredStyle:UIAlertControllerStyleAlert];
    [actionAlert addAction:[UIAlertAction actionWithTitle:alertButton style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                            {
                                [self dismissViewControllerAnimated:YES completion:^{
                                }];
                                if ([[UIApplication sharedApplication] canOpenURL:settings])
                                {
                                    [[UIApplication sharedApplication] openURL:settings];
                                }
                            }]];
    
    // Present action sheet.
    [self presentViewController:actionAlert animated:YES completion:nil];
}

#pragma mark - ImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    if (imagePikerType == PanCard)
    {
        isPanCardSelect = 1;
        _lbl_panCardImageName.text = [NSString stringWithFormat:@"%@_pan.jpeg",[[NSUserDefaults standardUserDefaults] valueForKey:USER_id]];
        _lbl_panCardImageName.textColor = [UIColor blackColor];
       _imgView_panCard.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    else if (imagePikerType == AadharCard)
    {
        isAadharCardSelect = 1;
        _imgView_aadhar.image = [info objectForKey:UIImagePickerControllerOriginalImage];
        _lbl_aadharCardImageName.text = [NSString stringWithFormat:@"%@_aadhar.jpeg",[[NSUserDefaults standardUserDefaults] valueForKey:USER_id]];
        _lbl_aadharCardImageName.textColor = [UIColor blackColor];

    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *uploadImg = [UIImage imageNamed:@"upload.png"];

    if (imagePikerType == PanCard)
    {
        if (![UIImagePNGRepresentation(_imgView_panCard.image) isEqualToData:UIImagePNGRepresentation(uploadImg)])
        {
            isPanCardSelect = 0;
        }
        else
        {
            isPanCardSelect = 1;
        }
    }
    else if (imagePikerType == AadharCard)
    {
        if (![UIImagePNGRepresentation(_imgView_panCard.image) isEqualToData:UIImagePNGRepresentation(uploadImg)])
        {
            isAadharCardSelect = 0;
        }
        else
        {
            isAadharCardSelect = 1;
        }
    }
}


#pragma mark -  API Call

-(void)getInterestMaster
{
    apiCallStr = @"getInterestMaster";
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@interestMaster",[ClsSetting defaultURL]]  view:self.view];
    objSetting.passResponseDataDelegate=self;
}

-(void)getAboutUs
{
    apiCallStr = @"getAboutUs";
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@aboutUs",[ClsSetting defaultURL]]  view:self.view];
    objSetting.passResponseDataDelegate=self;
}


-(void)getProfile
{
    apiCallStr = @"getProfile";

    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@FilterUser?userid=%@",[ClsSetting defaultURL],[[NSUserDefaults standardUserDefaults] valueForKey:USER_id]]  view:self.view];
    objSetting.passResponseDataDelegate=self;
    

}

-(void)getCountry
{
    
    apiCallStr = @"getCountry";
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@Country",[ClsSetting defaultURL]]  view:self.view];
    objSetting.passResponseDataDelegate=self;
}

-(void)getStateWithCountryId:(NSString*)countryid
{
    
    apiCallStr = @"getState";
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
    NSLog(@"%@",selectedPostalCountry.countryid);
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@State?countryid=%@",[ClsSetting defaultURL], countryid]  view:self.view];
    objSetting.passResponseDataDelegate = self;
}

-(void)getCityWithStateId:(NSString*)stateid
{
//http://restapi.infomanav.com/api/v2/asta/_table/City?api_key=c255e4bd10c8468f9e7e393b748750ee108d6308e2ef3407ac5d2b163a01fa37&filter=stateid%20=%2022

    
    apiCallStr = @"getCity";
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    ClsSetting *objSetting=[[ClsSetting alloc]init];
    [objSetting callGetWeb:dict url:[NSString stringWithFormat:@"%@City?stateid=%@",[ClsSetting defaultURL],stateid]  view:self.view];
    objSetting.passResponseDataDelegate=self;
}

//-(void)passReseposeData:(id)arr
- (void)passGetResponseData:(id)responseObject
{
    NSArray *arr1 = [responseObject valueForKey:@"resource"];
    if (arr1.count > 0)
    {
        if ([apiCallStr isEqualToString:@"getInterestMaster"])
        {
            NSArray *intrestsResult = [Interest interestsFromJSON:arr1];
            self.intrests = intrestsResult;
            [self getAboutUs];
        }
        else if ([apiCallStr isEqualToString:@"getAboutUs"])
        {
            NSArray *aboutsResult = [About aboutsFromJSON:arr1];
            self.abouts = aboutsResult;
            [self getProfile];
        }
        else if ([apiCallStr isEqualToString:@"getProfile"])
        {
            NSMutableDictionary *dict=[arr1 objectAtIndex:0];
            dict = [ClsSetting RemoveNullOnly:dict];

            //Presonal details
            NSString *name = [dict valueForKey:@"name"];
            _txt_firstName.text = [ClsSetting TrimWhiteSpaceAndNewLine:name];
            
            NSString *lastname = [dict valueForKey:@"lastname"];
            _txt_lastName.text = [ClsSetting TrimWhiteSpaceAndNewLine:lastname];
            
            NSString *Mobile =  [dict valueForKey:@"Mobile"];
            _txt_mobileNumber.text = [ClsSetting TrimWhiteSpaceAndNewLine:Mobile];
            
            NSString *email =  [dict valueForKey:@"email"];
            _txt_email.text = [ClsSetting TrimWhiteSpaceAndNewLine:email];
            
            id amountlimt = [dict valueForKey:@"amountlimt"];
            
            id isOldUser = [dict valueForKey:@"isOldUser"];
            
            [[NSUserDefaults standardUserDefaults] setValue:amountlimt forKey:@"amountlimt"];
            [[NSUserDefaults standardUserDefaults] setValue:isOldUser forKey:@"isOldUser"];
            
            NSString *unlimited = @"Unlimited";
            if ([isOldUser isEqual:[NSNumber numberWithLong:1]]){
                _txt_bidLimit.text = unlimited;
            }
            else{
                if ([amountlimt isKindOfClass:[NSString class]])
                          {
                              _txt_bidLimit.text = [ClsSetting TrimWhiteSpaceAndNewLine:amountlimt];
                          }
                          else if ([amountlimt isKindOfClass:[NSNumber class]])
                          {
                              _txt_bidLimit.text = [amountlimt stringValue];
                          }
            }
          
            
            //Profile
            id genderid = [dict valueForKey:@"genderid"];
            if ([genderid isKindOfClass:[NSString class]])
            {
                genderId = [ClsSetting TrimWhiteSpaceAndNewLine:genderid];
            }
            else if ([genderid isKindOfClass:[NSNumber class]])
            {
                genderId = [genderid stringValue];
            }

            if ([genderId isEqualToString:@"1"])
            {
                [self.btn_male setSelected:TRUE];
                [self.btn_female setSelected:FALSE];
                [self.btn_other setSelected:FALSE];
            }
            else if ([genderId isEqualToString:@"2"])
            {
                [self.btn_male setSelected:FALSE];
                [self.btn_female setSelected:TRUE];
                [self.btn_other setSelected:FALSE];
            }
            else if ([genderId isEqualToString:@"3"])
            {
                [self.btn_male setSelected:FALSE];
                [self.btn_female setSelected:FALSE];
                [self.btn_other setSelected:TRUE];
            }
            
            id bday = [dict valueForKey:@"bday"];
            if ([bday isKindOfClass:[NSString class]])
            {
                _txt_birthDay.text = [ClsSetting TrimWhiteSpaceAndNewLine:bday];
            }
            else if ([bday isKindOfClass:[NSNumber class]])
            {
                _txt_birthDay.text = [bday stringValue];
            }
            
            id bmonth = [dict valueForKey:@"bmonth"];
            if ([bmonth isKindOfClass:[NSString class]])
            {
                _txt_birthMonth.text = [ClsSetting TrimWhiteSpaceAndNewLine:bmonth];
            }
            else if ([bmonth isKindOfClass:[NSNumber class]])
            {
                _txt_birthMonth.text = [bmonth stringValue];
            }
            
            id byear = [dict valueForKey:@"byear"];
            if ([byear isKindOfClass:[NSString class]])
            {
                _txt_birthYear.text = [ClsSetting TrimWhiteSpaceAndNewLine:byear];
            }
            else if ([byear isKindOfClass:[NSNumber class]])
            {
                _txt_birthYear.text = [byear stringValue];
            }
            
            id _aboutId = [dict valueForKey:@"aboutId"];
            if ([_aboutId isKindOfClass:[NSString class]])
            {
                aboutId = [ClsSetting TrimWhiteSpaceAndNewLine:_aboutId];
            }
            else if ([_aboutId isKindOfClass:[NSNumber class]])
            {
                aboutId = [_aboutId stringValue];
            }
            NSArray *filterAboutArr = [self.abouts filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.aboutId == %ld",[aboutId integerValue]]];
            About *about = (filterAboutArr.count > 0) ? [filterAboutArr objectAtIndex:0] : nil;
            _txt_aboutUs.text = about.about;
            
            id _interestedIds = [dict valueForKey:@"interestedIds"];
            if ([_interestedIds isKindOfClass:[NSString class]])
            {
                interestedIds = [ClsSetting TrimWhiteSpaceAndNewLine:_interestedIds];
            }
            else if ([_interestedIds isKindOfClass:[NSNumber class]])
            {
                interestedIds = [_interestedIds stringValue];
            }
            
            NSArray *interestedIds_arr = [interestedIds componentsSeparatedByString:@","];
            NSString *intrestStr = @"";
            for (NSString *intrestId in interestedIds_arr)
            {
                NSInteger iID = [intrestId integerValue];
                NSArray *filterInterest_Arr = [self.intrests filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.interestedId == %ld",iID]];
                Interest *interest = ([filterInterest_Arr count] > 0) ? [filterInterest_Arr objectAtIndex:0] : nil;
                if (interest != nil)
                {
                    if (intrestStr.length == 0)
                    {
                        intrestStr = interest.interest;
                    }
                    else
                    {
                        intrestStr = [NSString stringWithFormat:@"%@,%@", intrestStr, interest.interest];
                    }
                }
            }
            _txt_interestedIn.text = intrestStr;
            
            //Postal address
            NSString *address1 = dict[@"address1"];
            _txt_postalAddressLine1.text = [ClsSetting TrimWhiteSpaceAndNewLine:address1];
            
            NSString *address2 = dict[@"address2"];
            _txt_postalAddressLine2.text = [ClsSetting TrimWhiteSpaceAndNewLine:address2];
            
            NSString *country = dict[@"country"];
            country = [ClsSetting TrimWhiteSpaceAndNewLine:country];
            _txt_postalCountry.text = country;
            
            selectedPostalCountry = [[Country alloc] init];
            selectedPostalCountry.countryname = country;

            id countryid = dict[@"countryid"];
            if ([countryid isKindOfClass:[NSNumber class]])
            {
                countryid = [countryid stringValue];
            }
            countryid = [ClsSetting TrimWhiteSpaceAndNewLine:countryid];
            
            selectedPostalCountry.countryid = countryid;
            
            NSString *countryCode = dict[@"countryCode"];
            countryCode = [ClsSetting TrimWhiteSpaceAndNewLine:countryCode];
            selectedPostalCountry.countrycode = countryCode;
            
            NSString *state = dict[@"state"];
            state = [ClsSetting TrimWhiteSpaceAndNewLine:state];
            _txt_postalState.text = state;
            
            selectedPostalState = [[State alloc] init];
            selectedPostalState.statename = state;

            selectedPostalState.countryid = countryid;
            
            id stateid = dict[@"stateid"];
            if ([stateid isKindOfClass:[NSNumber class]])
            {
                stateid = [stateid stringValue];
            }
            stateid = [ClsSetting TrimWhiteSpaceAndNewLine:stateid];
            selectedPostalState.stateid = stateid;

            NSString *city = dict[@"city"];
            city = [ClsSetting TrimWhiteSpaceAndNewLine:city];
            _txt_postalCity.text = city;
            
            selectedPostalCity = [[City alloc] init];
            selectedPostalCity.cityname = city;

            selectedPostalCity.countryid = countryid;
            selectedPostalCity.stateid = stateid;
            
            id cityid = dict[@"cityid"];
            if ([cityid isKindOfClass:[NSNumber class]])
            {
                cityid = [cityid stringValue];
            }
            cityid = [ClsSetting TrimWhiteSpaceAndNewLine:city];
            
            selectedPostalCity.cityid = cityid;
            

            NSString *zip = dict[@"zip"];
            zip = [ClsSetting TrimWhiteSpaceAndNewLine:zip];
            _txt_postalZip.text = zip;
            
            NSString *GSTIN = dict[@"GSTIN"];
            GSTIN = [ClsSetting TrimWhiteSpaceAndNewLine:GSTIN];
            _txt_postalGstin.text = GSTIN;
            
            //Billing address
            NSString *BillingName = dict[@"BillingName"];
            BillingName = [ClsSetting TrimWhiteSpaceAndNewLine:BillingName];
            _txt_billingName.text = BillingName;
            
            NSString *BillingAddress = dict[@"BillingAddress"];
            BillingAddress = [ClsSetting TrimWhiteSpaceAndNewLine:BillingAddress];
            _txt_billingAddressLine1.text = BillingAddress;
            
            NSString *billingAddress2 = dict[@"billingAddress2"];
            billingAddress2 = [ClsSetting TrimWhiteSpaceAndNewLine:billingAddress2];
            _txt_billingAddressLine2.text = billingAddress2;
            
            NSString *BillingCountry = dict[@"BillingCountry"];
            BillingCountry = [ClsSetting TrimWhiteSpaceAndNewLine:BillingCountry];
            _txt_billingCountry.text = BillingCountry;
            
            selectedBillingCountry = [[Country alloc] init];
            selectedBillingCountry.countryname = BillingCountry;

            id bCountryid = dict[@"bCountryid"];
            if ([bCountryid isKindOfClass:[NSNumber class]])
            {
                bCountryid = [bCountryid stringValue];
            }
            bCountryid = [ClsSetting TrimWhiteSpaceAndNewLine:bCountryid];
            selectedBillingCountry.countryid = bCountryid;
            
            selectedBillingCountry.countrycode = @"";

            NSString *BillingState = dict[@"BillingState"];
            BillingState = [ClsSetting TrimWhiteSpaceAndNewLine:BillingState];
            _txt_billingState.text = BillingState;
            
            selectedBillingState = [[State alloc] init];
            selectedBillingState.statename = BillingState;

            selectedBillingState.countryid = bCountryid;
            
            id bStateid = dict[@"bStateid"];
            if ([bStateid isKindOfClass:[NSNumber class]])
            {
                bStateid = [bStateid stringValue];
            }
            bStateid = [ClsSetting TrimWhiteSpaceAndNewLine:bStateid];
            selectedBillingState.stateid = bStateid;

            NSString *BillingCity = dict[@"BillingCity"];
            BillingCity = [ClsSetting TrimWhiteSpaceAndNewLine:BillingCity];
            _txt_billingCity.text = BillingCity;
            
            selectedBillingCity = [[City alloc] init];
            selectedBillingCity.cityname = BillingCity;

            selectedBillingCity.countryid = bCountryid;
            
            selectedBillingCity.stateid = bStateid;
            
            id bCityid = dict[@"bCityid"];
            if ([bCityid isKindOfClass:[NSNumber class]])
            {
                bCityid = [bCityid stringValue];
            }
            bCityid = [ClsSetting TrimWhiteSpaceAndNewLine:bCityid];
            selectedBillingCity.cityid = dict[@"bCityid"];

            NSString *BillingZip = dict[@"BillingZip"];
            BillingZip = [ClsSetting TrimWhiteSpaceAndNewLine:BillingZip];
            _txt_billingZip.text = BillingZip;
            
            //Account details
            NSString *username = dict[@"username"];
            username = [ClsSetting TrimWhiteSpaceAndNewLine:username];
            _txt_userName.text = username;
            
            NSString *password = dict[@"password"];
            password = [ClsSetting TrimWhiteSpaceAndNewLine:password];
            _txt_password.text = password;
            
            NSString *nickname = dict[@"nickname"];
            nickname = [ClsSetting TrimWhiteSpaceAndNewLine:nickname];
            _txt_nickName.text = nickname;
            
            //Bank Detils
            NSString *panCard = dict[@"panCard"];
            panCard = [ClsSetting TrimWhiteSpaceAndNewLine:panCard];
            _txt_panCard.text = [panCard uppercaseString];
            
            //NSString *urlString = @"http://astanew.infomanav.com/Content/";
            
            NSString *imagePanCard = dict[@"imagePanCard"];
            imagePanCard = [ClsSetting TrimWhiteSpaceAndNewLine:imagePanCard];
            if([imagePanCard length] > 0)
            {
                self.lbl_panCardImageName.text = imagePanCard;
                self.lbl_panCardImageName.textColor = [UIColor blackColor];
                
                self.imgView_panCard.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@panCardImage/%@",[ClsSetting profileImagesURL], imagePanCard]];

            }

            NSString *aadharCard = dict[@"aadharCard"];
            aadharCard = [ClsSetting TrimWhiteSpaceAndNewLine:aadharCard];
            _txt_aadharCard.text = aadharCard;
            
            NSString *imageAadharCard = dict[@"imageAadharCard"];
            imageAadharCard = [ClsSetting TrimWhiteSpaceAndNewLine:imageAadharCard];
            if ([imageAadharCard length] > 0)
            {
                self.lbl_aadharCardImageName.text = imageAadharCard;
                self.lbl_aadharCardImageName.textColor = [UIColor blackColor];
                
                self.imgView_aadhar.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@aadharCardImage/%@",[ClsSetting profileImagesURL], imageAadharCard]];
            }

            NSString *acNumber = dict[@"acNumber"];
            acNumber = [ClsSetting TrimWhiteSpaceAndNewLine:acNumber];
            _txt_accountNo.text = acNumber;
            
            NSString *holderName = dict[@"holderName"];
            holderName = [ClsSetting TrimWhiteSpaceAndNewLine:holderName];
            _txt_accountHolderName.text = holderName;
            
            NSString *ifscCode = dict[@"ifscCode"];
            ifscCode = [ClsSetting TrimWhiteSpaceAndNewLine:ifscCode];
            _txt_ifscCode.text = ifscCode;
            
            NSString *branchName = dict[@"branchName"];
            branchName = [ClsSetting TrimWhiteSpaceAndNewLine:branchName];
            _txt_branchName.text = branchName;
            
            NSString *swiftCode = dict[@"swiftCode"];
            swiftCode = [ClsSetting TrimWhiteSpaceAndNewLine:swiftCode];
            _txt_swiftCode.text = swiftCode;
        }
        else if ([apiCallStr isEqualToString:@"getCountry"])
        {
            NSArray *countrysResult = [Country coutrysFromJSON:arr1];
            self.coutrys = countrysResult;

            [[NSUserDefaults standardUserDefaults] setObject:arr1 forKey:@"coutrys"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            [self presentViewController:self.searchController animated:true completion:nil];
            self.searchController.searchResultsController.view.hidden = NO;
        }
        else if ([apiCallStr isEqualToString:@"getState"])
        {
            self.states = [State statesFromJSON:arr1];;
            
            [self presentViewController:self.searchController animated:true completion:nil];
            self.searchController.searchResultsController.view.hidden = NO;

        }
        else if ([apiCallStr isEqualToString:@"getCity"])
        {
            self.citys = [City citysFromJSON:arr1];;
            
            [self presentViewController:self.searchController animated:true completion:nil];
            self.searchController.searchResultsController.view.hidden = NO;
        }
    }
    else
    {
        if ([apiCallStr isEqualToString:@"getProfile"])
        {
            [ClsSetting ValidationPromt:@"User Information Not Found"];
        }
        else if ([apiCallStr isEqualToString:@"getCountry"])
        {
            [ClsSetting ValidationPromt:@"Error While Featching Country"];
        }
        else if ([apiCallStr isEqualToString:@"getState"])
        {
            [ClsSetting ValidationPromt:@"Error While Featching Staet"];
        }
        else if ([apiCallStr isEqualToString:@"getCity"])
        {
            [ClsSetting ValidationPromt:@"Error While Featching City"];
        }
    }
}

//-(void)passReseposeData1:(id)str
- (void)passPostResponseData:(id)responseObject
{
    NSArray *value = responseObject[@"resource"];
    NSLog(@"%@",value);
    NSDictionary *dictUser=[value objectAtIndex:0];
    [[NSUserDefaults standardUserDefaults] setValue:[dictUser valueForKey:@"userid"] forKey:USER_id];
    [ClsSetting ValidationPromt:@"Thank You!\nYour details will be updated soon. You might receive a verification call from our team."];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)updateProfile
{
    //NSString *urlString = @"http://astanew.infomanav.com/API/updateProfile";

    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"userid"] = [[NSUserDefaults standardUserDefaults] valueForKey:USER_id];
    
    //Profile
    params[@"genderid"] = (_btn_male.selected == YES) ? @"1" : (_btn_female.selected == YES) ? @"2" : (_btn_other.selected == YES) ? @"3" : @"0";
    params[@"bday"] = _txt_birthDay.text;
    params[@"bmonth"] = _txt_birthMonth.text;
    params[@"byear"] = _txt_birthYear.text;
    params[@"interestedIds"] = interestedIds;

    //Postal address
    params[@"address1"] = _txt_postalAddressLine1.text;
    params[@"address2"] = _txt_postalAddressLine2.text;
    params[@"country"] = _txt_postalCountry.text;
    params[@"countryid"] = selectedPostalCountry.countryid;
    //params[@"countryCode"] = selectedPostalCountry.countrycode;
    params[@"state"] = _txt_postalState.text;
    params[@"stateid"] = selectedPostalState.countryid;
    params[@"city"] = _txt_postalCity.text;
    params[@"cityid"] = selectedPostalCity.countryid;
    params[@"zip"] = _txt_postalZip.text;
    params[@"GSTIN"] = _txt_postalGstin.text;
    //Billing address
    params[@"companyName"] = _txt_billingName.text;
    params[@"BillingAddress"] = _txt_billingAddressLine1.text;
    params[@"billingAddress2"] = _txt_billingAddressLine2.text;
    params[@"BillingCountry"] = _txt_billingCountry.text;
    params[@"bCountryid"] = selectedBillingCountry.countryid;
    params[@"BillingState"] = _txt_billingState.text;
    params[@"bStateid"] = selectedBillingState.countryid;
    params[@"BillingCity"] = _txt_billingCity.text;
    params[@"bCityid"] = selectedBillingCity.countryid;
    params[@"BillingZip"] = _txt_billingZip.text;
    //Accounts details
    params[@"nickname"] = _txt_nickName.text;
    //Banking details
    params[@"panCard"] = _txt_panCard.text;
    params[@"imagePanCard"] = _lbl_panCardImageName.text;
    params[@"aadharCard"] = _txt_aadharCard.text;
    params[@"imageAadharCard"] = _lbl_aadharCardImageName.text;
    params[@"acNumber"] = _txt_accountNo.text;
    params[@"holderName"] = _txt_accountHolderName.text;
    params[@"ifscCode"] = _txt_ifscCode.text;
    params[@"branchName"] = _txt_branchName.text;
    params[@"swiftCode"] = _txt_swiftCode.text;
    params[@"MobileVerified"] = @"1";
//     params[@"PanImage"] = @"";
//    params[@"AadharImage"] = @"";
    
    
//    NSLog(@"params == %@", params);
        
    NSData *panImageData;
    if (isPanCardSelect == 1)
    {
        panImageData = UIImageJPEGRepresentation(_imgView_panCard.image, 0.5f);
    }
    
    NSData *aadharImageData;
    if (isAadharCardSelect == 1)
    {
        aadharImageData = UIImageJPEGRepresentation(_imgView_aadhar.image, 0.5f);
    }

    __block float per;
    
//    NSData *imgData = UIImageJPEGRepresentation(IMAGE_HERE, 0.9);
//    formReq = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];
//    formReq.delegate = self;
//    [formReq setPostValue:VALUE1 forKey:KEY1];
//    [formReq setPostValue:VALUE2 forKey:KEY2];
//    if (imgData) {
//        [formReq setData:imgData withFileName:@"SAMPLE.jpg" andContentType:@"image/jpeg" forKey:IMAGE_KEY];
//    }
//    [formReq startSynchronous];
    
    
    
//    [manager POST:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject)
//    {
//        NSLog(@"%@",responseObject);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//    }];
    
    
//    NSDictionary *jsonDictionary = params;
//       NSData *imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"sampleJohnTsioris"], 0.5);

//       NSError *error;
//       NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:0 error:&error];
//       if (!jsonData) {
//           NSLog(@"JSON ERROR");
//       } else {
//           NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
//           NSLog(@"JSON OUTPUT: %@",JSONString);
//       }

//      AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//       [manager POST:[ClsSetting profileAPIURL] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//           manager.requestSerializer=[AFHTTPRequestSerializer serializer];
//           manager.responseSerializer=[AFHTTPResponseSerializer serializer];
//           manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@[@"application/json",@"text/html"]];
//           NSMutableDictionary *headers = [NSMutableDictionary dictionaryWithObjectsAndKeys: @"application/json; charset=UTF-8",@"Content-Type", @"form-data; name=\"registration\"",@"Content-Disposition", nil];
//           [formData appendPartWithHeaders:headers body:jsonData];
////           [formData appendPartWithFileData:imageData name:@"pic" fileName:@"avatar.png" mimeType:@"image/png"];
//
//       } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//           NSLog(@"operation=%@",operation);
//           NSLog(@"Response: %@", responseObject);
//       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//           NSLog(@"operation=%@",operation);
//           NSLog(@"Error: %@", error);
//       }];
//    [manager start];
    
//    NSDictionary *jsonDictionary = params;
////    NSData *imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"sampleJohnTsioris"], 0.5);
//
//    NSError *error;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:0 error:&error];
//    if (!jsonData) {
//        NSLog(@"JSON ERROR");
//    } else {
//        NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
//        NSLog(@"JSON OUTPUT: %@",JSONString);
//    }
//
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    [manager POST:[ClsSetting profileAPIURL] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//
//        NSMutableDictionary *headers = [NSMutableDictionary dictionaryWithObjectsAndKeys: @"application/json; charset=UTF-8",@"Content-Type", @"form-data; name=\"registration\"",@"Content-Disposition", nil];
//        [formData appendPartWithHeaders:headers body:jsonData];
////        [formData appendPartWithFileData:imageData name:@"pic" fileName:@"avatar.png" mimeType:@"image/png"];
//          NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
//   NSLog(@"JSON OUTPUT: %@",JSONString);
//
//    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"operation=%@",operation);
//        NSLog(@"Response: %@", responseObject);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"operation=%@",operation);
//        NSLog(@"Error: %@", error);
//    }];
    
    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//       manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//     AFHTTPRequestOperation *operation = [manager POST:[ClsSetting profileAPIURL] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
//                                         {
//
//     }
    
    
    
//    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
//    NSString*userID = [def objectForKey:@"UserId"];
//    NSMutableDictionary *getUpdates= [[NSMutableDictionary alloc]init];
//    [getUpdates setObject:userID forKey:@"userId"];
//    [getUpdates setObject:phoneNumTF.text forKey:@"mobileNumber"];
//    [getUpdates setObject:nameTF.text forKey:@"name"];
//    [getUpdates setObject:workZipCodeTF.text forKey:@"work"];
//    [getUpdates setObject:homeZipCodeTF.text forKey:@"home"];
    
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer]; // only needed if the server is not returning JSON; if web service returns JSON, remove this line
//
//
//    [manager POST:[ClsSetting profileAPIURL] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        NSError *error;
//        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
//        NSAssert(jsonData, @"Failure building JSON: %@", error);
//
//
//        NSDictionary *jsonHeaders = @{@"Content-Disposition" : @"form-data; name=\"json\"",
//                                      @"Content-Type"        : @"application/json"};
//        [formData appendPartWithHeaders:jsonHeaders body:jsonData];
//         [params enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL* stop)
//                                    {
//                            if ([value isKindOfClass:[NSNumber class]])
//                                            {
//                        [formData appendPartWithFormData:[[value stringValue] dataUsingEncoding:NSUTF8StringEncoding] name:key];
//                                                          }
//                                                          else
//                                        {
//                        [formData appendPartWithFormData:[value dataUsingEncoding:NSUTF8StringEncoding] name:key];
//                                                          }
//                                                      }];
//
//                                                     if (panImageData != nil)
//                                                     {
//                                                         [formData appendPartWithFileData:panImageData
//                                                                                     name:@"PanImage"
//                                                                                 fileName:_lbl_panCardImageName.text
//                                                                                 mimeType:@"image/jpeg"];
//                                                     }
//        //                                             else{
//        //                                                 params[@"PanImage"] = @"";
//        //                                             }
//
//                                                     if (aadharImageData != nil)
//                                                     {
//                                                         [formData appendPartWithFileData:aadharImageData
//                                                                                     name:@"AadharImage"
//                                                                                 fileName:_lbl_aadharCardImageName.text
//                                                                                 mimeType:@"image/jpeg"];
//                                                     }
//
//
//        NSLog(@"%@",params);
//    } success:^(NSURLSessionDataTask *task, id responseObject) {
//        NSLog(@"Response: %@", responseObject);
//
//        NSError* error;
//        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
//                                                             options:kNilOptions
//                                                               error:&error];
//        NSLog(@"error appending part: %@", json);
//
//        [MBProgressHUD hideHUDForView:self.view animated:true];
//
//        [self.navigationController popViewControllerAnimated:YES];
//
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        NSLog(@"Error: %@", error);
//        [MBProgressHUD hideHUDForView:self.view animated:true];
//
//    }];
    
    
    
    
    
    
    
    
    
    //result
    
    
    NSDictionary *body = params;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:body options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];

    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@",[ClsSetting profileAPIURL]] parameters:nil error:nil];

    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];


    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {

        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);

            if ([responseObject isKindOfClass:[NSDictionary class]]) {
//                                                         NSError *err;
//                            NSDictionary *JSONData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:&err];
                NSDictionary *JSONData = (NSDictionary *)responseObject;
                   NSLog(@"jsonDictionary - %@",JSONData);
                                                         NSLog(@"JSONData = %@", JSONData);
                                                         if (JSONData.count == 0)
                                                         {
                                                             [ClsSetting ValidationPromt:@"Server Error!"];
                                                         }
                                                         else if ([[JSONData objectForKey:@"status"] isEqualToString:@"Success"])
                                                         {
                                                             [ClsSetting ValidationPromt:@"Profile Updated Successfully"];
                                                         }
                                                         else if ([[JSONData objectForKey:@"status"] isEqualToString:@"Fail"])
                                                         {
                                                             [ClsSetting ValidationPromt:@"Profile Updated Fail"];
                                                         }
                                                         else
                                                         {
                                                             [ClsSetting ValidationPromt:@"Server Error!"];
                                                         }
                                                         [HUD hide:TRUE];
                                                         [HUD removeFromSuperViewOnHide];
                                                     }
        } else {
                                                     [ClsSetting ValidationPromt:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
                                                     [HUD hide:TRUE];
                                                     [HUD removeFromSuperViewOnHide];
                                                 }
    }] resume];
    
    
    
    //result1
    
    
    
//        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//        manager.responseSerializer = [AFHTTPResponseSerializer serializer];

//    AFHTTPRequestOperation *operation = [manager POST:[ClsSetting profileAPIURL] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
//                                         {
//
//                [params enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL* stop)
//                            {
//                    if ([value isKindOfClass:[NSNumber class]])
//                                    {
//                [formData appendPartWithFormData:[[value stringValue] dataUsingEncoding:NSUTF8StringEncoding] name:key];
//                                                  }
//                                                  else
//                                {
//                [formData appendPartWithFormData:[value dataUsingEncoding:NSUTF8StringEncoding] name:key];
//                                                  }
//                                              }];
//
//                                             if (panImageData != nil)
//                                             {
//                                                 [formData appendPartWithFileData:panImageData
//                                                                             name:@"PanImage"
//                                                                         fileName:_lbl_panCardImageName.text
//                                                                         mimeType:@"image/jpeg"];
//                                             }
////                                             else{
////                                                 params[@"PanImage"] = @"";
////                                             }
//
//                                             if (aadharImageData != nil)
//                                             {
//                            [formData appendPartWithFileData:aadharImageData
//                                                    name:@"AadharImage"
//                                                        fileName:_lbl_aadharCardImageName.text
//                                                                         mimeType:@"image/jpeg"];
//
//                                                NSData *imageData = UIImageJPEGRepresentation(_imgView_aadhar.image, 0.5f);
//
//                                                 NSString *urlString = [ NSString stringWithFormat:@"%@", [ClsSetting profileAPIURL]];
//
//                                                 NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//                                                        [request setURL:[NSURL URLWithString:urlString]];
//                                                        [request setHTTPMethod:@"POST"];
//
//                                                 NSString *boundary = @"---------------------------14737809831466499882746641449";
//                                                        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
//                                                        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
//
//                                                        NSMutableData *body = [NSMutableData data];
//                                                        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//                                                        [body appendData:[[NSString stringWithString:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"AadharImage\"; filename=\"%@\"\r\n", _lbl_aadharCardImageName.text]] dataUsingEncoding:NSUTF8StringEncoding]];
//                                                 [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//                                                        [body appendData:[NSData dataWithData:imageData]];
//                                                        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//
//                                                        [request setHTTPBody:body];
//
//                                                        [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
//
//                                                 // Get Response of Your Request
//                                                 NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
//                                                 NSString *responseString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
//                                                 NSLog(@"Response  %@",responseString);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//                                             }
//        NSLog(@"%@",params);
//            NSDictionary *body = params;
//            NSError *error;
//            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:body options:0 error:&error];
//            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//
//            AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//
//            NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@",[ClsSetting profileAPIURL]] parameters:nil error:nil];
//
//            req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
//            [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//            [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//            [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
//
//
//            [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//
//                if (!error) {
//                    NSLog(@"Reply JSON: %@", responseObject);
//
//                    if ([responseObject isKindOfClass:[NSDictionary class]]) {
//        //                                                         NSError *err;
//        //                            NSDictionary *JSONData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:&err];
//                        NSDictionary *JSONData = (NSDictionary *)responseObject;
//                           NSLog(@"jsonDictionary - %@",JSONData);
//                                                                 NSLog(@"JSONData = %@", JSONData);
//                                                                 if (JSONData.count == 0)
//                                                                 {
//                                                                     [ClsSetting ValidationPromt:@"Server Error!"];
//                                                                 }
//                                                                 else if ([[JSONData objectForKey:@"status"] isEqualToString:@"Success"])
//                                                                 {
//                                                                     [ClsSetting ValidationPromt:@"Profile Updated Successfully"];
//                                                                 }
//                                                                 else if ([[JSONData objectForKey:@"status"] isEqualToString:@"Fail"])
//                                                                 {
//                                                                     [ClsSetting ValidationPromt:@"Profile Updated Fail"];
//                                                                 }
//                                                                 else
//                                                                 {
//                                                                     [ClsSetting ValidationPromt:@"Server Error!"];
//                                                                 }
//                                                                 [HUD hide:TRUE];
//                                                                 [HUD removeFromSuperViewOnHide];
//                                                             }
//                } else {
//                                                             [ClsSetting ValidationPromt:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
//                                                             [HUD hide:TRUE];
//                                                             [HUD removeFromSuperViewOnHide];
//                                                         }
//            }] resume];
//
//
//
//
//
//
//        NSLog(@"%@",params);
//                                         }
//
//
//                                              success:^(AFHTTPRequestOperation *operation, id responseObject)
//                                         {
//                                             NSError *err;
//                NSDictionary *JSONData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:&err];
//                                             NSLog(@"JSONData = %@", JSONData);
//                                             if (JSONData.count == 0)
//                                             {
//                                                 [ClsSetting ValidationPromt:@"Server Error!"];
//                                             }
//                                             else if ([[JSONData objectForKey:@"status"] isEqualToString:@"Success"])
//                                             {
//                                                 [ClsSetting ValidationPromt:@"Profile Updated Successfully"];
//                                             }
//                                             else if ([[JSONData objectForKey:@"status"] isEqualToString:@"Fail"])
//                                             {
//                                                 [ClsSetting ValidationPromt:@"Profile Updated Fail"];
//                                             }
//                                             else
//                                             {
//                                                 [ClsSetting ValidationPromt:@"Server Error!"];
//                                             }
//                                             [HUD hide:TRUE];
//                                             [HUD removeFromSuperViewOnHide];
//                                         }
//                                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
//                                         {
////                                             [ClsSetting ValidationPromt:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
////                                             [HUD hide:TRUE];
////                                             [HUD removeFromSuperViewOnHide];
//                                         }];
//     4. Set the progress block of the operation.
//    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
//                                        long long totalBytesWritten,
//                                        long long totalBytesExpectedToWrite)
//     {
//         per = (float)((totalBytesWritten*100)/totalBytesExpectedToWrite);
//         if (per == 100.00)
//         {
//             HUD.labelText = @"Please wait!";
//             HUD.detailsLabelText = [NSString stringWithFormat:@"%.2f %@",per, @"%"];
//             HUD.mode = MBProgressHUDModeIndeterminate;
//         }
//         else
//         {
//             HUD.detailsLabelText = [NSString stringWithFormat:@"%.2f %@",per, @"%"];
//             HUD.progress = (float)totalBytesWritten/totalBytesExpectedToWrite;
//         }
//     }];
//    [operation start];
}


- (IBAction)yes:(UIButton *)sender {
}
@end
