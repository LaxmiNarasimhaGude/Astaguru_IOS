//
//  Country.h
//  AstaGuru
//
//  Created by Apple.Inc on 26/08/19.
//  Copyright © 2019 4Fox Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Country : NSObject

@property (strong, nonatomic) NSString *countrycode;
@property (strong, nonatomic) NSString *countryid;
@property (strong, nonatomic) NSString *countryname;

+ (NSArray *)coutrysFromJSON:(NSArray *)responseObject;

@end


@interface State : NSObject

@property (strong, nonatomic) NSString *stateid;
@property (strong, nonatomic) NSString *countryid;
@property (strong, nonatomic) NSString *statename;

+ (NSArray *)statesFromJSON:(NSArray *)responseObject;

@end

@interface City : NSObject

@property (strong, nonatomic) NSString *cityid;
@property (strong, nonatomic) NSString *stateid;
@property (strong, nonatomic) NSString *countryid;
@property (strong, nonatomic) NSString *cityname;

+ (NSArray *)citysFromJSON:(NSArray *)responseObject;

@end


@interface Interest : NSObject

@property (nonatomic, assign) NSInteger interestedId;
@property (strong, nonatomic) NSString *interest;
@property (strong, nonatomic) NSString *status;

+ (NSArray *)interestsFromJSON:(NSArray *)responseObject;

@end

@interface About : NSObject

@property (nonatomic, assign) NSString *aboutId;
@property (strong, nonatomic) NSString *about;
@property (strong, nonatomic) NSString *status;

+ (NSArray *)aboutsFromJSON:(NSArray *)responseObject;

@end

NS_ASSUME_NONNULL_END
