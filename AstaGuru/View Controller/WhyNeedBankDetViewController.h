//
//  WhyNeedBankDetViewController.h
//  AstaGuru
//
//  Created by macbook on 09/04/20.
//  Copyright © 2020 4Fox Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WhyNeedBankDetViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labelText;

@property bool isFromBiling;
@end

NS_ASSUME_NONNULL_END
