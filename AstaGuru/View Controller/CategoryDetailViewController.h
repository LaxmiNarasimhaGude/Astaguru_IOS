//
//  CategoryDetailViewController.h
//  AstaGuru
//
//  Created by Apple.Inc on 11/12/18.
//  Copyright © 2018 4Fox Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "clsCategory.h"

NS_ASSUME_NONNULL_BEGIN

@interface CategoryDetailViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIBarButtonItem *sideleftbarButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UICollectionView *clv_aboutus;
@property (nonatomic, retain) NSString *catID;
@property (nonatomic, retain) clsCategory *objCategory;
@end

NS_ASSUME_NONNULL_END
