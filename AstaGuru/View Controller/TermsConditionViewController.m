//
//  TermsConditionViewController.m
//  AstaGuru
//
//  Created by Aarya Tech on 05/10/16.
//  Copyright © 2016 Aarya Tech. All rights reserved.
//

#import "TermsConditionViewController.h"
#import "clsAboutUs.h"
#import "OurValutionCollectionViewCell.h"
#import "SectionHeaderReusableView.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"

@interface TermsConditionViewController ()
{
    NSMutableArray *arrTC;
    NSMutableArray *arrTCTitle;
}

@end

@implementation TermsConditionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.sideleftbarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-close"] style:UIBarButtonItemStyleDone target:self action:@selector(closePressed)];
    self.sideleftbarButton.tintColor = [UIColor whiteColor];
    [[self navigationItem] setRightBarButtonItem:self.sideleftbarButton];
    self.navigationItem.title=@"Terms & Conditions";
    
    self.sidebarButton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"signs"] style:UIBarButtonItemStyleDone target:self.revealViewController action:@selector(revealToggle:)];
    self.sidebarButton.tintColor=[UIColor whiteColor];
    [[self navigationItem] setLeftBarButtonItem:self.sidebarButton];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    arrTC = [[NSMutableArray alloc] init];
    arrTCTitle = [[NSMutableArray alloc] init];

    [self createTCArray];

}

-(void)closePressed
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SWRevealViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    UIViewController *viewController =rootViewController;
    AppDelegate * objApp = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    objApp.window.rootViewController = viewController;
    
}

-(void)createTCArray
{
    //GENERAL TERMS
    [arrTCTitle addObject:@"GENERAL TERMS"];
    
    NSMutableArray *arrSection1 = [[NSMutableArray alloc]init];
    
    clsAboutUs *obj_1_1 = [[clsAboutUs alloc]init];
    obj_1_1.strName = @"By participating in this auction, you acknowledge that you are bound by the Conditions for Sale listed below and on the website www.astaguru.com\n";
    obj_1_1.strType=@"1";
    [arrSection1 addObject:obj_1_1];
    
    clsAboutUs *obj_1_2 = [[clsAboutUs alloc]init];
    obj_1_2.strName = @"Securing the Winning Bid results in an enforceable contract of sale.";
    obj_1_2.strType = @"2";
    [arrSection1 addObject:obj_1_2];
    
    clsAboutUs *obj_1_3 = [[clsAboutUs alloc]init];
    obj_1_3.strName = @"AstaGuru is authorized by source to display at AstaGuru's discretion images and description of all lots in the catalogue and on the website.";
    obj_1_3.strType = @"2";
    [arrSection1 addObject:obj_1_3];
    
    clsAboutUs *obj_1_4 = [[clsAboutUs alloc]init];
    obj_1_4.strName = @"AstaGuru can grant, record and reject any bid.";
    obj_1_4.strType = @"2";
    [arrSection1 addObject:obj_1_4];
    
    clsAboutUs *obj_1_5 = [[clsAboutUs alloc]init];
    obj_1_5.strName = @"Bidding access shall be given at AstaGuru's discretion.";
    obj_1_5.strType = @"2";
    [arrSection1 addObject:obj_1_5];
    
    clsAboutUs *obj_1_6 = [[clsAboutUs alloc]init];
    obj_1_6.strName = @"AstaGuru may ask for a deposit of Rs. 500,000 or an equivalent in US$ on lots prior to giving bidding access.";
    obj_1_6.strType = @"2";
    [arrSection1 addObject:obj_1_6];
    
    clsAboutUs *obj_1_7 = [[clsAboutUs alloc]init];
    obj_1_7.strName = @"AstaGuru may review bid history of specific lots periodically to preserve the efficacy of the auction process.";
    obj_1_7.strType = @"2";
    [arrSection1 addObject:obj_1_7];
    
    clsAboutUs *obj_1_8 = [[clsAboutUs alloc]init];
    obj_1_8.strName = @"AstaGuru has the right to withdraw a lot before, during or after the bidding, if it has reason to believe that the authenticity of the Property or accuracy of description is in doubt.";
    obj_1_8.strType = @"2";
    [arrSection1 addObject:obj_1_8];
    
    
    clsAboutUs *obj_1_9 = [[clsAboutUs alloc]init];
    obj_1_9.strName = @"Reserve price on lots are confidential.";
    obj_1_9.strType = @"2";
    [arrSection1 addObject:obj_1_9];
    
    clsAboutUs *obj_1_10 = [[clsAboutUs alloc]init];
    obj_1_10.strName = @"AstaGuru shall raise all invoices including Auction House Margin and related taxes.";
    obj_1_10.strType = @"2";
    [arrSection1 addObject:obj_1_10];
    
    
    clsAboutUs *obj_1_11 = [[clsAboutUs alloc]init];
    obj_1_11.strName = @"The Auction House Margin shall be calculated at 15% on the hammer price.";
    obj_1_11.strType = @"2";
    [arrSection1 addObject:obj_1_11];
    
    clsAboutUs *obj_1_12 = [[clsAboutUs alloc]init];
    obj_1_12.strName = @"All foreign currency exchange rates during the Auction are made on a constant. However they are subject to change for each auction.";
    obj_1_12.strType = @"2";
    [arrSection1 addObject:obj_1_12];
    
    clsAboutUs *obj_1_13 = [[clsAboutUs alloc]init];
    obj_1_13.strName = @"All invoicing details should be provided by the buyer prior to the auction.";
    obj_1_13.strType = @"2";
    [arrSection1 addObject:obj_1_13];
    
    clsAboutUs *obj_1_14 = [[clsAboutUs alloc]init];
    obj_1_14.strName = @"All payments shall be made within 7 days from the date of the invoice.";
    obj_1_14.strType = @"2";
    [arrSection1 addObject:obj_1_14];
    
    clsAboutUs *obj_1_15 = [[clsAboutUs alloc]init];
    obj_1_15.strName = @"In case payment is not made within the stated time period, it shall be treated as a breach of contract, AstaGuru shall take any steps including the institution of legal proceedings.";
    obj_1_15.strType=@"2";
    [arrSection1 addObject:obj_1_15];
    
    clsAboutUs *obj_1_16 = [[clsAboutUs alloc]init];
    obj_1_16.strName = @"AstaGuru will charge a 2% late payment fine per month.";
    obj_1_15.strType = @"2";
    [arrSection1 addObject:obj_1_16];
    
    clsAboutUs *obj_1_17 = [[clsAboutUs alloc]init];
    obj_1_17.strName = @"If the buyer wishes to collect the property from AstaGuru, it must be collected within 30 Days from the date of the auction. The buyer shall be charged a 2% storage fee if the property is not collected on time.";
    obj_1_17.strType = @"2";
    [arrSection1 addObject:obj_1_17];
    
    clsAboutUs *obj_1_18=[[clsAboutUs alloc]init];
    obj_1_18.strName = @"AstaGuru reserves the right not to award the Winning Bid to the Bidder with the highest Bid at Closing Date if it deems it necessary to do so.";
    obj_1_18.strType = @"2";
    [arrSection1 addObject:obj_1_18];
    
    clsAboutUs *obj_1_19 = [[clsAboutUs alloc]init];
    obj_1_19.strName = @"In an unlikely event of any technical failure and the website is inaccessible, the lot closing time shall be extended.";
    obj_1_19.strType = @"2";
    [arrSection1 addObject:obj_1_19];
    
    clsAboutUs *obj_1_20 = [[clsAboutUs alloc]init];
    obj_1_20.strName = @"Bids recorded prior to the technical problem shall stand valid according to the terms of sale.";
    obj_1_20.strType = @"2";
    [arrSection1 addObject:obj_1_20];
    
    clsAboutUs *obj_1_21 = [[clsAboutUs alloc]init];
    obj_1_21.strName = @"All Lots are offered during the auction in its ‘present/current condition’, this implies and encompasses all existing faults and imperfections. We encourage all potential buyers to inspect each item carefully before bidding.";
    obj_1_21.strType = @"2";
    [arrSection1 addObject:obj_1_21];
    
    //2. AUTHENTICITY GUARANTEE
    [arrTCTitle addObject:@"AUTHENTICITY GUARANTEE"];

    NSMutableArray *arrSection2 = [[NSMutableArray alloc]init];

    clsAboutUs *obj_2_1 = [[clsAboutUs alloc]init];
    obj_2_1.strName = @"AstaGuru assures on behalf of source that all properties on the website are genuine works of the artists listed.";
    obj_2_1.strType = @"2";
    [arrSection2 addObject:obj_2_1];
    
    clsAboutUs *obj_2_2 = [[clsAboutUs alloc]init];
    obj_2_2.strName = @"However in an unlikely event if a property is proved to be unauthentic to AstaGuru’s satisfaction within a period of 6 months from the collection date, the seller shall be liable to pay back the full amount to the buyer. These claims will be handled on a case-by-case basis, and will require that examinable proof which clearly demonstrates that the property is unauthentic is provided by an established and acknowledged authority. Only the actual buyer (as registered with AstaGuru) can make the claim.";
    obj_2_2.strType = @"2";
    [arrSection2 addObject:obj_2_2];
    
    clsAboutUs *obj_2_3 = [[clsAboutUs alloc]init];
    obj_2_3.strName = @"The property when returned, should be in the same condition as when it was purchased.";
    obj_2_3.strType = @"2";
    [arrSection2 addObject:obj_2_3];
    
    clsAboutUs *obj_2_4 = [[clsAboutUs alloc]init];
    obj_2_4.strName = @"AstaGuru shall charge the buyer in case any steps are to be taken for special expenses that shall take place in order to prove the authenticity of the property.";
    obj_2_4.strType = @"2";
    [arrSection2 addObject:obj_2_4];
    
    clsAboutUs *obj_2_5 = [[clsAboutUs alloc]init];
    obj_2_5.strName = @"In case the source fails to refund the amount, Astaguru shall be authorized by the buyer to take legal action on behalf of the buyer to recover the money at the expense of the buyer.";
    obj_2_5.strType = @"2";
    [arrSection2 addObject:obj_2_5];
    
    
    //3. EXTENT OF ASTAGURU'S LIABILITY
    [arrTCTitle addObject:@"EXTENT OF ASTAGURU'S LIABILITY"];

    NSMutableArray *arrSection3 = [[NSMutableArray alloc]init];

    clsAboutUs *obj_3_1 = [[clsAboutUs alloc]init];
    obj_3_1.strName=@"AstaGuru has to obtain the money from the source and refund to the buyer the amount of purchase value in case the work is not authentic.";
    obj_3_1.strType = @"2";
    [arrSection3 addObject:obj_3_1];
    
    clsAboutUs *obj_3_2 = [[clsAboutUs alloc]init];
    obj_3_2.strName = @"All damages and loss during transit must be covered by the insurance policy, AstaGuru will not be liable.";
    obj_3_2.strType = @"2";
    [arrSection3 addObject:obj_3_2];
    
    clsAboutUs *obj_3_3 = [[clsAboutUs alloc]init];
    obj_3_3.strName = @"AstaGuru or any member of its team is not liable for any mistakes made in the catalogue.";
    obj_3_3.strType = @"2";
    [arrSection3 addObject:obj_3_3];
    
    clsAboutUs *obj_3_4 = [[clsAboutUs alloc]init];
    obj_3_4.strName = @"The description and additional information provided by Astaguru in the catalogue is to our best opinion, however we have not undertaken a comprehensive research exercise and therefore potential buyers are encouraged to gauge and consult with professional advisors of their preference.";
    obj_3_4.strType = @"2";
    [arrSection3 addObject:obj_3_4];

    clsAboutUs *obj_3_5 = [[clsAboutUs alloc]init];
    obj_3_5.strName = @"If any discrepancy with regards to description and additional information occurs Astaguru/Astaguru Team Members are not liable for the same.";
    obj_3_5.strType = @"2";
    [arrSection3 addObject:obj_3_5];
    
    clsAboutUs *obj_3_6 = [[clsAboutUs alloc]init];
    obj_3_6.strName = @"Astaguru’s liability with regards to authenticity is limited to the values mentioned in Capital letters in the title heading section of the Lot. Furthermore the Astaguru’s liability for the same is limited for a period of one year from the date of the auction of the lots.";
    obj_3_6.strType = @"2";
    [arrSection3 addObject:obj_3_6];
    
    clsAboutUs *obj_3_7 = [[clsAboutUs alloc]init];
    obj_3_7.strName = @"AstaGuru is not liable for any claims on insurance.";
    obj_3_7.strType = @"2";
    [arrSection3 addObject:obj_3_7];
    
    clsAboutUs *obj_3_8 = [[clsAboutUs alloc]init];
    obj_3_8.strName = @"AstaGuru is not liable in case the website has any technical problems.";
    obj_3_8.strType = @"2";
    [arrSection3 addObject:obj_3_8];
    
    clsAboutUs *obj_3_9 = [[clsAboutUs alloc]init];
    obj_3_9.strName = @"If any part of the Conditions for Sale between the Buyer and AstaGuru is found by any court to be invalid, illegal or unenforceable, that part may be discounted and the rest of the conditions shall be enforceable to the fullest extent permissible by law.";
    obj_3_9.strType = @"2";
    [arrSection3 addObject:obj_3_9];
    
    clsAboutUs *obj_3_10 = [[clsAboutUs alloc]init];
    obj_3_10.strName = @"The said named Owner being the principal is the sole, absolute and rightful owner of the ARTWORK(S) and is able to transfer good and marketable title over the ARTWORK(S) to AstaGuru, free from any liens, encumbrances, unpaid taxes or dues or charges. There are no legal proceedings pending in respect of the ARTWORK(S) and the ARTWORK(S)is not the subject matter of any dispute.";
    obj_3_10.strType = @"2";
    [arrSection3 addObject:obj_3_10];
    
    //4. EXTENT OF THE BIDDER’S LIABILITY
    [arrTCTitle addObject:@"EXTENT OF THE BIDDER’S LIABILITY"];

    NSMutableArray *arrSection4 = [[NSMutableArray alloc]init];

    clsAboutUs *obj_4_1 = [[clsAboutUs alloc]init];
    obj_4_1.strName = @"The bidder must take the onus of inspecting the condition and cross checking the descriptive information with regards to the lots prior to the auction schedule.";
    obj_4_1.strType = @"2";
    [arrSection4 addObject:obj_4_1];
    
    clsAboutUs *obj_4_2 = [[clsAboutUs alloc]init];
    obj_4_2.strName = @"The bidder must assert their decision based on their own judgment.";
    obj_4_2.strType = @"2";
    [arrSection4 addObject:obj_4_2];
    
    clsAboutUs *obj_4_3 = [[clsAboutUs alloc]init];
    obj_4_3.strName = @"In parity with the unpredictable nature of internet/Mobile App data network there may be a lag period between the time of the bidder placing the bid and it being received/registered by Astaguru’s server. Although the bidder may have placed a bid within the stipulated closing schedule, none the less id Astaguru receives the same post the closing schedule the bid will be rejected under such circumstances. Therefore bidders are requested to update their account with the relevant Proxy Bid in order to safeguard any lag oriented delays while placing individual bids.";
    obj_4_3.strType = @"2";
    [arrSection4 addObject:obj_4_3];
    
    
    //5. LAW AND JURISDICTION
    [arrTCTitle addObject:@"LAW AND JURISDICTION"];
    
    NSMutableArray *arrSection5 = [[NSMutableArray alloc]init];

    clsAboutUs *obj_5_1 = [[clsAboutUs alloc]init];
    obj_5_1.strName = @"The terms and conditions of this Auction are subject to the laws of India, which will apply to the construction and to the effect of the clauses. All parties are subject to the exclusive jurisdiction of the courts at Mumbai, Maharashtra, India.";
    obj_5_1.strType = @"1";
    [arrSection5 addObject:obj_5_1];
    
    clsAboutUs *obj_5_2 = [[clsAboutUs alloc]init];
    obj_5_2.strName=@"Under the circumstances that a legal notice has to be issued by Astaguru, the modes to do so are as follows:";
    obj_5_2.strType=@"1";
    [arrSection5 addObject:obj_5_2];
    
    clsAboutUs *obj_5_3 = [[clsAboutUs alloc]init];
    obj_5_3.strName = @"I: Via an email to the registered email id as per Astaguru’s records.";
    obj_5_3.strType = @"1";
    [arrSection5 addObject:obj_5_3];
    
    clsAboutUs *obj_5_4 = [[clsAboutUs alloc]init];
    obj_5_1.strName = @"II: Via dispatch of a courier to the registered address as per Astaguru’s records.";
    obj_5_4.strType = @"1";
    [arrSection5 addObject:obj_5_4];
    
    //TC array
    [arrTC addObject:arrSection1];
    [arrTC addObject:arrSection2];
    [arrTC addObject:arrSection3];
    [arrTC addObject:arrSection4];
    [arrTC addObject:arrSection5];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return arrTC.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
//    if (section == 0)
//    {
//        return 1;
//    }
//    else
//    {
        NSMutableArray *tcArr = [arrTC objectAtIndex:section];
        return tcArr.count;
//    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.section==0)
//    {
//        UICollectionViewCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellline" forIndexPath:indexPath];
//        return cell;
//    }
//    else
//    {
        OurValutionCollectionViewCell *ourValutioncell = [collectionView dequeueReusableCellWithReuseIdentifier:@"valuation" forIndexPath:indexPath];
        
        NSMutableArray *tcArr = [arrTC objectAtIndex: indexPath.section];
        
        clsAboutUs *objAboutUs = [tcArr objectAtIndex:indexPath.row];
        
        if ([objAboutUs.strType intValue]==1)
        {
            ourValutioncell.wtImageConstant.constant = -10;
        }
        else
        {
            ourValutioncell.wtImageConstant.constant = 15;
        }
        
        ourValutioncell.lblTitle.text=objAboutUs.strName;
        ourValutioncell.lblTitle.textAlignment = NSTextAlignmentJustified;
        
        return ourValutioncell;
//    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView1 layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.section==0)
//    {
//        return CGSizeMake(collectionView1.frame.size.width, 20);
//    }
//    else
//    {
        NSMutableArray *tcArr = [arrTC objectAtIndex: indexPath.section];
        clsAboutUs *objAboutUs = [tcArr objectAtIndex:indexPath.row];
        
        CGSize maximumLabelSize;
        if ([objAboutUs.strType intValue] == 1)
        {
            maximumLabelSize = CGSizeMake(collectionView1.frame.size.width-29, FLT_MAX);
            CGRect labelRect = [objAboutUs.strName
                                boundingRectWithSize:maximumLabelSize
                                options:NSStringDrawingUsesLineFragmentOrigin
                                attributes:@{
                                             NSFontAttributeName : [UIFont boldSystemFontOfSize:16]
                                             }
                                context:nil];
            float height11= labelRect.size.height+20;
            return CGSizeMake(collectionView1.frame.size.width, height11);
        }
        else
        {
            maximumLabelSize = CGSizeMake(collectionView1.frame.size.width-44, FLT_MAX);
            CGRect labelRect = [objAboutUs.strName
                                boundingRectWithSize:maximumLabelSize
                                options:NSStringDrawingUsesLineFragmentOrigin
                                attributes:@{
                                             NSFontAttributeName : [UIFont systemFontOfSize:16]
                                             }
                                context:nil];
            
            float height11= labelRect.size.height+5;
            return CGSizeMake(collectionView1.frame.size.width, height11+10);
        }
//    }
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader)
    {
        SectionHeaderReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
//        if (indexPath.section == 0)
//        {
//            headerView.title.hidden=YES;
//        }
//        else
//        {
            headerView.title.hidden=NO;
            headerView.title.text = [arrTCTitle objectAtIndex:indexPath.section];
//        }
        
        reusableview = headerView;
    }
    
    if (kind == UICollectionElementKindSectionFooter)
    {
        UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"FooterView" forIndexPath:indexPath];
        
        reusableview = footerview;
    }
    
    return reusableview;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
//    if (section==0)
//    {
//     return CGSizeMake(CGRectGetWidth(collectionView.bounds), 0);
//    }
//    else
//    {
        return CGSizeMake(CGRectGetWidth(collectionView.bounds), 50);
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
