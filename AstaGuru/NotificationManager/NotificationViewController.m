//
//  NotificationViewController.m
//  Tokative
//
//  Created by Amrit Singh on 12/13/16.
//  Copyright © 2016 FoxSolution. All rights reserved.
//

#import "NotificationViewController.h"
#import "NotificationTableViewCell.h"
#import "NSString+TimeCalculation.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "ClsSetting.h"
#import "BforeLoginViewController.h"
#import "SWRevealViewController.h"
#import "NotificationDetailViewController.h"
#import "AppDelegate.h"
#import "DB.h"
#import "dataBaseModel.h"
@interface NotificationViewController ()

@property(nonatomic, retain) NSMutableArray *notiList_Array;
@property dataBaseModel *dbModel;
@property (strong, nonatomic) NSMutableArray *FinalListArray;


@end

@implementation NotificationViewController
@synthesize notification_TableView, notiList_Array, noti_Dic, msg_Lbl;
- (void)viewDidLoad
{
    // Do any additional setup after loading the view.
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:-1];
    //left button
    [self setUpNavigationItem];
    notiList_Array = [[NSMutableArray alloc]init];
    _FinalListArray = [[NSMutableArray alloc]init];
    
    msg_Lbl.hidden = YES;
    notification_TableView.estimatedRowHeight = 100.0;
    notification_TableView.rowHeight = UITableViewAutomaticDimension;
    msg_Lbl.hidden = NO;
    notification_TableView.hidden = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self getNotification];
}

-(void)setUpNavigationItem
{
    UIButton *btnBack = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    [btnBack setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    // [btnBack addTarget:self action:@selector(backPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc]initWithCustomView:btnBack];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    
    self.navigationItem.title=@"Notifications";
    self.sideleftbarButton=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-close"] style:UIBarButtonItemStyleDone target:self action:@selector(closePressed)];
    self.sideleftbarButton.tintColor=[UIColor whiteColor];
    [[self navigationItem] setRightBarButtonItem:self.sideleftbarButton];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"WorkSans-Medium" size:17]}];
}

-(void)closePressed
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)getNotification
{
    
    @try {
        MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.labelText = @"loading";
        
        NSMutableDictionary *Discparam=[[NSMutableDictionary alloc]init];
        // [Discparam setValue:@"cr2016" forKey:@"validate"];
        //[Discparam setValue:@"banner" forKey:@"action"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];  //AFHTTPResponseSerializer serializer
        manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:USER_id] intValue]>0)
        {
            NSString  *strQuery = @"";
            if ([[ClsSetting urlType] isEqualToString:@"Live"]) {
                strQuery=[NSString stringWithFormat:@"%@?userid=%@",[ClsSetting notificationListURL],[[NSUserDefaults standardUserDefaults] valueForKey:USER_id]];
            } else {
                strQuery=[NSString stringWithFormat:@"%@/spGetNotification(%@)?api_key=%@",[ClsSetting procedureURL],[[NSUserDefaults standardUserDefaults] valueForKey:USER_id],[ClsSetting apiKey]];
            }
            
            NSString *url = strQuery;
            NSLog(@"%@",url);
            NSString *encoded = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [manager GET:encoded parameters:Discparam success:^(AFHTTPRequestOperation *operation, id responseObject)
             {
                 NSError *error;
                 NSArray *notiArray = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&error];
                NSLog(@"%@",notiArray);
                for (NSDictionary *dict in notiArray)
                {
                    _dbModel = [[dataBaseModel alloc]init];
                    _dbModel.deviceId = [dict valueForKey:@"device_id"];
                    _dbModel.bidTitle = [dict valueForKey:@"subject"];
//                    _dbModel.bidMessage = [dict valueForKey:@"shortdescription"];
                    _dbModel.bidMessage = [dict valueForKey:@"description"];
                    _dbModel.date = [dict valueForKey:@"CreatedDate"];
                    [notiList_Array addObject:_dbModel];
                    _dbModel = nil;
                }
                 if (notiList_Array.count > 0){
                     msg_Lbl.hidden = YES;
                     notification_TableView.hidden = NO;
                 }
                [self setUptableData];
                 [HUD hide:YES];
                 
             }
                 failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                     NSLog(@"Error: %@", error);
                     [ClsSetting ValidationPromt:error.localizedDescription];
                     [HUD hide:YES];
                     
            }];
        }
        else
        {
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"SignIn" bundle:nil];
            BforeLoginViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"BforeLoginViewController"];
            [self.navigationController pushViewController:rootViewController animated:YES];
        }
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
    }
}


-(void)setUptableData{
    if ([[DB sharedInstance]getNotificationCount] > 0) {
        _FinalListArray = [[[DB sharedInstance]getDataFromDatabase]mutableCopy];
        [_FinalListArray addObjectsFromArray:notiList_Array];
        _FinalListArray = [[self getSortedArray:_FinalListArray]mutableCopy];
    } else {
        _FinalListArray = [notiList_Array mutableCopy];
        _FinalListArray = [self getSortedArray:_FinalListArray];
       
    }
    
    [notification_TableView reloadData];
}

-(NSMutableArray *)getSortedArray:(NSMutableArray *)tableListArray{
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [tableListArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        dataBaseModel *object1 = [[dataBaseModel alloc]init];
        dataBaseModel *object2 = [[dataBaseModel alloc]init];
        object1 = obj1;
        object2 = obj2;
        
        NSDate *date1 = [dateFormatter dateFromString:object1.date];
        NSDate *date2 = [dateFormatter dateFromString:object2.date];
        object1 = object2 = nil;
        return [date2 compare:date1];
    }];
    return tableListArray;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _FinalListArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mCell"forIndexPath:indexPath];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell isKindOfClass:[NotificationTableViewCell class]])
    {
        NotificationTableViewCell *mCell = (NotificationTableViewCell *)cell;
         _dbModel = _FinalListArray[indexPath.row];
        mCell.labelHeading.text = _dbModel.bidTitle;
        mCell.labelSubDesc.text =_dbModel.bidDetailMessage;
        mCell.labelDesc.text = _dbModel.bidMessage;
        NSString *pDate = _dbModel.date;
        mCell.mTime_Lbl.text = [NSString stringWithFormat:@"%@, %@",[NSString StringFromDate:pDate],[NSString calculatePostTimeForIndexPath:pDate]];
        
        UIView *dotView = [cell viewWithTag:11];
        dotView.layer.cornerRadius = 8.0;
        dotView.backgroundColor = [UIColor colorWithRed:167.0/255.0 green:142.0/255.0 blue:105.0/255.0 alpha:1];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
