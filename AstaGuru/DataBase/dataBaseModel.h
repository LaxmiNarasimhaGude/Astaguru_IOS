//
//  dataBaseModel.h
//  AstaGuru
//
//  Created by Tejas Todkar on 16/03/21.
//  Copyright © 2021 4Fox Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface dataBaseModel : NSObject

@property(nonatomic,retain)NSString *deviceId;
@property(nonatomic,retain)NSString *bidTitle;
@property(nonatomic,retain)NSString *bidMessage;
@property(nonatomic,retain)NSString *bidDetailMessage;
@property(nonatomic,retain)NSString *date;

@end

NS_ASSUME_NONNULL_END
